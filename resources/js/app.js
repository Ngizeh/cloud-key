require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
*/

Vue.component('comment-form', require('./components/CommentForm.vue').default);
Vue.component('comments-section', require('./components/CommentsSection.vue').default);
Vue.component('contact-page', require('./components/ContactPage.vue').default);
Vue.component('add-admin', require('./components/AddAdmin.vue').default);
Vue.component('password-settings', require('./components/PasswordSettings.vue').default);
const app = new Vue({
    el: '#app'
});
