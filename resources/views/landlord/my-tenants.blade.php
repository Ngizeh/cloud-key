@extends('landlord.main')

@section('details')

@include('partials.user_banner', ['type' => 'My Tenants'])


 <div class="user-page content-area-13">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-12">
                <div class="user-profile-box mrb">
                    <!--header -->
                    @include('agent.agent-menu')
                </div>
            </div>
            <div class="col-lg-8 col-md-7 col-sm-12">
                <div class="my-address contact-2 widget hdn-mb-30">
                    <h3 class="heading">My tenants</h3>
                     <label>You don't have tenants yet</label>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
