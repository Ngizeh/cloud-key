<div class="col-lg-8 col-md-7 col-sm-12">
    <div class="my-address contact-1 widget">
        <h3 class="heading">Personal Details</h3>
        @include('partials.errors')
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif
        <form action="/landlord" method="post">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group email">
                        <label>Your Nationality</label>
                        <input type="text" name="nationality" class="form-control" placeholder="Your Nationality"
                               value="{{ old('nationality', $landlordProfile->nationality) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group subject">
                        <label>ID/Passport Number</label>
                        <input type="text" name="identification_number" class="form-control"
                               placeholder="ID or Passport Number"
                               value="{{ old('identification_number',$landlordProfile->identification_number) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group subject">
                        <label>Phone</label>
                        <input type="text" name="phone" class="form-control" placeholder="Phone"
                               value="{{ old('phone', $landlordProfile->phone) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group subject">
                        <label>Physical Location</label>
                        <input type="text" name="physical_location" class="form-control"
                               placeholder="Where can we find you"
                               value="{{ old('physical_location', $landlordProfile->physical_location) }}" required>
                    </div>
                </div>
                 <div class="col-lg-12 ">
                    <p><strong>Bank Details</strong></em></p>
                    <div class="form-group number">
                        <label>Name of your Bank</label>
                        <input type="text" name="bank_name" class="form-control" placeholder="Enter receiving Back Name"
                               value="{{ old('bank_name', $landlordProfile->bank_name) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group number">
                        <label>Bank Account</label>
                        <input type="text" name="bank_account" class="form-control" placeholder="Enter the Bank Account Number"
                               value="{{ old('bank_account', $landlordProfile->bank_account) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group number">
                        <label>Account Branch</label>
                        <input type="text" name="account_branch" class="form-control" placeholder="Enter the Account Branch"
                               value="{{ old('account_branch', $landlordProfile->account_branch) }}" required>
                    </div>
                </div>

                 <div class="col-lg-12 ">
                    <div class="form-group number">
                        <label>Swift Code</label>
                        <input type="text" name="swift_code" class="form-control" placeholder="Enter the Swift code"
                               value="{{ old('swift_code', $landlordProfile->swift_code) }}">
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <p><strong>Next Of Kin Details</strong></em></p>
                    <div class="form-group number">
                        <label>Full Name</label>
                        <input type="text" name="next_of_kin_name" class="form-control" placeholder="Full Name"
                               value="{{ old('next_of_kin_name', $landlordProfile->next_of_kin_name) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group number">
                        <label>Email</label>
                        <input type="text" name="next_of_kin_email" class="form-control" placeholder="email"
                               value="{{ old('next_of_kin_email', $landlordProfile->next_of_kin_email) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group number">
                        <label>Phone Number</label>
                        <input type="text" name="next_of_kin_phone" class="form-control" placeholder="Phone Number"
                               value="{{ old('next_of_kin_phone', $landlordProfile->next_of_kin_phone) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group number">
                        <label>Relationship</label>
                        <select class="form-control selectpicker search-fields" name="next_of_kin_relationship">
                            <option selected disabled value="">Choose the Relationship</option>
                             @include('partials.relationship', ['person' => $landlordProfile])
                        </select>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="send-btn">
                        <button type="submit" class="btn btn-color btn-md btn-message">Save the Changes</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
