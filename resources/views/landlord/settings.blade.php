@extends('landlord.main')

@section('details')

@include('partials.user_banner', ['type' => 'Settings'])

<div class="user-page content-area-13">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-5 col-sm-12">
				<div class="user-profile-box mrb">
					<!--header -->
					@include('landlord.landlord-menu')
				</div>
			</div>
			@include('partials.password-settings')
		</div>
	</div>
</div>

@endsection
