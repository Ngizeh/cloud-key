<!--header -->
<div class="header clearfix">
    <h2>{{auth()->user()->name}}</h2>
    <h4>CloudKey Landlord</h4>
    <img src="{{auth()->user()->avatar == true ? '/uploaded/avatars/'.auth()->user()->avatar  : '/assets/img/avatar/avatar-13.jpg' }}" alt="avatar" class="img-fluid profile-img">
</div>
<!-- Detail -->
<div class="detail clearfix">
    <ul>
        <li>
            <a href="{{url('/', auth()->user()->roles->role ) }}" class="active">
                <i class="flaticon-user"></i>Profile
            </a>
        </li>
        <li>
            <a href="/landlord-properties">
                <i class="flaticon-house"></i>My Properties
            </a>
        </li>
        <li>
            <a href="#">
                <i class="flaticon-money-bag-with-dollar-symbol"></i>My Revenue
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-users"></i>My Tenants
            </a>
        </li>
        <li>
            <a href="/property/create">
                <i class="fa fa-home"></i>Submit a Property
            </a>
        </li>
        <li>
            <a href="/landlord-settings">
                <i class="fa fa-cog"></i>Settings
            </a>
        </li>
        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault();  document.getElementById('logout-form').submit();" >
                <i class="flaticon-logout"></i>Log Out
            </a>
            <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</div>
