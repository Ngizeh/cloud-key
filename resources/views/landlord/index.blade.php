@extends('landlord.main')

@section('details')

@include('partials.user_banner', ['type' => 'Landlord Profile'])

    <!-- User page start -->
    <div class="user-page content-area-14">
        <div class="container">
            <div class="row search-area contact-1">
                <div class="col-lg-4 col-md-5 col-sm-12">
                    <div class="user-profile-box mrb">
                        @include('landlord.landlord-menu')
                    </div>
                </div>
                @include('landlord.landlord-profile')
            </div>
        </div>
    </div>
    <!-- User page end -->

@endsection
