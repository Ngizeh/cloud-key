@extends('landlord.main')


@section('details')


@include('partials.user_banner', ['type' => 'My Properties'])


    <div class="user-page content-area-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="user-profile-box mrb">
                        @include('landlord.landlord-menu')
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12">
                  @include('partials.my-properties')
                </div>
            </div>
        </div>
    </div>

@endsection
