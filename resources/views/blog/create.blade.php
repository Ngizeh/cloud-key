@extends('blog.main')

@section('details')


<!-- Sub banner 2 start -->
@include('partials.banner', ['title' => 'Create Blog', 'type' => 'Add a blog' ])
    <!-- Sub banner 2 end -->

    <!-- User page start -->
    <div class="user-page content-area-7">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="search-area contact-1">
                        <div class="search-area-inner">
                            <div class="search-contents ">
                                <h3 class="heading">The Blog</h3>
                                <form method="post" action="/blogs">
                                    @include('form_partial.blog')
                                    <div class="row">
                                        <div class="col-lg-6 col-4">
                                            <a href="{{url()->previous()}}" class="btn btn-link">
                                                <i class="fa fa-chevron-left"></i>
                                                Back
                                            </a>
                                        </div>
                                        <div class="col-lg-6 col-4">
                                            <button type="submit" class="btn btn-md btn-primary">Continue</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- User page end -->

@endsection
