@extends('blog.main')

@section('details')

    <div class="sub-banner overview-bgi">
        <div class="container">
            <div class="breadcrumb-area">
                <h1>Blog Images</h1>
                <ul class="breadcrumbs">
                    <li><a href="/">Home</a></li>
                    <li class="active">Add an image</li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Sub banner 2 end -->
    <div class="container py-4">
        <div class="box">
            @foreach($blog->photos->chunk(3) as $set)
                <div class="row">
                    @foreach($set as $photo)
                        <div class="col-md-4 col-lg-4">
                            <form method="post" action="/blog-image/{{$photo->id}}" class="mb-0">
                                @csrf
                                {{ method_field('DELETE')}}
                                <div class="">
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </div>
                            </form>
                            <img src="/{{$photo->thumbnail}}" alt="">
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
        <div class="box py-4">
            <h3 class="heading">Blog Gallery</h3>
            <form id="myDropzone" action="/blog-images/{{$slug}}/files"
                  class="dropzone" method="post" enctype="multipart/form-data">
                @csrf
            </form>
        </div>

        <div class="py-4">
            <div class="row flex-nowrap justify-content-between">
                <div class="col-md-6">
                    <a href="javascript:history.back()" class="btn btn-info">Back</a>
                </div>
                <div class="col-md-6 ">
                    <a href="/blogs/{{$blog->slug}}" class="btn btn-primary">Publish</a>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts.footer')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
    <script>
        Dropzone.options.myDropZone = {
            paramName: 'file',
            maxFilesize: 10.0,
            acceptedFiles: '.jpg,.jpeg,.png, .bmp'
        };
    </script>
@stop

