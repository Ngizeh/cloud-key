@extends('blog.main')

@section('details')


    <!-- Sub banner 2 start -->
    @include('partials.banner', ['title' => 'Blog in Full Details ', 'type' => 'Our Property Full Details' ])
    <!-- Sub banner 2 end -->

  <!-- Blog section start -->
<div class="blog-section content-area-7">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <!-- Blog grid box start -->
                <div class="blog-grid-box">
                    @foreach($blog->photos->chunk(1) as $set)
                        @foreach($set as $photo)
                            @if ($loop->parent->first)
                            <img class="blog-theme img-fluid" src="/{{$photo->path}}" alt="blog-3">
                            @endif
                        @endforeach
                    @endforeach
                    <div class="detail">
                        <div class="date-box">
                            <h5>{{ $blog->formattedDay() }}</h5>
                            <h5> {{ $blog->formattedMonth() }}</h5>
                        </div>
                        <h2>
                            {{ $blog->title }}
                        </h2>
                        <div class="post-meta">
                            <span><a href="#"><i class="fa fa-user"></i>{{ $blog->user->name }}</a></span>
                            <span><a><i class="fa fa-clock-o"></i>{{ $blog->formattedDate() }}</a></span>
                            <span><a href="#"><i class="fa fa-commenting-o"></i>24 Comment</a></span>
                        </div>
                        <p style="white-space: pre-line">
                            {{ $blog->description }}
                        </p>
                        <br>
                        <div class="row clearfix tags-socal-box">
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <div class="tags">
                                    <h2>Tags</h2>
                                    <ul>
                                        <li><a href="#">Image</a></li>
                                        <li><a href="#">Features</a></li>
                                        <li><a href="#">Gallery</a></li>
                                        <li><a href="#">News</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <div class="social-list">
                                    <h2>Share</h2>
                                    <ul>
                                        <li>
                                            <a href="#" class="facebook">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="twitter">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="google">
                                                <i class="fa fa-google"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="linkedin">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="rss">
                                                <i class="fa fa-rss"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Blog grid box end -->
                <!-- Comments section start -->
              <comments-section :comments="{{ $comments }}" :blog="{{ $blog }}"></comments-section>
            </div>
        </div>
    </div>
</div>
<!-- Blog section end -->
@endsection
