@extends('blog.main')

@section('details')

<!-- Sub banner 2 start -->

@include('partials.banner', ['title' => 'Blog For Our Properties', 'type' =>'What Others are Saying' ])

<!-- Sub banner 2 end -->

<!-- Blog section start -->
<div class="blog-section content-area-2">
   <div class="container">
		<div class="row">
			@forelse ($blogs as $blog)
			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="blog-grid-box">
					@foreach($blog->photos->chunk(1) as $set)
					  @foreach($set as $photo)
                            @if ($loop->parent->first)
                            <img class="blog-theme img-fluid" src="/{{$photo->path}}" alt="blog-3">
                            @endif
                        @endforeach
                     @endforeach
					<div class="detail my-4">
						<div class="date-box">
							<h5>{{ $blog->formattedDay() }}</h5>
							<h5> {{ $blog->formattedMonth() }}</h5>
						</div>
						<h3>
							<a href="/blogs/{{ $blog->slug }}">{{ $blog->title }}</a>
						</h3>
						<div class="post-meta">
							<span><a href="#"><i class="fa fa-user"></i>{{ $blog->user->name }}</a></span>
							<span><a href="#"><i class="fa fa-commenting-o"></i>24 Comment</a></span>
						</div>
						<p style="height: 7rem">{{ $blog->blogDescription() }}</p>
						<a href="/blogs/{{ $blog->slug }}" class="btn-read-more">Read more</a>
					</div>
				</div>
			</div>
			@empty
			<div class="col-lg-12">
				<h3 class="heading">No blogs posted yet </h3>
			</div>
			@endforelse
            </div>
            <div class="col-lg-12">
            	<div class="pagination-box">
            		<nav aria-label="Page navigation example">
            			<ul class="pagination">
            				{{ $blogs->links() }}
            			</ul>
            		</nav>
            	</div>
          </div>
        </div>
    </div>
</div>
<!-- Blog section end -->

@endsection
