@csrf
<div class="row mb-50">
    <div class="col-md-12">
        <div class="form-group">
            <label for="title">Property Title</label>
            <input type="text" name="title"
                   class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                   placeholder="Property Title" value="{{old('title', $property->title)}}">
            @if ($errors->has('title'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group">
            <label for="price">Price per Month</label>
            <input type="text" name="price" class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" placeholder="KSH" value="{{old('price', $property->price)}}">
            @if ($errors->has('price'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('price') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group">
            <label for="deposit">Deposit</label>
            <input type="number" name="deposit" class="form-control  {{ $errors->has('deposit') ? 'is-invalid' : '' }}" placeholder="KSH" value="{{old('deposit', $property->deposit)}}">
            @if ($errors->has('deposit'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('deposit') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group">
            <label for="type">Type</label>
            <select class="custom-select selectpicker search-fields {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type">
                <option selected disabled>Choose the type</option>
                <option value="Apartment" {{$property->type == "Apartment" ? 'selected' : ''}}>Apartment</option>
                <option value="House" {{$property->type == "House" ? 'selected' : ''}}>House</option>
                <option value="Commercial"{{$property->type == "Commercial" ? 'selected' : ''}}>Commercial</option>
                <option value="Garage" {{$property->type == "Garage" ? 'selected' : ''}}>Garage</option>
                <option value="Lot" {{$property->type == "Lot" ? 'selected' : ''}}>Lot</option>
            </select>
            @if ($errors->has('type'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('type') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group">
            <label for="units">Number of Units</label>
            <input type="number" name="units" class="form-control {{ $errors->has('units') ? 'is-invalid' : '' }}" placeholder="Number of Units" value="{{old('units', $property->units)}}">
            @if ($errors->has('units'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('units') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group">
            <label for="area">Area in Square Foot</label>
            <input type="number" name="area" class="form-control {{ $errors->has('area') ? 'is-invalid' : '' }}" placeholder="SqFt" value="{{old('area', $property->area)}}">
            @if ($errors->has('area'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('area') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group">
            <label for="bedroom">Bedrooms</label>
            <input type="number" name="bedroom" class="form-control {{ $errors->has('bedroom') ? 'is-invalid' : '' }}" placeholder="Number of Bedrooms" value="{{old('bedroom', $property->bedroom)}}">
            @if ($errors->has('bedroom'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('bedroom') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group">
            <label for="bathroom">Bathroom</label>
            <input type="number" name="bathroom" class="form-control {{ $errors->has('bathroom') ? 'is-invalid' : '' }}" placeholder="Number of Bedrooms" value="{{old('bathroom', $property->bathroom)}}">
            @if ($errors->has('bathroom'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('bathroom') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group">
            <label for="available_date">Availability Date</label>
            <input type="date" name="available_date" class="form-control {{ $errors->has('available_date') ? 'is-invalid' : '' }}" placeholder="Select Date" value="{{old('available_date', $property->available_date)}}">
            @if ($errors->has('available_date'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('available_date') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group">
            <label for="installements">Do you accept Deposit Installments?</label>
            <select class="custom-select selectpicker search-fields {{ $errors->has('installments') ? 'is-invalid' : '' }}" name="installments">
                <option selected disabled >Choose the options</option>
                <option value="1" {{ $property->installments == "1" ? 'selected' : ''}}>Yes</option>
                <option value="2" {{ $property->installments == "2" ? 'selected' : ''}}>No</option>
            </select>
            @if ($errors->has('installments'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('installments') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group">
            <label for="compound_units">Units in the Compound(optional)</label>
            <input type="number" name="compound_units" class="form-control" placeholder="Number of units in the compound" value="{{old('compound_units', $property->compound_units)}}">
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group">
            <label for="service_charge">Service charge(optional)</label>
            <input type="number" name="service_charge" class="form-control" placeholder="Service Charge" value="{{old('service_charge', $property->service_charge)}}">
        </div>
    </div>
</div>
<h3 class="heading">Location</h3>
<div class="row mb-50">
    <div class="col-lg-6 col-md-6">
        <div class="form-group">
            <label for="address">Street Address</label>
            <input type="text" name="address" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" placeholder="Street Address" value="{{old('address', $property->address)}}">
            @if ($errors->has('address'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('address') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="form-group">
            <label for="county">County</label>
            <input type="text" name="county" list="test"
            class="form-control {{ $errors->has('county') ? 'is-invalid' : '' }}"
            placeholder="Choose County" value="{{old('county', $property->county)}}"
            >
            <datalist id="test">
                @foreach($property->counties() as $location)
                <option value="{{ $location }}">
                @endforeach
            </datalist>
            @if ($errors->has('county'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('county') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="form-group">
            <label for="town">Town</label>
            <input type="text" name="town" class="form-control {{ $errors->has('town') ? 'is-invalid' : '' }}" placeholder="eg. Nairobi" value="{{old('town', $property->town)}}">
            @if ($errors->has('town'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('town') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="form-group">
            <label for="postal_code">Postal Code</label>
            <input type="text" name="postal_code" class="form-control" placeholder="Postal Code" value="{{old('postal_code', $property->postal_code)}}">
        </div>
    </div>
</div>

<h3 class="heading">Detailed Information</h3>
<div class="row mb-50">
    <div class="col-lg-12">
        <div class="form-group message">
            <label for="description">Detailed Information</label>
            <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" placeholder="Detailed Information" rows="10">{{old('description', $property->description)}} </textarea>
            @if ($errors->has('description'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<h3 class="heading">Features (optional)</h3>
<div class="row mb-50">
    <div class="col-lg-4 col-md-4">
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="air-condition" value="air-condition">
                <label class="form-check-label" for="air-condition">
                    Air Condition
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="free-parking" value="free-parking">
                <label class="form-check-label" for="free-parking">
                    Free Parking
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="swimming-pool" value="swimming-pool" >
                <label class="form-check-label" for="swimming-pool">
                    Swimming Pool
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="gym" value="gym" >
                <label class="form-check-label" for="gym">
                    Gym
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="alarm-back-up" value="alarm-back-up" >
                <label class="form-check-label" for="alarm-back-up">
                    Alarm back up
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="solar-panel" value="solar-panel" >
                <label class="form-check-label" for="solar-panel">
                    Solar panels
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="internet" value="internet" >
                <label class="form-check-label" for="internet">
                    Internet plug-in
                </label>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="laundry-room" value="laundry-room" >
                <label class="form-check-label" for="laundry-room">
                    Laundry Room
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="window-covering" value="window-covering" >
                <label class="form-check-label" for="window-covering">
                    Window Covering
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="places" value="places" >
                <label class="form-check-label" for="places">
                    Places to seat
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="backup-generator" value="backup-generator" >
                <label class="form-check-label" for="backup-generator">
                    Backup Generator
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="pet-allowed" value="pet-allowed" >
                <label class="form-check-label" for="pet-allowed">
                    Pets allowed
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="cable" value="cable" >
                <label class="form-check-label" for="cable">
                    Cable plug-in
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="entertainment" value="entertainment" >
                <label class="form-check-label" for="entertainment">
                    Entertainment area
                </label>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="alarm" value="alarm" >
                <label class="form-check-label" for="alarm">
                    Alarm
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="central-heating" value="central-heating">
                <label class="form-check-label" for="central-heating">
                    Central Heating
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="parking-space" value="parking-space">
                <label class="form-check-label" for="parking-space">
                    Parking Space
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="cctv" value="cctv">
                <label class="form-check-label" for="cctv">
                    CCtv
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="borehole" value="borehole">
                <label class="form-check-label" for="borehole">
                    Borehole
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="master-ensuit" value="master-ensuit">
                <label class="form-check-label" for="master-ensuit">
                    Master en suite
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check checkbox-theme">
                <input class="form-check-input" type="checkbox" name="features[]"  id="balcony" value="balcony">
                <label class="form-check-label" for="balcony">
                    Balcony
                </label>
            </div>
        </div>
    </div>
</div>







