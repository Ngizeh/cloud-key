@csrf
<div class="row mb-50">
    <div class="col-md-12">
        <div class="form-group">
            <label>Blog Title</label>
            <input type="text" name="title"
                   class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                   placeholder="Blog Description" value="{{old('title', $blog->title)}}">
            @if ($errors->has('title'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group message">
            <label>Blog Description</label>
            <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" placeholder="Detailed Information" row="40" style="height: 400px">{{old('description', $blog->description)}} </textarea>
            @if ($errors->has('description'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>



