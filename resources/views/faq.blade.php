@extends('layouts.app')

@section('content')

@include('partials.header')

    <!-- Sub banner start -->
    @include('partials.banner', ['title' => 'FAQ', 'type' =>'FAQ' ])
    <!-- Sub banner end -->

<div class="about-us content-area-8 bg-white">
    {{-- <div class="container"> --}}
        <div class="row mx-2 pb-5">
            <div class="col-lg-6 offset-md-3 align-self-center">
                <div class="about-text more-info">
                    <h3>Frequent Asked Questions</h3>
                    <p>Tenant FAQs</p>
                    <div id="faq" class="faq-accordion">
                        <div class="card m-b-0">
                            <div class="card-header">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse1">
                                    After making an application, how long does it take to find out if I am approved?
                                </a>
                            </div>
                            <div id="collapse1" class="card-block collapse">
                                <p>There is no waiting time after signing up as a tenant. However, landlord and employment verifications take approximately 24-48 hours, or longer if the initial information submitted is not complete</p>
                            </div>

                            <div class="card-header">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse2">
                                    Will I be responsible for utility bills?
                                </a>
                            </div>
                            <div id="collapse2" class="card-block collapse">
                                <p>Yes, as a tenant you will be responsible for utility bills according to your consumption</p>
                            </div>

                            <div class="card-header">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse3">
                                    Can I paint the interior of the home I rent or make changes to the property?
                                </a>
                            </div>
                            <div id="collapse3" class="card-block collapse">
                                <p>This will be dependent on your tenancy agreement. Most landlords would allow minor property changes, such as painting a different colour on the walls, but they will require the property to be restored to its original state when the tenant is moving out. Alternatively, the costs to restore the property to its original condition could be deducted from the tenant’s security deposit. </p>
                            </div>

                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    Can I pay my rent in person?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>No, all rent payments and payment for other third-party services (if necessary) will be done online through the Cloudkey payment portal..</p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    Do I have to be there for my move-out walk through?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    It’s important that when moving out you are there to ascertain that the condition you leave the house in is the condition recorded, for your own peace of mind.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    Do I have to put a holding/good faith deposit down on the home until I find out if I'm approved?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    It’s important that when moving out you are there to ascertain that the condition you leave the house in is the condition recorded, for your own peace of mind.
                                 </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    How am I expected to leave the home?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    You are expected to take care of the property during your tenancy, so that it is in good condition for the next tenant.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    How can I know which properties are available and which are not just from the website?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    The properties on Cloudkey that are not available during the period you’re interested in will be shown as “Not available” but will still appear in the search results.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    I have credit issues. Can I still qualify as a renter?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    As Cloudkey we will need to verify your ability to pay rent before moving into a property.
                                 </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    Once I view the home, can I hold the home so that it is no longer available for anyone else?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    House availability on Cloudkey is on a ‘first come first serve basis’. If you see a house that you like and would want to move into, you will be required to pay a security deposit, after which it will be shown as “Not available” to other potential tenants.
                                 </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    What guidelines and documentation do I need in order to apply for a home?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    Once you have signed up as a tenant on Cloudkey you will be able to rent whatever house you like. The necessary documents will be listed for you as part of the sign-up process for a tenant.
                                 </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    What happens to my application fee if I change my mind and don't want the home I applied for?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    There is no application fee applicable on Cloudkey.
                                 </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    If I have maintenance issues, can I call a repairman and deduct the cost from my rent?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    Maintenance fees resulting from normal wear and tear or because of damage caused by the tenant will be handled by the tenant. There are third party maintenance services that you can access from the Cloudkey website. If, however, the maintenance results from structural deficiencies previously existing, the repair costs will be handled by the landlord. As Cloudkey, we try to ensure that all houses are in good condition before a tenant moves in but cannot completely offer a guarante
                                 </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    How much Security Deposit do you require?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    The amount of security deposit varies per property, and will be clearly outlined in the listing on the website and as part of the tenancy agreement.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    I have an emergency maintenance problem; how do I get in touch with someone?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    There are third party service providers listed on the website that you can reach out. Additionally, you can also get in touch with us as Cloudkey.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    What forms of payment do you accept for rent?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    All rental payments will be paid online through our various payment channels or via MPesa.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    When is rent due? When is it late?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    This will be clearly stipulated in the tenancy agreement for each property. Typically, that month’s rent will be due at the start of the month. Failure to pay rent on time will attract certain penalties, as will be outlined in the tenancy agreement. Continued late payment of rent or non-payment may result in eviction.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    How much notice do I need to give that I am vacating the home?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    This will be stipulated in the tenancy agreement, though most landlords require at least one month notice to be given.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    How do I handle my move-out?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    You will be required to have given sufficient notice prior to moving out. On the actual day that you are moving out, you can request for moving services from the Cloudkey website. One of our agents will be present to help you fill out the clearance form, as well as to clarify the details of your security deposit refund should you have any questions.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    When can I expect to receive my security deposit back after I move? Will I receive a full refund of my deposit?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    This will be communicated to you during your notice period, prior to your moving out.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    I just received my move-out report and I need to pay money that is owed. What do I do?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    Usually if you owe any money as a result of repairs that will need to be undertaken before the next tenant moves in, this will be deducted from your security deposit. If, however, the repair costs exceed the amount of the security deposit paid, you will be required to pay the extra amount.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    Who do I speak to if I have problems with my tenancy?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    Refer to the tenancy agreement to check if that matter is covered therein. If it is not, contact Cloudkey.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    How quickly can I move in?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    As soon as you have made the required payments as per the tenancy agreement you may move in.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    Do I need to take out my own insurance?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    No, home insurance will typically be taken care of by the landlord.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    What rights does my landlord have on entering my property?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    Another potential tenant may wish to view the property if you have given notice to vacate. Your landlord would have need to notify you of this, at least 24 hours prior to the viewing, according to the tenancy agreement.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    Do I have to advise my landlord if I am going on holiday or will be away for an extended amount of time?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    Yes, it is advisable to do this.
                                 </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    What penalties will I incur should I break my lease agreement early?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    It will be clearly stated what the consequences are of terminating your tenancy without giving the required notice
                                 </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    How do I renew my contract?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    So long as you have not given notice to terminate your tenancy, it is assumed that you will be continuing as a tenant for at least the normal duration of the notice period.
                                 </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    What are my responsibilities as a tenant?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    As a tenant on Cloudkey you will be required to abide by our rules and regulations.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    Can a tenant withhold rent because a landlord is not maintaining the premises?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    A tenant will typically be required to pay rent. Should the landlord not fulfill their requirements with regards to maintenance and according to the tenancy agreement, Cloudkey will step in to handle the matter
                                 </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    Can a landlord terminate a rental agreement when the tenant does not pay rent?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    This will be stipulated in the tenancy agreement. Typically, there will be some penalties for late payment of rent, but total non-payment may result in eviction.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    Can a landlord increase rent by giving a verbal notice to the tenant?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    No, the means and circumstances for increasing rent will be clearly stipulated in the tenancy agreement.
                                </p>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    Can a termination notice be verbal?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>
                                    No, a termination notice will typically be given in writing, in accordance with the tenancy agreement.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{-- </div> --}}
    </div>
</div>


@include('partials.footer')

@endsection
