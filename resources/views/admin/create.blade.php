@extends('admin.main')

@section('details')

    @include('partials.user_banner', ['type' => 'Add Admin'])

    <div class="user-page content-area-13">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-5 col-sm-12">
                    <div class="user-profile-box mrb">
                        <!--header -->
                        @include('admin.admin-menu')
                    </div>
                </div>
                <add-admin></add-admin>
            </div>
        </div>
    </div>

@endsection

