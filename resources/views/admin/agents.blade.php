@extends('admin.main')

@section('details')

    @include('partials.user_banner', ['type' => 'My Agents'])

    <div class="user-page content-area-13">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-5 col-sm-12">
                    <div class="user-profile-box mrb">
                        <!--header -->
                        @include('admin.admin-menu')
                    </div>
                </div>
                <div class="col-lg-8 col-md-7 col-sm-12">
                    @if(Session::has('deleted-property'))
                        <div class="alert alert-success">
                            {{ Session::get('deleted-property') }}
                        </div>
                    @endif
                    @if(count($agents))
                        <div class="my-properties">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Listed Properties</th>
                                    <th>Date Listed</th>
                                    <th>Approved</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                @foreach( $agents as $agent )
                                    <tbody>
                                    <tr>
                                        <td>{{ $agent->user()->first()->name }}</td>
                                        <td>{{ ucfirst($agent->user()->first()->properties()->first()->type) ?? html_entity_decode('&mdash;') }}</td>
                                        <td>{{ $agent->user()->first()->properties()->count() }}</td>
                                        <td>{{$agent->created_at->diffForHumans()}}</td>
                                        <td>Yes</td>
                                        <td><button class="btn btn-dark"><span class="text-sm">disapprove</span></button></td>
                                    </tr>
                                    </tbody>
                                @endforeach
                            </table>
                        </div>
                        <div class="pagination-box">
                            {{  $agents->links('partials.pagination') }}
                        </div>
                    @else
                        <div class="my-address contact-2 widget hdn-mb-30">
                            <h3 class="heading">My Agents</h3>
                            <label>You don't have Agents yet</label>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
