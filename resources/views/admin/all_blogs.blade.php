@extends('admin.main')

@section('details')

@include('partials.user_banner', ['type' => 'All Blogs'])

<div class="user-page content-area-2">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-12 col-sm-12">
				<div class="user-profile-box mrb">
					@include('admin.admin-menu')
				</div>
			</div>
			<div class="col-lg-8 col-md-12 col-sm-12">
				@if(Session::has('deleted-blog'))
				<div class="alert alert-info">
					{{ Session::get('deleted-blog') }}
				</div>
				@endif
				@if(count($blogs))
				<div class="my-properties">
					<table class="table">
						<thead>
							<tr>
								<th>Blog Image</th>
								<th>Title</th>
								<th>Description</th>
								<th>Created</th>
								<th>Actions</th>
							</tr>
						</thead>
						@foreach( $blogs as $blog )
						<tbody>
							<tr>
								<td class="image">
									<a href="/property/{{$blog->slug}}">
										@foreach($blog->photos->chunk(1) as $set)
											@foreach($set as $photo)
												@if ($loop->parent->first)
													<img class="img-fluid" src="/{{$photo->path}}" alt="blog">
												@endif
											@endforeach
										@endforeach
									</a>
								</td>
								<td>
									<div class="inner">
										<a href="/blog/{{$blog->slug}}"><h2>{{ $blog->title }}</h2></a>
									</div>
								</td>
								<td>{{ $blog->excerpt }}</td>
								<td>{{ $blog->created_at->diffForHUmans(null, false) }}</td>
								<td class="actions flex items-center">
									<a href="{{ route('blogs.edit', $blog)}}" class="edit" title="Edit"><i class="fa fa-pencil pt-2"></i></a>
									<form action="{{ route('blogs.destroy', $blog)}}" method="post" title="Delete">
										@csrf
										@method('delete')
										<button class="btn btn-light" type="submit"><i class="delete fa fa-trash-o"></i></button>
									</form>
								</td>
							</tr>
						</tbody>
						@endforeach
					</table>
				</div>
				<div class="pagination-box">
					{{ $blogs->links('partials.pagination') }}
				</div>
				@else
				<div class="my-address contact-2 widget hdn-mb-30">
					<h3 class="heading">All Blogs</h3>
					<label>You Don't have Blogs yet</label>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>

@endsection
