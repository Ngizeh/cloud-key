<!--header -->
<div class="header clearfix">
	<h2>{{auth()->user()->name}}</h2>
	<h4>CloudKey Admin</h4>
	<img src="{{auth()->user()->avatar == true ? '/uploaded/avatars/'.auth()->user()->avatar  : '/assets/img/avatar/avatar-13.jpg' }}" alt="avatar" class="img-fluid profile-img">
</div>
<!-- Detail -->
<div class="detail clearfix">
	<ul>
		<li>
			<a href="{{url('/', auth()->user()->roles->role ) }}" class="active">
				<i class="flaticon-user"></i>Profile
			</a>
		</li>
		<li>
			<a href="/admin-properties">
				<i class="flaticon-house"></i>Properties List
			</a>
		</li>
		<li>
			<a href="/property/create">
				<i class="fa fa-plus"></i>Submit a Property
			</a>
		</li>
		<li>
			<a href="/all-admins	">
				<i class="fa fa-users"></i>Admins List
			</a>
		</li>
		<li>
			<a href="/all-agents">
				<i class="fa fa-users"></i>Agents List
			</a>
		</li>
		<li>
			<a href="/all-landlords">
				<i class="fa fa-users"></i>Landlords List
			</a>
		</li>
		<li>
			<a href="/all-tenants">
				<i class="fa fa-users"></i>Tenants List
			</a>
		</li>
		<li>
			<a href="#">
				<i class="flaticon-money-bag-with-dollar-symbol"></i>Revenue Collected
			</a>
		</li>
		<li>
			<a href="/admin/create">
				<i class="fa fa-plus"></i>Add Admin
			</a>
		</li>
		<li>
			<a href="/list-blogs">
				<i class="fa fa-rss"></i>List Blogs
			</a>
		</li>
		<li>
			<a href="/blogs">
				<i class="fa fa-file"></i>Blogs
			</a>
		</li>
		<li>
			<a href="/blogs/create">
				<i class="fa fa-plus"></i>Add Blog
			</a>
		</li>
		<li>
			<a href="/admin-settings">
				<i class="fa fa-cog"></i>Settings
			</a>
		</li>
		<li>
			<a href="{{ route('logout') }}" onclick="event.preventDefault();  document.getElementById('logout-form').submit();" >
				<i class="flaticon-logout"></i>Log Out
			</a>
			<form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
				@csrf
			</form>
		</li>
	</ul>
</div>
