@extends('admin.main')

@section('details')

    @include('partials.user_banner', ['type' => 'All Tenants'])

    <div class="user-page content-area-13">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-5 col-sm-12">
                    <div class="user-profile-box mrb">
                        <!--header -->
                        @include('admin.admin-menu')
                    </div>
                </div>
                <div class="col-lg-8 col-md-7 col-sm-12">
                    @if(Session::has('deleted-property'))
                        <div class="alert alert-success">
                            {{ Session::get('deleted-property') }}
                        </div>
                    @endif
                    @if(count($tenants))
                        <div class="my-properties">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Rented Property</th>
                                    <th>Listed Properties</th>
                                    <th>Joined Cloudkey</th>
                                    <th>Type</th>
                                </tr>
                                </thead>
                                @foreach( $tenants as $user )
                                    <tbody>
                                    <tr>
                                        <td>{{ $user->user->name }}</td>
                                        <td>Kilimani</td>
                                        <td>421</td>
                                        <td>{{$user->created_at->diffForHumans()}}</td>
                                        <td>Apartment</td>
                                    </tr>
                                    </tbody>
                                @endforeach
                            </table>
                        </div>
                        <div class="pagination-box">
                           {{ $tenants->links('partials.pagination') }}
                        </div>
                    @else
                        <div class="my-address contact-2 widget hdn-mb-30">
                            <h3 class="heading">All Tenants</h3>
                            <label>You don't have Tenants yet</label>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
