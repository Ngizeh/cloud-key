<div class="col-lg-8 col-md-7 col-sm-12">
	<div class="my-address contact-1 widget">
		<h3 class="heading">Personal Details</h3>
		@include('partials.errors')
		@if(Session::has('updated-profile'))
		<div class="alert alert-success">
			{{ Session::get('updated-profile') }}
		</div>
		@endif
		<form action="/admin-profile" method="post">
			@csrf
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group email">
						<label>Full Name</label>
						<input type="text" name="name" class="form-control" placeholder="Full Name"
						value="{{ old('name', $user->name) }}" required>
					</div>
				</div>
				<div class="col-lg-12 ">
					<div class="form-group subject">
						<label>Email Address</label>
						<input type="text" name="email" class="form-control"
						placeholder="Email Addeess"
						value="{{ old('email',$user->email) }}" required>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="send-btn">
						<button type="submit" class="text-white btn btn-color btn-md btn-message">Save the Changes</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
