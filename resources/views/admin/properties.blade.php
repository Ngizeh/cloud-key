@extends('admin.main')

@section('details')

@include('partials.user_banner', ['type' => 'All Properties'])

<div class="user-page content-area-2">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-12 col-sm-12">
				<div class="user-profile-box mrb">
					@include('admin.admin-menu')
				</div>
			</div>
			<div class="col-lg-8 col-md-12 col-sm-12">
				@if(Session::has('deleted-property'))
				<div class="alert alert-success">
					{{ Session::get('deleted-property') }}
				</div>
				@endif
				@if(count($properties))
				<div class="my-properties">
					<table class="table">
						<thead>
							<tr>
								<th>Property</th>
								<th></th>
								<th>Created</th>
								<th>Views</th>
								<th>Actions</th>
							</tr>
						</thead>
						@foreach( $properties as $property )
						<tbody>
							<tr>
								<td class="image">
									<a href="/property/{{$property->slug}}">
										<x-image :property="$property" class="img-fluid"></x-image>
									</a>
								</td>
								<td>
									<div class="inner">
										<a href="/property/{{$property->slug}}"><h2>{{$property->truncatedTitle()}}</h2></a>
										<figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i>
											<small>{{$property->address}} </small>
										</figure>
										<div class="tag price">Ksh. {{$property->price}}</div>
									</div>
								</td>
								<td>{{$property->created_at }}</td>
								<td>421</td>
								<td class="actions flex items-center">
									<a href="/property/{{$property->id}}/edit" class="edit" title="Edit"><i class="fa fa-pencil pt-2"></i></a>
									<form action="/property/{{ $property->id}}" method="post" title="Delete">
										@csrf
										@method('delete')
										<button class="btn btn-light" type="submit"><i class="delete fa fa-trash-o"></i></button>
									</form>
								</td>
							</tr>
						</tbody>
						@endforeach
					</table>
				</div>
				<div class="pagination-box">
					{{ $properties->links('partials.pagination') }}
				</div>
				@else
				<div class="my-address contact-2 widget hdn-mb-30">
					<h3 class="heading">All Properties</h3>
					<label>You Don't have Properties yet</label>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>

@endsection
