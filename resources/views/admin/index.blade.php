@extends('admin.main')

@section('details')

    @include('partials.user_banner', ['type' => 'Admin Profile',])

    <!-- User page start -->
    <div class="user-page content-area-14">
        <div class="container">
            <div class="row search-area contact-1">
                <div class="col-lg-4 col-md-5 col-sm-12">
                    <div class="user-profile-box mrb">
                        @include('admin.admin-menu')
                    </div>
                </div>
               @include('admin.admin-profile')
            </div>
        </div>
    </div>
    <!-- User page end -->


@endsection
