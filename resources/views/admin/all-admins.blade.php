@extends('admin.main')

@section('details')

    @include('partials.user_banner', ['type' => 'Admins'])

    <div class="user-page content-area-13">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-5 col-sm-12">
                    <div class="user-profile-box mrb">
                        <!--header -->
                        @include('admin.admin-menu')
                    </div>
                </div>
                <div class="col-lg-8 col-md-7 col-sm-12">
                    @if(Session::has('deleted-admin'))
                        <div class="alert alert-success">
                            {{ Session::get('deleted-admin') }}
                        </div>
                    @endif
                    @if(count($admins))
                        <div class="my-properties">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                @foreach( $admins as $role )
                                    <tbody>
                                    <tr>
                                        <td>
                                            {{ $role->user->name }}
                                        </td>
                                        <td>{{ $role->user->email }}</td>
                                        <td>{{$role->user->created_at->diffForHumans()}}</td>
                                        @unless (auth()->id() === $role->user->id)
                                        <td>
                                            Delete
                                        </td>
                                        @endunless
                                    </tr>
                                    </tbody>
                                @endforeach
                            </table>
                        </div>
                        <div class="pagination-box">
                            {{ $admins->links('partials.pagination') }}
                        </div>
                    @else
                        <div class="my-address contact-2 widget hdn-mb-30">
                            <h3 class="heading">My Agents</h3>
                            <label>You don't have Agents yet</label>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
