<html lang="zxx">
<head>
    <title>Cloud Key - Rentals</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- External CSS libraries -->
    <link rel="stylesheet"  href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet"  href="/assets/css/magnific-popup.css">
    <link rel="stylesheet"  href="/assets/css/jquery.selectBox.css">
    <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">
    <link rel="stylesheet"  href="/assets/css/skins/default.css">
    <link rel="stylesheet"  href="/assets/css/rangeslider.css">
    <link rel="stylesheet"  href="/assets/css/animate.min.css">
    <link rel="stylesheet"  href="/assets/css/leaflet.css">
    <link rel="stylesheet"  href="/assets/css/map.css">
    <link rel="stylesheet"  href="/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet"  href="/assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet"  href="/assets/fonts/flaticon/font/flaticon.css">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="/assets/images/cloudlogo.png" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPoppins:400,500,700,800,900%7CRoboto:100,300,400,400i,500,700">

    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/skins/default.css">
    <link rel="stylesheet" href="/css/app.css">
    <script src="/assets/js/jquery-2.2.0.min.js"></script>
    <script src="https://unpkg.com/turbolinks"></script>
    <script>
		window.App =  @json([
			'csrfToken' => csrf_token(),
			'user' => Auth::user(),
			'signedIn' => Auth::check()
			]);
            setTimeout(() => {
                 $('.alert').alert('close')
            }, 4000)
		</script>
</head>
<body id="top">
    <div id="app">
        @yield('content')
    </div>
    <!-- External JS libraries -->
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/jquery.selectBox.js"></script>
<script src="/assets/js/rangeslider.js"></script>
<script src="/assets/js/jquery.magnific-popup.min.js"></script>
<script src="/assets/js/jquery.filterizr.js"></script>
<script src="/assets/js/wow.min.js"></script>
<script src="/assets/js/backstretch.js"></script>
<script src="/assets/js/jquery.scrollUp.js"></script>
<script src="/assets/js/particles.min.js"></script>
<script src="/assets/js/typed.min.js"></script>
<script src="/assets/js/dropzone.js"></script>
<script src="/assets/js/jquery.mb.YTPlayer.js"></script>
<script src="/assets/js/leaflet.js"></script>
<script src="/assets/js/leaflet-providers.js"></script>
<script src="/assets/js/leaflet.markercluster.js"></script>
<script src="/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
 <script src="/assets/js/ie-emulation-modes-warning.js"></script>
@yield('scripts.footer')
<!-- Custom JS Script -->
 <script  src="/assets/js/app.js"></script>
<script src="/js/app.js"></script>

<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5b863e65f31d0f771d843d2e/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
</body>
</html>
