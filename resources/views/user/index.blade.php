@extends('layouts.app')

@section('content')

@include('partials.header')

<!-- Sub banner 2 start -->
<div class="sub-banner overview-bgi">
  <div class="container">
	<div class="breadcrumb-area">
	  <h1>My Profile</h1>
	  <ul class="breadcrumbs">
		<li><a href="index.html">Home</a></li>
		<li class="active">My Profile</li>
	  </ul>
	</div>
  </div>
</div>
<!-- Sub banner 2 end -->

<!-- User page start -->
<div class="user-page content-area-14">
  <div class="container">
	<div class="row">
	  <div class="col-lg-4 col-md-5 col-sm-12">
		<div class="user-profile-box mrb">
		  @include('layouts.user-menu')
		</div>
	  </div>
	  <div class="col-lg-8 col-md-7 col-sm-12">
		<div class="my-address contact-2 widget">
		  <h3 class="heading">Personal Details</h3>
		  <form action="#" method="GET" enctype="multipart/form-data">
			<div class="row">
			  <div class="col-lg-12 ">
				<div class="form-group name">
				  <label>Your Name</label>
				  <input type="text" name="name" class="form-control" placeholder="John Deo">
				</div>
			  </div>
			  <div class="col-lg-12 ">
				<div class="form-group number">
				  <label>Email</label>
				  <input type="email" name="email" class="form-control" placeholder="Email">
				</div>
			  </div>
			  <div class="col-lg-12">
				<div class="form-group email">
				  <label>Your Nationality</label>
				  <input type="text" name="title" class="form-control" placeholder="Your Nationality">
				</div>
			  </div>
			  <div class="col-lg-12 ">
				<div class="form-group subject">
				  <label>Phone</label>
				  <input type="text" name="phone" class="form-control" placeholder="Phone">
				</div>
			  </div>
			  <div class="col-lg-12 ">
				<div class="form-group subject">
				  <label>ID/Passport Number</label>
				  <input type="text" name="phone" class="form-control" placeholder="ID or Passport Number">
				</div>
			  </div>
			  <div class="col-lg-12 ">
				<div class="form-group">
				  <label>Select Employment Status</label>
				  <select class="form-control selectpicker search-fields" name="brand">
				   <option value="" disabled>Select Employment Status</option>
				   <option value="">Self Employment</option>
				   <option value="">Employed Company</option>
				 </select>
			   </div>
			 </div>
			 <div class="col-lg-12 ">
			  <p><strong>Next Of Kin Details</strong></em></p>
			  <div class="form-group number">
				<label>Full Name</label>
				<input type="text" name="full-name" class="form-control" placeholder="Full Name">
			  </div>
			</div>
			<div class="col-lg-12 ">
			  <div class="form-group number">
				<label>Email</label>
				<input type="text" name="full-name" class="form-control" placeholder="Full Name">
			  </div>
			</div>
			<div class="col-lg-12 ">
			 <div class="form-group number">
			  <label>Phone Number</label>
			  <input type="text" name="full-name" class="form-control" placeholder="Phone Number">
			</div>
		  </div>
		  <div class="col-lg-12 ">
			<div class="form-group number">
			  <label>Relationship</label>
			  <select class="form-control selectpicker search-fields" name="brand">
			   <option value="" disabled>Choose the Relationship</option>
			   <option value="">Brother</option>
			   <option value="">Sister</option>
			   <option value="">Wife</option>
			   <option value="">Husband</option>
			   <option value="">Father</option>
			   <option value="">Mother</option>
			 </select>
		   </div>
		 </div>

					  <!--       <div class="col-lg-12">
								<div class="form-group message">
									<label>Name</label>
									<textarea class="form-control" name="message" placeholder="Write message"></textarea>
								</div>
							  </div> -->
							  <div class="col-lg-12">
								<div class="send-btn">
								  <button type="submit" class="btn btn-color btn-md btn-message">Save the Changes</button>
								</div>
							  </div>
							</div>
						  </form>
						</div>
					  </div>
					</div>
				  </div>
				</div>
				<!-- User page end -->

				@include('partials.footer')

				@endsection
