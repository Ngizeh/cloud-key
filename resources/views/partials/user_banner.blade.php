<!-- Sub banner 2 start -->
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>{{ ucfirst(auth()->user()->roles->role) }} DashBoard</h1>
            <ul class="breadcrumbs">
                <li><a href="/">Home</a></li>
                <li class="active">{{ $type  }}</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub banner 2 end -->
