@foreach (App\Utilities\Relationship::all() as $relationship)
    <option value="{{ $relationship }}"
    {{ old('next_of_kin_relationship', $person->next_of_kin_relationship) == $relationship ? 'selected' : ''}}>
    {{ ucfirst($relationship) }}
</option>
@endforeach