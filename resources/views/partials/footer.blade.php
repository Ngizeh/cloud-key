<footer class="footer">
    <div class="container footer-inner">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item">
                    <h4>Contact Us</h4>

                    <ul class="contact-info">
                        <li>
                            Spring Valley Nairobi, Kenya
                        </li>
                        <li>
                            Email: <a href="mailto:info@cloudkey.com">info@cloudkey.co.ke</a>
                        </li>
                        <li>
                            Phone: <a href="tel:+254-715-008-946">+254-715-008-946</a>
                        </li>
                    </ul>

                    <ul class="social-list clearfix">
                        <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#" class="rss"><i class="fa fa-rss"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                <div class="footer-item">
                    <h4>
                        Useful Links
                    </h4>
                    <ul class="links">
                        <li>
                            <a href="/about"><i class="fa fa-angle-right"></i>About us</a>
                        </li>
                        <li>
                            <a href="/services"><i class="fa fa-angle-right"></i>Service</a>
                        </li>
                        <li>
                            <a href="/property"><i class="fa fa-angle-right"></i>Properties Listing</a>
                        </li>
                        {{--    <li>
                               <a href="#"><i class="fa fa-angle-right"></i>Properties Grid</a>
                           </li> --}}
                        <li>
                            <a href="/contact-us"><i class="fa fa-angle-right"></i>Contact Us</a>
                        </li>
                        <li>
                            <a href="/blogs"><i class="fa fa-angle-right"></i>Blog</a>
                        </li>
                        <li>
                            <a href="/property"><i class="fa fa-angle-right"></i>Property Details</a>
                        </li>
                        <li>
                            <a href="/faq"><i class="fa fa-angle-right"></i>FAQ</a>
                        </li>
                    </ul>
                </div>
            </div>
            @if($properties->count() > 0)
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                    <div class="recent-posts footer-item">
                        <h4>Recent Properties</h4>
                        @include('partials.widget')
                    </div>
                </div>
            @endif
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                    <h4>Subscribe</h4>
                    <div class="Subscribe-box">
                        <p>Join the monthly newsletter and never miss out on new tips on properties and what's trending in Real Estates.</p>
                        <form action="#" method="GET">
                            <input type="text" class="form-contact" name="email" placeholder="Enter Address">
                            </p>
                            <p>
                                <button type="submit" name="submitNewsletter" class="text-white btn btn-block btn-color">
                                    Subscribe
                                </button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                <p class="copy">&copy;  {{date('Y')}} <a href="/" target="_blank">Cloud Key </a>. Cloud Key all Rights Reserved .</p>
            </div>
        </div>
    </div>
</footer>
{{-- @include('partials.twak-to') --}}
