<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>{{ $title }}</h1>
            <ul class="breadcrumbs">
                <li><a href="/">Home</a></li>
                <li class="active">{{ $type }} </li>
            </ul>
        </div>
    </div>
</div>