@if(count($properties))
    <div class="my-properties">
        <table class="table">
            <thead>
            <tr>
                <th>Property</th>
                <th></th>
                <th>Date Added</th>
                <th>Views</th>
                <th>Actions</th>
            </tr>
            </thead>
            @foreach( $properties as $property )
                <tbody>
                <tr>
                    <td class="image">
                        <a href="/property/{{$property->slug}}">
                            @foreach($property->photos as $set)
                                @if($loop->first)
                                    <img alt="my-properties-3" src="/{{$set->path}}" class="img-fluid">
                                @endif
                            @endforeach
                        </a>
                    </td>
                    <td>
                        <div class="inner">
                            <a href="/property/{{$property->slug}}"><h2>{{$property->title}}</h2></a>
                            <figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i>
                                <small>{{$property->address}}, {{$property->county}},{{$property->town}} </small>
                            </figure>
                            <div class="tag priKsh.e">Ksh. {{$property->price}}</div>
                        </div>
                    </td>
                    <td>{{$property->created_at}}</td>
                    <td>421</td>
                    <td class="actions">
                        <a href="/property/{{$property->id}}/edit" class="edit"><i class="fa fa-pencil"></i>Edit</a>
                        <a href="#"><i class="delete fa fa-trash-o"></i></a>
                    </td>
                </tr>
                </tbody>
            @endforeach
        </table>
    </div>
    <div class="pagination-box">
        {{ $properties->links('partials.pagination')}}
    </div>
@else
    <div class="my-address contact-2 widget hdn-mb-30">
        <h3 class="heading">All Properties</h3>
        <label>You Don't have Properties yet</label>
    </div>
@endif
