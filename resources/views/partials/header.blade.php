	<header class="main-header sticky-header" id="main-header-2">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<nav class="navbar navbar-expand-lg navbar-light rounded">
						<a class="navbar-brand logo navbar-brand d-flex mr-auto" href="/">
							<img src="{!! asset('assets/img/logos/cloudlogo.png') !!}" alt="logo">
						</a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
							<span class="fa fa-bars"></span>
						</button>
						<div class="navbar-collapse collapse w-100" id="navbar">
							<ul class="navbar-nav ml-auto">
                                <li class="nav-item active">
									<a class="nav-link" href="/" >
										Home
									</a>
								</li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/about" >
                                        About Us
                                    </a>
                                </li>
                                <li class="nav-item">
									<a class="nav-link" href="/property" >
										Properties
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="/services">
										Services
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="/contact-us">
										Contact Us
									</a>
								</li>
								<li class="nav-item dropdown">
									<a data-turbolinks="false" href="#full-page-search" class="nav-link">
										<i class="fa fa-search"></i>
									</a>
								</li>

								@guest
								<li class="nav-item">
									<a class="btn btn-sm btn-white-sm-outline btn-round signup-link" href="{{ route('login')}}">Login</a>
								</li>
								<li class="nav-item">
									@if(Route::has('register'))
									<a class="btn btn-sm btn-theme btn-round signup-link" href="{{route('register')}}">Register</a>
									@endif
								</li>
								@else
								<li class="nav-item">
								   <a id="navbarDropdown" class="nav-link " href="{{url('/', auth()->user()->roles->role)}}" role="button" aria-haspopup="true" aria-expanded="false" v-pre>
									 My Profile
								 </a>
							 </li>
							 <li class="nav-item">
								<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="margin-top: -4;">
									{{ Auth::user()->name }}
									<span class="caret"></span>
										<img class="rounded-circle img-fluid profile-img"
                                         src="{{ auth()->user()->avatar == true ? '/uploaded/avatars/'.auth()->user()->avatar :'/assets/img/avatar/avatar-13.jpg'}}"
                                         alt="avatar"
                                         style="height: 30; width: 30; margin-left:.5rem; margin-top: -2px; border:2px solid #c0ddf6;">
									</a>
								<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="background:#c0ddf6 ">
									<a class="dropdown-item" href="{{route('logout')}}"
									onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
									Logout
								</a>
								<form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
									@csrf
								</form>
							</div>
						</li>
						@endguest
					</ul>
				</div>
			</nav>
		</div>
	</div>
</div>
</header>


<div id="full-page-search" class="searching">
	<button type="button" class="close">×</button>
	<form action="#">
		<input type="search" value="" placeholder="Where do you want to stay?" />
		<button type="button" class="text-white btn btn-sm btn-color">Search</button>
	</form>
</div>
<!-- main header end -->
