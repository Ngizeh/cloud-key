  <!-- Search area start -->
  <div class="widget search-area d-none d-xl-block d-lg-block">
    <h5 class="sidebar-title">Advanced Search</h5>
    <div class="search-area-inner">
        <div class="search-contents ">
            <form method="GET">
                <div class="form-group">
                    <label>Area From</label>
                    <select class="selectpicker search-fields" name="area">
                        <option>Area From</option>
                        <option>1500</option>
                        <option>1200</option>
                        <option>900</option>
                        <option>600</option>
                        <option>300</option>
                        <option>100</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Property Status</label>
                    <select class="selectpicker search-fields" name="Status">
                        <option>Property Status</option>
                        <option>For Sale</option>
                        <option>For Rent</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Location</label>
                    <select class="selectpicker search-fields" name="Location">
                        <option>Location</option>
                        <option>United Kingdom</option>
                        <option>American Samoa</option>
                        <option>Belgium</option>
                        <option>Canada</option>
                        <option>Delaware</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Property Types</label>
                    <select class="selectpicker search-fields" name="types">
                        <option>Property Types</option>
                        <option>Residential</option>
                        <option>Commercial</option>
                        <option>Land</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Bedrooms</label>
                    <select class="selectpicker search-fields" name="bedrooms">
                        <option>Bedrooms</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                    </select>
                </div>
                <div class="form-group">
                    <label> Bathrooms</label>
                    <select class="selectpicker search-fields" name="bedrooms">
                        <option>2 Bathrooms</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>
                </div>
                <br>
                <div class="form-group">
                    <label>Price</label>
                 <div class="form-group">
                     <select class="selectpicker search-fields" name="category">
                        <option>Price Range</option>
                        <option>5000-10000</option>
                        <option>5000-10000</option>
                        <option>5000-10000</option>
                        <option>5000-10000</option>
                    </select>
                    </div>
                </div>
                <br/>
                <button class="search-button btn-md btn-primary">Search</button>
            </form>
        </div>
    </div>
</div>

<!-- Categories start -->
<div class="widget categories">
    <h5 class="sidebar-title">Categories</h5>
    <ul>
        <li><a href="#">Apartments<span>(12)</span></a></li>
        <li><a href="#">Houses<span>(8)</span></a></li>
        <li><a href="#">Family Houses<span>(23)</span></a></li>
        <li><a href="#">Offices<span>(5)</span></a></li>
        <li><a href="#">Villas<span>(63)</span></a></li>
        <li><a href="#">Other<span>(7)</span></a></li>
    </ul>
</div>

<!-- Recent posts start -->
<div class="widget recent-posts">
    <h5 class="sidebar-title">Recent Properties</h5>
     @include('partials.widget')
</div>
