@section('scripts.footer')
    <script type="text/javascript">
        $('div.alert-success').not('.alert-important').delay(3000).fadeOut(350);
    </script>
@endsection
