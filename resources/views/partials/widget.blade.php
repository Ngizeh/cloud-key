@foreach ($properties->slice(0,3) as $property)
    <div class="media mb-4">
        <a class="pr-4" href="/property/{{ $property->slug }}">
            <x-image :property="$property" style="height:75px"></x-image>
        </a>
        <div class="media-body align-self-center">
            <h5>
                <a href="/property/{{ $property->slug }}">{{ $property->truncatedTitleWidget() }}</a>
            </h5>
            <p>
                {{ $property->formatted_date }}
            </p>
            <p> <strong>Ksh. {{ $property->price }}</strong></p>
        </div>
    </div>
@endforeach
