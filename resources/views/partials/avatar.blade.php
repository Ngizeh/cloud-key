<h3 class="heading">Profile Settings</h3>
<div class="my-address contact-2 widget hdn-mb-30">
    @if(Session::has('uploaded-picture'))
        <div class="alert alert-success">
            {{ Session::get('uploaded-picture') }}
        </div>
    @endif
    <div class="row py-4">
        <div class="col-lg-12">
            <img class="rounded-circle img-fluid profile-img" 
            src="{{$user->avatar == true ? '/uploaded/avatars/'.$user->avatar : '/assets/img/avatar/avatar-13.jpg' }}" alt="avatar" style="height: 150; width: 150">
        </div>
    </div>
    <div class="row flex justify-content-between">
        <form method="post" class="form-group" action="/profile-picture" enctype="multipart/form-data" >
            @csrf
            <div class="col-lg-12">
                <div class="form-group">
                    <input type="file" name="file" class="form-control-file">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Change Profile</button>
                </div>
            </div>
        </form>
        <div class="pt-5 col-lg-3">
            <form action="/profile-picture/{{$user->id}}" method="POST">
                @csrf
                @method('DELETE')
                <div class="form-group">
                    <button class="btn btn-danger text-sm" type="submit">Delete Avatar</button>
                </div>
            </form>
        </div>
    </div>
</div>
