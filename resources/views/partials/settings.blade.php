 <div class="col-lg-8 col-md-7 col-sm-12">
     <div class="my-address contact-2 widget hdn-mb-30">
         <h3 class="heading">Change Password</h3>
         <form action="/settings-password" method="post" enctype="multipart/form-data">
             @csrf
             <div class="row">
                 <div class="col-lg-12 ">
                     <div class="form-group name">
                         <label>Current Password</label>
                         <input type="password" name="current-password" class="form-control" placeholder="Current Password">
                     </div>
                 </div>
                 <div class="col-lg-12">
                     <div class="form-group email">
                         <label>New Password</label>
                         <input type="password" name="new-password" class="form-control" placeholder="New Password">
                     </div>
                 </div>
                 <div class="col-lg-12 ">
                     <div class="form-group subject">
                         <label>Confirm New Password</label>
                         <input type="password" name="confirm-new-password" class="form-control" placeholder="Confirm New Password">
                     </div>
                 </div>
                 <div class="col-lg-12">
                     <div class="send-btn">
                         <button type="submit" class="btn btn-color btn-md btn-message">Send Changes</button>
                     </div>
                 </div>
             </div>
         </form>
     </div>
 </div>