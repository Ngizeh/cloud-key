<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInUp delay-04s">
        <div class="media services-info">
            <i class="flaticon-hotel-building"></i>
            <div class="media-body">
                <h5>Property Management</h5>
                <p>We take care of the stressful management nuances from rent collection to property maintenance, paying bills and finding the right tenants for you.</p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInUp delay-04s">
        <div class="media services-info">
            <i class="flaticon-house"></i>
            <div class="media-body">
                <h5>Room Services</h5>
                <p>We provide additional services such as laundry (bedsheets and towels), internet, gas refill and water to ensure the best stay our tenants.</p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInUp delay-04s">
        <div class="media services-info">
            <i class="flaticon-padlock"></i>
            <div class="media-body">
                <h5>Security</h5>
                <p>We are very particular about the security features and personnel that we provide ensuring maximum security in all our facilities.</p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInUp delay-04s">
        <div class="media services-info">
            <i class="flaticon-call-center-agent"></i>
            <div class="media-body">
                <h5 class="mt-0">Support 24/7</h5>
                <p>Our team is available around the clock to ensure that every client gets the services they need in a timely manner</p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInUp delay-04s">
        <div class="media services-info">
            <i class="flaticon-agreement"></i>
            <div class="media-body">
                <h5>Trusted Agents</h5>
                <p>We vet, handpick and train all our agents to ensure that they maintain professionalism and operate in accordance with our values.</p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInUp delay-04s">
        <div class="media services-info">
            <i class="flaticon-office-block"></i>
            <div class="media-body">
                <h5>Professional Services</h5>
                <p>We  offer a range of services including marketing and advertising, vetting of tenants, remittance of taxes and rent adjustment</p>
            </div>
        </div>
    </div>
</div>