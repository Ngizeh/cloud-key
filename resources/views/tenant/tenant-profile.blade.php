<div class="col-lg-8 col-md-7 col-sm-12">
    <div class="my-address contact-1 widget">
        <h3 class="heading">Personal Details</h3>
        @include('partials.errors')
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif
        <form action="/tenant" method="post">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group email">
                        <label>Your Nationality</label>
                        <input type="text" name="nationality" class="form-control" placeholder="Your Nationality"
                               value="{{ old('nationality', $tenant->nationality) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group subject">
                        <label>ID/Passport Number</label>
                        <input type="text" name="identification_number" class="form-control"
                               placeholder="ID or Passport Number"
                               value="{{ old('identification_number',$tenant->identification_number) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group subject">
                        <label>Phone</label>
                        <input type="text" name="phone" class="form-control" placeholder="Phone"
                               value="{{ old('phone', $tenant->phone) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group subject">
                        <label>Current Residence</label>
                        <input type="text" name="current_residence" class="form-control" placeholder="Current Residence"
                               value="{{ old('current_residence', $tenant->current_residence) }}">
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group">
                        <label>Select Employment Status</label>
                        <select class="form-control selectpicker search-fields" name="employment_status">
                            <option selected disabled>Select Employment Status</option>
                            @foreach (App\Utilities\Employment::all() as $employment)
                             <option value="{{ $employment }}"
                             {{ old('employment_status', $tenant->employment_status) == $employment ? 'selected' : ''}}
                             >{{ ucfirst($employment) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group message">
                        <label>Brief Information about you</label>
                        <textarea class="form-control" name="bio" placeholder="Breif Description about yourself"
                                  required>{{old('bio',$tenant->bio)}}</textarea>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <p><strong>Next Of Kin Details</strong></em></p>
                    <div class="form-group number">
                        <label>Full Name</label>
                        <input type="text" name="next_of_kin_name" class="form-control" placeholder="Full Name"
                               value="{{ old('next_of_kin_name', $tenant->next_of_kin_name) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group number">
                        <label>Email</label>
                        <input type="text" name="next_of_kin_email" class="form-control" placeholder="email"
                               value="{{ old('next_of_kin_email', $tenant->next_of_kin_email) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group number">
                        <label>Phone Number</label>
                        <input type="text" name="next_of_kin_phone" class="form-control" placeholder="Phone Number"
                               value="{{ old('next_of_kin_phone', $tenant->next_of_kin_phone) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group number">
                        <label>Relationship</label>
                        <select class="form-control selectpicker search-fields" name="next_of_kin_relationship">
                            <option selected disabled>Choose the Relationship</option>
                            @include('partials.relationship', ['person' => $tenant])
                        </select>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="send-btn">
                        <button type="submit" class="btn btn-color btn-md btn-message">Save the Changes</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
