@extends('tenant.main')

@section('details')

@include('partials.user_banner', ['type' => 'My Profile'])

<!-- User page start -->
<div class="user-page content-area-14">
    <div class="container">
        <div class="row search-area contact-1">
            <div class="col-lg-4 col-md-5 col-sm-12">
                <div class="user-profile-box mrb">
                      @include('tenant.tenant-menu')
                    </div>
            </div>
            @include('tenant.tenant-profile')
        </div>
    </div>
</div>
<!-- User page end -->


@endsection
