<!--header -->
<div class="header clearfix">
    <h2>{{auth()->user()->name}}</h2>
    <h4>My Profile</h4>
    <img src="{{auth()->user()->avatar == true ? '/uploaded/avatars/'.auth()->user()->avatar  : '/assets/img/avatar/avatar-13.jpg' }}" alt="avatar" class="img-fluid profile-img">
</div>
<!-- Detail -->
<div class="detail clearfix">
    <ul>
        <li>
            <a href="/tenant" class="active">
                <i class="flaticon-user"></i>Profile
            </a>
        </li>

        <li>
            <a href="/my-favorite">
                <i class="flaticon-heart-shape-outline"></i>Favorited Properties
            </a>
        </li>
        <li>
            <a href="/payments">
                <i class="flaticon-locked-padlock"></i>My Payments
            </a>
        </li>
        <li>
            <a href="/tenant-settings">
                <i class="fa fa-cog"></i>Settings
            </a>
        </li>
        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault();  document.getElementById('logout-form').submit();" >
                <i class="flaticon-logout"></i>Log Out
            </a>
            <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</div>
