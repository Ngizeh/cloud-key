@extends('tenant.main')

@section('details')


@include('partials.user_banner', ['type' => 'My Favorite'])

    <div class="user-page content-area-2">
        <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="user-profile-box mrb">
                    @include('tenant.tenant-menu')
                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12">
                <div class="my-properties">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Property</th>
                            <th></th>
                            <th>Date Added</th>
                            <th>Views</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="image">
                                <a href="/properties/show"><img alt="my-properties-3" src="assets/img/my-properties-3.jpg" class="img-fluid"></a>
                            </td>
                            <td>
                                <div class="inner">
                                    <a href="/properties/show"><h2>Modern Family Home</h2></a>
                                    <figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i>Mombasa Road, Nairobi, Kenya,</figure>
                                    <div class="tag price">Ksh. 27,000</div>
                                </div>
                            </td>
                            <td>14.02.2018</td>
                            <td>421</td>
                            <td class="actions">
                                <a href="#" class="edit"><i class="fa fa-pencil"></i>Edit</a>
                                <a href="#"><i class="delete fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="image">
                                <a href="/properties/show"><img alt="my-properties" src="assets/img/my-properties.jpg"></a>
                            </td>
                            <td>
                                <div class="inner">
                                    <a href="/properties/show"><h2>Beautiful Single Home</h2></a>
                                    <figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i>Mombasa Road, Nairobi, Kenya,</figure>
                                    <div class="tag price">Ksh. 315,000</div>
                                </div>
                            </td>
                            <td>4.01.2018</td>
                            <td>266</td>
                            <td class="actions">
                                <a href="#" class="edit"><i class="fa fa-pencil"></i>Edit</a>
                                <a href="#"><i class="delete fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="image">
                                <a href="/properties/show"><img alt="my-properties-2" src="assets/img/my-properties-2.jpg"></a>
                            </td>
                            <td>
                                <div class="inner">
                                    <a href="/properties/show"><h2>Masons Villas</h2></a>
                                    <figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i>Mombasa Road, Nairobi, Kenya,</figure>
                                    <div class="tag price">Ksh. 62,000</div>
                                </div>
                            </td>
                            <td>24.03.2018</td>
                            <td>45</td>
                            <td class="actions">
                                <a href="#" class="edit"><i class="fa fa-pencil"></i>Edit</a>
                                <a href="#"><i class="delete fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="image">
                                <a href="/properties/show"><img alt="my-properties-3" src="assets/img/my-properties-3.jpg"></a>
                            </td>
                            <td>
                                <div class="inner">
                                    <a href="/properties/show"><h2>Modern Family Home</h2></a>
                                    <figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i>Mombasa Road, Nairobi, Kenya,</figure>
                                    <div class="tag price">Ksh. 27,000</div>
                                </div>
                            </td>
                            <td>14.02.2018</td>
                            <td>421</td>
                            <td class="actions">
                                <a href="#" class="edit"><i class="fa fa-pencil"></i>Edit</a>
                                <a href="#"><i class="delete fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="image">
                                <a href="/properties/show"><img alt="my-properties" src="assets/img/my-properties.jpg"></a>
                            </td>
                            <td>
                                <div class="inner">
                                    <a href="/properties/show"><h2>Beautiful Single Home</h2></a>
                                    <figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i>Mombasa Road, Nairobi, Kenya,</figure>
                                    <div class="tag price">Ksh. 315,000</div>
                                </div>
                            </td>
                            <td>4.01.2018</td>
                            <td>266</td>
                            <td class="actions">
                                <a href="#" class="edit"><i class="fa fa-pencil"></i>Edit</a>
                                <a href="#"><i class="delete fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="image">
                                <a href="/properties/show"><img alt="my-properties-2" src="assets/img/my-properties-2.jpg"></a>
                            </td>
                            <td>
                                <div class="inner">
                                    <a href="/properties/show"><h2>Masons Villas</h2></a>
                                    <figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i>Mombasa Road, Nairobi, Kenya,</figure>
                                    <div class="tag price">Ksh. 62,000</div>
                                </div>
                            </td>
                            <td>24.03.2018</td>
                            <td>45</td>
                            <td class="actions">
                                <a href="#" class="edit"><i class="fa fa-pencil"></i>Edit</a>
                                <a href="#"><i class="delete fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-box">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="#"><span aria-hidden="true">«</span></a></li>
                            <li class="page-item"><a class="page-link active" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#"><span aria-hidden="true">»</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    </div>


@endsection
