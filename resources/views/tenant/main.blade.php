@extends('layouts.app')


@section('content')

    @include('partials.header')

    @yield('details')

    @include('partials.footer')

@endsection
