<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Wallet</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="container py-4">
		<div class="row">
			<div class="col-md-6">
				<form method="post" class="form-group" action="/collection" >
					{{csrf_field()}}
					<div class="form-group">
						<label for="amount">Amount</label>
						<input type="text" name="amount" class="form-control" placeholder="" id="amount">
					</div> 

					<div class="form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>


