@extends('layouts.app')

@section('content')

    <div class="login-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-7 overview-bgi cnt-bg-photo cnt-bg-photo-2 d-none d-xl-block d-lg-block d-md-block" style="background-image: url(assets/img/banner-1.jpg)">
                    <div class="login-info">
                        <h3>We make spectacular</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-5 content-box">
                    <div class="content-form-box">
                        <h1 class="login-header">Login</h1>
                        <p>Please enter your user name and password to login</p>
                        <form method="POST" action="/login">
                            @csrf
                            <div class="form-group">
                                <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"
                                       placeholder="Email Address"
                                       required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                                 placeholder="Password"
                                required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="form-check checkbox-theme">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                        Keep Me Signed In
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn bg-dark text-light btn-md btn-block">Login</button>
                            </div>
                          </form>
                            <div class="form-group">
                                <a href="/redirect/facebook" class="text-white btn btn-color btn-md btn-block">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                    Login with Facebook
                                </a>
                                <hr>
                                <a href="/redirect/google" class="btn btn-google btn-md btn-block text-light">
                                    <i class="fa fa-google" aria-hidden="true"></i>
                                    Login with Google
                                </a>
                            </div>
                        <p><a href="/password/reset"><small>Forgotten Password?</small></a></p>
                    </div>
                    <div class="login-footer clearfix">
                        <div class="pull-left">
                            <p class="text-uppercase text-secondary">
                                <strong><a href="/">cloud key</a></strong>
                            </p>
                        </div>
                        <div class="pull-right">
                            <p>Don't have an account?<a href="/register"> Sign Up Now</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Login page end -->
@endsection
