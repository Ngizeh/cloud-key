@extends('layouts.app')

@section('content')

    <div class="register-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-4 cnt-bg-photo d-none d-xl-block d-lg-block d-md-block"
                     style="background-image: url(assets/img/img-31.jpg)">
                    <div class="register-info">
                        <h2 class="text-white text-uppercase">Cloud Key</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                            has</p>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 align-self-center">
                    <div class="content-form-box register-box">
                        <div class="login-header"><h4>Create Your account</h4></div>
                        <form action="/register" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Full Names</label>
                                <input id="name" type="text"
                                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                       name="name" value="{{ old('name') }}"
                                       placeholder="Name"
                                       required autofocus>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Email Address</label>
                                <input id="email" type="email"
                                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       name="email" value="{{ old('email') }}"
                                       placeholder="Email Address"
                                       required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input id="password" type="password"
                                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password"
                                       placeholder="Password"
                                       required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Re-type Password</label>
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation" placeholder="Confirm Password" required>
                                @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('password_confirmation') }}</strong>
								</span>
                                @endif
                            </div>
                            <hr>
                            <div class="form-group">
                                <button type="submit" class="btn bg-dark text-light btn-md btn-block">
                                    Create New Account
                                </button>
                            </div>
                        </form>
                        <small>OR</small>
                        <hr>
                        <div class="form-group ">
                            <a type="button" class="btn btn-color btn-md btn-block text-light"
                               href="/redirect/facebook">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                Sign Up with Facebook</a>
                            <hr>
                            <a type="button" class="btn btn-google btn-md btn-block text-light" href="/redirect/google">
                                <i class="fa fa-google" aria-hidden="true"></i>
                                Login with Google
                            </a>
                            <div class="login-footer text-center">
                                <p>Already have an account?<a href="/login"> Sign In</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="login-footer clearfix">
                        <div class="pull-left">
                            <p class="text-uppercase text-secondary">
                                <strong><a href="/">cloud key</a></strong>
                            </p>
                        </div>
                        <div class="pull-right">
                            <p>Yes, I have an account! <a href="/login">Login</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
    {{--<div class="col-md-8">--}}
    {{--<div class="card">--}}
    {{--<div class="card-header">{{ __('Register') }}</div>--}}

    {{--<div class="card-body">--}}
    {{--<form method="POST" action="{{ route('register') }}">--}}
    {{--@csrf--}}

    {{--<div class="form-group row">--}}
    {{--<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>--}}

    {{--@if ($errors->has('name'))--}}
    {{--<span class="invalid-feedback" role="alert">--}}
    {{--<strong>{{ $errors->first('name') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group row">--}}
    {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>--}}

    {{--@if ($errors->has('email'))--}}
    {{--<span class="invalid-feedback" role="alert">--}}
    {{--<strong>{{ $errors->first('email') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group row">--}}
    {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

    {{--@if ($errors->has('password'))--}}
    {{--<span class="invalid-feedback" role="alert">--}}
    {{--<strong>{{ $errors->first('password') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group row">--}}
    {{--<label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group row mb-0">--}}
    {{--<div class="col-md-6 offset-md-4">--}}
    {{--<button type="submit" class="btn btn-primary">--}}
    {{--{{ __('Register') }}--}}
    {{--</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
@endsection
