@extends('layouts.app')

@section('content')

    <div class="forgot-password">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-8 col-lg-7 col-md-7 overview-bgi cnt-bg-photo d-none d-xl-block d-lg-block d-md-block d-md-block"
                     style="background-image: url(../assets/img/banner-1.jpg)">
                    <div class="login-info">
                        <h3>We make spectacular</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-5 col-md-5 content-box">
                    <div class="content-form-box">
                        <h1 class="login-header">Forgot Password</h1>
                        <p>Please enter your email address to reset your password</p>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form action="{{ route('password.email') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <input type="email"
                                       class="form-control @error('email') is-invalid @enderror"
                                       value="{{ old('email') }}"
                                       required autocomplete="email" autofocus
                                       name="email" placeholder="Email Address">
                            </div>
                            @include('partials.errors')

                            <button type="submit" class="btn bg-primary text-light btn-md btn-block">Forgot Password</button>
                        </form>
                    </div>
                    <div class="login-footer clearfix">
                        <div class="pull-left">
                            <p class="text-uppercase text-secondary">
                                <strong><a href="/">cloud key</a></strong>
                            </p>
                        </div>
                        <div class="pull-right">
                            <p>Back to login?<a href="/login">Login</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
