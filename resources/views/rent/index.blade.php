@extends('layouts.app')

@section('content')

    @include('partials.header')
    <!-- Sub banner start -->
    <div class="sub-banner overview-bgi">
        <div class="container">
            <div class="breadcrumb-area">
                <h1>Rental Payment</h1>
                <ul class="breadcrumbs">
                    <li><a href="/">Home</a></li>
                    <li class="active">Pay Rent</li>
                </ul>
            </div>
        </div>
    </div>

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif

    <div class="container">
    </div>
    <!-- Sub banner end -->
    <div class="container">
        <h2 class="leading pt-4 text-center">Rent Payable</h2>
        <div class="row py-4">
            <div class="col-sm-6 offset-md-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Pay Full Amount</h5>
                        <p class="card-text">Rent amount: Ksh. {{$property->price_format()}}</p>
                        <p class="card-text">Deposit amount: Ksh. {{$property->deposit_format()}}</p>
                        <p class="card-text">Total Amount Payable: Ksh. {{$property->fullAmount()}}</p>

                        <div class="row">
                            <div class="col-md-4">
                                <p class="card-text">Pay with Card</p>
                            </div>
                            <div class="col-md-4">
                                <p class="card-text">
                                    <a data-isw-payment-button=""
                                       data-isw-ref="PRIVAT0001">
                                        <script
                                            data-isw-amount="100"
                                            data-isw-channel="WEB"
                                            data-isw-currencyCode="KES"
                                            {{-- Customer infor --}}
                                            data-isw-customerInfor="1002|kelvin|mwangi| kelvin.mwangi@interswitchgroup.com |0714171282|NBI|KE|00200|wstlnds|NBI"
                                            data-isw-dateOfPayment="2016-09-05T10:20:26"
                                            data-isw-domain="ISWKE"
                                            data-isw-fee="0"
                                            data-isw-merchantCode="PRIVAT0001"
                                            data-isw-narration="Payment"
                                            data-isw-orderId="{{substr(md5(mt_rand()), 0,7)}}"
                                            data-isw-preauth="0"
                                            {{-- Dashboard for successful Payment --}}
                                            data-isw-redirectUrl="{{route('my-properties')}}"
                                            data-isw-terminalId="3TLP0001"
                                            data-isw-transactionReference="{{$property->id}}"
                                            src="https://testmerchant.interswitch-ke.com/webpay/button/functions.js" type="text/javascript">
                                        </script>
                                    </a>
                            </div>
                        </div>
                        <!-- <a href="#" data-toggle="modal" data-target="#exampleModal" class="btn btn-success pull-right">Lipa na Mpesa</a> -->
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 py-4">
                        <a href="{{URL::previous()}}"><i class="fa 2x fa-chevron-circle-left"></i> Back</a>
                    </div>
                </div>
            </div>
        <!--   <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Pay in Installments</h5>
        <p class="card-text">Rent amount: Ksh. {{$property->price_format()}}</p>
        <p class="card-text">Deposit amount: Ksh. {{$property->installment()}}</p>
        <p class="card-text">Total Amount Payable: Ksh. {{$property->rentalAndInstallment()}}</p>
        <a href="#" class="btn btn-primary">Pay with Card</a>
        <a href="#" class="btn btn-success pull-right">Lipa na Mpesa</a>
      </div>
    </div>
  </div> -->
        {{-- </div>
        </div> --}}

        <!-- Button trigger modal -->
            <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
              Launch demo modal
            </button -->

            <!-- Modal -->
        <!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
     <form method="post" action="/tujenge" >
         @csrf
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pay Rent with Mpesa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Amount Payable: Ksh. {{$property->fullAmount()}}</p>
        <p>Make sure you phone is unlocked on the screen</p>
        <div class="form-group">
             <label for="phone_number"></label>
             <input type="hidden" name="amount" value="{{$property->total()}}">
              <input type="text" name="phoneNumber" class="form-control" placeholder="Enter Phone Number" id=phone_number>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Lipa na Mpesa</button>
      </div>
    </div>
  </form>
  </div>
</div> -->

            @section('scripts.footer')
                <script>
                    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
                </script>
@endsection
@endsection
