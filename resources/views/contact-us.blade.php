@extends('layouts.app')

@section('content')

@include('partials.header')

  <!-- Sub banner start -->

 @include('partials.banner', ['title' => 'Contact Us', 'type' =>'Contact Us' ])
<!-- Sub banner end -->

<!-- Contact 1 start -->
<contact-page flash="{{ Session::get('success') }}"></contact-page>
<!-- Contact 1 end -->
@include('partials.footer')
@endsection
