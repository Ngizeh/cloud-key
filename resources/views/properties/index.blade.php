@extends('layouts.app')

@section('content')

@include('partials.header')

<!-- Sub banner 2 start -->

@include('partials.banner', ['title' => 'Our Property', 'type' =>'Our Property' ])

<!-- Sub banner 2 end -->


<!-- Properties list rightside start -->
<div class="properties-list-rightside content-area-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="option-bar d-none d-xl-block d-lg-block d-md-block d-sm-block">
                    <div class="row clearfix">
                        <div class="col-xl-4 col-lg-5 col-md-5 col-sm-5">
                            <h4>
                                <span class="heading-icon">
                                        <!-- <i class="fa fa-caret-right icon-design"></i>
                                        <i class="fa fa-th-large"></i> -->
                                </span>
                                <span class="heading">Properties</span>
                            </h4>
                        </div>

                        <div class="col-xl-8 col-lg-7 col-md-7 col-sm-7">
                            <div class="sorting-options clearfix">
                                <!-- <a href="properties-list-rightside.html" class="change-view-btn"><i class="fa fa-th-list"></i></a>
                                <a href="properties-grid-rightside.html" class="change-view-btn active-view-btn"><i class="fa fa-th-large"></i></a> -->
                            </div>
                            @if (count($properties))
                                <div class="search-area">
                                    <select class="selectpicker search-fields" name="location">
                                        <option>High to Low</option>
                                        <option>Low to High</option>
                                    </select>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
              <!--   <div class="subtitle">
                    20 Result Found
                </div> -->
                <div class="row">
                    @forelse($properties as $property)
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="property-box">
                            <div class="property-thumbnail">
                                <a href="/property/{{$property->slug}}" class="property-img">
                                    <div class="tag button alt featured">Vacant</div>
                                    <div class="price-ratings-box">
                                        <p class="price">
                                            Ksh. {{ $property->price + $property->service_charge }}
                                        </p>
                                    </div>
                                    <x-image :property="$property" class="img=fluid"></x-image>
                                </a>
                            </div>
                            <div class="detail">
                                <h1 class="title">
                                    <a href="/property/{{$property->slug}}">{{$property->title}}</a>
                                </h1>
                                <div class="location">
                                    <a href="/property/{{$property->slug}}">
                                        <i class="fa fa-map-marker"></i>{{$property->street}}, {{$property->town}},
                                    </a>
                                </div>
                                <ul class="facilities-list clearfix">
                                    <li>
                                        <i class="flaticon-bed"></i>  {{$property->bedroom}} Bedrooms
                                    </li>
                                    <li>
                                        <i class="flaticon-bath"></i> {{$property->bathroom}} Bathrooms
                                    </li>
                                    <li>
                                        <i class="flaticon-square-layouting-with-black-square-in-east-area"></i> Sq Ft:{{$property->area}}
                                    </li>
                                  <!--   <li>
                                        <i class="flaticon-car-repair"></i> 1 Garage
                                    </li> -->
                                </ul>
                            </div>
                            <div class="footer">
                                <a href="#">
                                    <i class="fa fa-user"></i> {{$property->user->name}}
                                </a>
                                <span>
                                <i class="fa fa-calendar-o"></i> {{ $property->formatted_created_at}}
                            </span>
                            </div>
                        </div>
                    </div>
                    @empty
                    <h3 class="ml-4">No properties yet</h3>
                    @endforelse
                    <div class="col-lg-12">
                        <div class="pagination-box hidden-mb-45">
                            {{ $properties->links('partials.pagination') }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="sidebar mbl">
                    @if(count($properties) > 0)
                         @include('partials.sidebar')
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Properties list rightside end -->

@include('partials.footer')

@endsection
