@extends('layouts.app')

@section('content')

@include('partials.header')


<!-- Sub banner 2 start -->

@include('partials.banner', ['title' => 'Property Details', 'type' =>'Property Details' ])

<!-- Sub banner 2 end -->

<!-- Properties details page start -->
<div class="properties-details-page content-area-15">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 col-xs-12 slider">
                <div id="propertiesDetailsSlider" class="carousel properties-details-sliders slide mb-60">
                    <div class="heading-properties">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-left">
                                    <h3>{{$property->title}}</h3>
                                    <p><i class="fa fa-map-marker"></i> {{$property->address}}, {{$property->county}},{{$property->town}}</p>
                                </div>
                                <div class="p-r">
                                    <h3>Ksh. {{number_format($property->price)}}</h3>
                                    <p><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- main slider carousel items -->
                    <div class="carousel-inner">
                    @foreach( $property->photos as $set )
                        <div class="item carousel-item {{ $loop->first ? 'active' : '' }}" data-slide-number="{{$loop->index}}">
                            <img src="/{{$set->path}}" class="img-fluid" alt="{{ $property->title}}">
                        </div>
                    @endforeach
                        <a class="carousel-control left" href="#propertiesDetailsSlider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                        <a class="carousel-control right" href="#propertiesDetailsSlider" data-slide="next"><i class="fa fa-angle-right"></i></a>
                    </div>
                    <!-- main slider carousel nav controls -->
                    <ul class="carousel-indicators smail-properties list-inline nav nav-justified">
                    @foreach( $property->photos as $set )
                        <li class="list-inline-item {{ $loop->first ? 'active' : '' }}">
                            <a id="carousel-selector-0" class="selected" data-slide-to="{{$loop->index}}" data-target="#propertiesDetailsSlider">
                                <img src="/{{$set->thumbnail_path}}" class="img-fluid" alt="{{ $property->title}}">
                            </a>
                        </li>
                    @endforeach
                    </ul>
                </div>
                    <!-- Tabbing box start -->
                <div class="tabbing tabbing-box mb-60">
                    <ul class="nav nav-tabs" id="carTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="one" aria-selected="false">Description</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="three" aria-selected="true">Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="6-tab" data-toggle="tab" href="#6" role="tab" aria-controls="6" aria-selected="true">Related Properties</a>
                        </li>
                        <li class="nav-item">
                            <a href="/rental/{{$property->slug}}" class="nav-link">Rent Now</a>
                        </li>
                        <li class="nav-item">
                            <a href="/rental/{{$property->slug}}" class="nav-link">Book to Move in Later</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="carTabContent">
                        <div class="tab-pane fade active show" id="one" role="tabpanel" aria-labelledby="one-tab">
                            <h3 class="heading">Property Description</h3>
                           <p>{{$property->description}}</p>
                        </div>
                        <div class="tab-pane fade " id="three" role="tabpanel" aria-labelledby="three-tab">
                            <div class="property-details">
                                <h3 class="heading">Property Details</h3>
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <ul>
                                            <li>
                                                <strong>Price:</strong>Ksh. {{$property->price}}/ Month
                                            </li>
                                            <li>
                                                <strong>Property Type:</strong>{{$property->type}}
                                            </li>
                                            <li>
                                                <strong>Bathrooms:</strong>{{$property->bathroom}}
                                            </li>
                                            <li>
                                                <strong>Bedrooms:</strong>{{$property->bedroom}}
                                            </li>
                                            <li>
                                                <strong>Property Lot Size:</strong>{{$property->area}} ft2
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <ul>
                                            <li>
                                                <strong>County:</strong>{{$property->county }}
                                            </li>
                                            <li>
                                                <strong>Land area:</strong>230 ft2
                                            </li>
                                            <li>
                                                <strong>Year Built:</strong>2018
                                            </li>
                                            <li>
                                                <strong>Available From:</strong>2018
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <ul>
                                            <li>
                                                <strong>City:</strong>{{$property->town}}
                                            </li>
                                            <li>
                                                <strong>Parking:</strong>Yes
                                            </li>
                                            <li>
                                                <strong>Property Owner:</strong>{{$property->user->name}}
                                            </li>
                                            <li>
                                                <strong>Zip Code: </strong>{{$property->postal_code}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade " id="6" role="tabpanel" aria-labelledby="6-tab">
                            <div class="related-properties">
                                <h3 class="heading">Related Properties</h3>
                                <div class="row">
                                    @foreach ($properties->slice(0, 2) as $property)
                                        <div class="col-md-6">
                                            <div class="property-box">
                                                <div class="property-thumbnail">
                                                    <a href="properties-details.html" class="property-img">
                                                        <div class="tag button alt featured">Featured</div>
                                                        <div class="price-ratings-box">
                                                            <p class="price">
                                                                Ksh. {{ $property->price }}
                                                            </p>
                                                            <div class="ratings">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </div>
                                                        </div>
                                                        <x-image :property="$property" class="img-fluid" alt="{{ $property->title }}"> </x-image>
                                                    </a>
                                                </div>
                                                <div class="detail">
                                                    <h1 class="title">
                                                        <a href="/property/{{$property->slug}}">{{ $property->truncatedTitle() }}</a>
                                                    </h1>
                                                    <div class="location">
                                                        <a href="properties-details.html">
                                                            <i class="fa fa-map-marker"></i>{{ $property->address}} {{ $property->town}} {{ $property->county}},
                                                        </a>
                                                    </div>
                                                    <ul class="facilities-list clearfix">
                                                        <li>
                                                            <i class="flaticon-square-layouting-with-black-square-in-east-area"></i> {{ $property->bathroom }} Bedrooms
                                                        </li>
                                                        <li>
                                                            <i class="flaticon-bath"></i> {{ $property->bathroom}} Bathrooms
                                                        </li>
                                                        <li>
                                                            <i class="flaticon-square-layouting-with-black-square-in-east-area"></i> Sq Ft:{{ $property->area }}
                                                        </li>
                                                        <li>
                                                            <i class="flaticon-car-repair"></i> {{ $property->units }} Units
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="footer">
                                                    <a href="#">
                                                        <i class="fa fa-user"></i> {{ $property->user->name }}
                                                    </a>
                                                    <span>
                                                        <i class="fa fa-calendar-o"></i> {{ $property->createdDate() }}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Amenities box start -->
                <div class="amenities-box mb-60">
                    <h3 class="heading">Condition</h3>
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <ul>
                                <li><span><i class="flaticon-bed"></i> {{ $property->bathroom }} Beds</span></li>
                                <li><span><i class="flaticon-bath"></i>{{ $property->bathroom }} Bathroom</span></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <ul>
                                <li><span><i class="flaticon-car-repair"></i> 1 Garage</span></li>
                                <li><span><i class="flaticon-balcony-and-door"></i>1 Balcony</span></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <ul>
                                <li><span><i class="flaticon-square-layouting-with-black-square-in-east-area"></i> {{ $property->area }} sq ft</span></li>
                                <li><span><i class="flaticon-monitor"></i> TV</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Features opions start -->
                <div class="features-opions mb-60">
                    <h3 class="heading">Features</h3>
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <ul>
                                <li>
                                    <i class="flaticon-air-conditioner"></i>
                                    Air conditioning
                                </li>
                                <li>
                                    <i class="flaticon-wifi-connection-signal-symbol"></i>
                                    Wifi
                                </li>
                                <li>
                                    <i class="flaticon-swimmer"></i>
                                    Swimming Pool
                                </li>
                                <li>
                                    <i class="flaticon-bed"></i>
                                    Double Bed
                                </li>
                                <li>
                                    <i class="flaticon-balcony-and-door"></i>
                                    Balcony
                                </li>

                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <ul>
                                <li>
                                    <i class="flaticon-old-typical-phone"></i>
                                    Telephone
                                </li>
                                <li>
                                    <i class="flaticon-car-repair"></i>
                                    Garage
                                </li>
                                <li>
                                    <i class="flaticon-parking"></i>
                                    Parking
                                </li>
                                <li>
                                    <i class="flaticon-monitor"></i>
                                    TV
                                </li>
                                <li>
                                    <i class="flaticon-theatre-masks"></i>
                                    Home Theater
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <ul>
                                <li>
                                    <i class="fa fa-clock-o"></i>
                                    Alarm
                                </li>
                                <li>
                                    <i class="flaticon-padlock"></i>
                                    Security
                                </li>
                                <li>
                                    <i class="flaticon-weightlifting"></i>
                                    Gym
                                </li>
                                <li>
                                    <i class="flaticon-idea"></i>
                                    Electric Range
                                </li>
                                <li>
                                    <i class="flaticon-green-park-city-space"></i>
                                    Private space
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="sidebar mbl">
                    @include('partials.sidebar')
                    <!-- Social list start -->
                    <div class="social-list widget clearfix">
                        <h5 class="sidebar-title">Follow Us</h5>
                        <ul>
                            <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" class="rss-bg"><i class="fa fa-rss"></i></a></li>
                            <li><a href="#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>

                    <!-- Helping center start -->
                    <div class="helping-center widget clearfix">
                        <div class="media">
                            <i class="fa fa-mobile"></i>
                            <div class="media-body  align-self-center">
                                <h5 class="mt-0">Helping Center</h5>
                                <h4><a href="#">+254 715 008 946</a></h4>
                            </div>
                        </div>
                    </div>

                    <!-- Financing calculator  start -->
                    <div class="contact-1 financing-calculator widget">
                        <h5 class="sidebar-title">Mortgage Calculator</h5>
                        <form action="#" method="GET" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="form-label">Property Price</label>
                                <input type="text" class="form-control" placeholder="Ksh. 36.400">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Interest Rate (%)</label>
                                <input type="text" class="form-control" placeholder="10%">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Period In Months</label>
                                <input type="text" class="form-control" placeholder="10 Months">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Down PaymenT</label>
                                <input type="text" class="form-control" placeholder="Ksh. 21,300">
                            </div>
                            <br>
                            <div class="form-group mb-0">
                                <button type="submit" class="btn btn-primary btn-md btn-message btn-block">Calculate</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Properties details page end -->

@include('partials.footer')

@endsection
