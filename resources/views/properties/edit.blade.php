@extends('layouts.app')

@section('content')


<!-- Sub banner 2 start -->

@include('partials.banner', ['title' => 'Edit Property', 'type' =>'Edit Property' ])

<!-- Sub banner 2 end -->

<!-- User page start -->
<div class="user-page content-area-7">
    <div class="container">
        <div class="row">
                <div class="search-area contact-1">
                    <div class="search-area-inner">
                        <div class="search-contents ">
                            <h3 class="heading">Basic Information</h3>
                              @if(Session::has('success'))
                                    <div class="alert alert-success">
                                        {{ Session::get('success') }}
                                    </div>
                            @endif
                            <form method="post" action="/property/{{$property->id}}">
                                @method('PATCH')
                                 @include('form_partial.form')
                               <div class="row">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-md btn-primary">Update Property</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- User page end -->

@include('partials.footer')

@endsection
