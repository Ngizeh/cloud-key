@extends('layouts.app')

@section('content')

    @include('partials.header')

    <!-- Sub banner 2 start -->

    @include('partials.banner', ['title' => 'Upload Image', 'type' =>'Upload Property Image' ])

    <!-- Sub banner 2 end -->

    <!-- Sub banner 2 end -->
    <div class="container py-4">
        <div class="box">
            @foreach($property->photos->chunk(3) as $set)
                <div class="row">
                    @foreach($set as $photo)
                        <div class="col-md-4 col-lg-4 pt-4">
                            <form method="post" action="/photo/{{$photo->id}}">
                                @csrf
                                {{ method_field('DELETE')}}
                                <button type="submit" class="float-left -pl-4 btn btn-black">Delete</button>
                            </form>
                            <img src="/{{$photo->thumbnail_path}}">
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
        <div class="box py-4">
            <h3 class="heading">Property Gallery</h3>
            <form id="myDropzone" action="/photo/{{$slug}}/files"
                  class="dropzone" method="post" enctype="multipart/form-data">
                @csrf
            </form>
        </div>
        @if ($errors->has('description'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('file') }}</strong>
            </span>
        @endif

        <div class="py-4">
            <div class="row">
                <div class="col-md-6">
                    <a href="javascript:history.back()" class="btn btn-info">Back</a>
                </div>
                <div class="col-md-6">
                    <a href="/property/{{$property->slug}}" class="btn btn-primary">Publish</a>
                </div>
            </div>
        </div>
    </div>

    @section('scripts.footer')
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
        <script>
            Dropzone.options.myDropZone = {
                paramName: 'file',
                maxFilesize: 15.0,
                acceptedFiles: '.jpg,.jpeg,.png, .bmp, .webp'
            };
        </script>
    @stop
@endsection

