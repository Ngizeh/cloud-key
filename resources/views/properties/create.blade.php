@extends('layouts.app')

@section('content')

   @include('partials.header')

   <!-- Sub banner 2 start -->

    @include('partials.banner', ['title' => 'Submit Property', 'type' =>'Submit Property' ])

    <!-- Sub banner 2 end -->

    <!-- User page start -->
    <div class="user-page content-area-7">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="search-area contact-1">
                        <div class="search-area-inner">
                            <div class="search-contents ">
                                <h3 class="heading">Basic Information</h3>
                                <form method="post" action="/property">
                                    @include('form_partial.form')
                                    <div class="row">
                                        <div class="col-lg-6 col-4">
                                            <a href="{{url()->previous()}}" class="btn btn-link">
                                                <i class="fa fa-chevron-left"></i>
                                                Back
                                            </a>
                                        </div>
                                        <div class="col-lg-6 col-4">
                                            <button type="submit" class="text-white btn btn-md btn-color">Continue</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- User page end -->

    @include('partials.footer')

@endsection

