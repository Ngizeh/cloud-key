@extends('agent.main')

@section('details')

 @include('partials.user_banner', ['type' => 'My Properties'])

    <div class="user-page content-area-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="user-profile-box mrb">
                        @include('agent.agent-menu')
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12">
                    @if(count($properties))
                        <div class="my-properties">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Property</th>
                                    <th></th>
                                    <th>Created</th>
                                    <th>Views</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                @foreach( $properties as $property )
                                    <tbody>
                                    <tr>
                                        <td class="image">
                                            <a href="/property/{{$property->slug}}">
                                                <x-image alt="my-properties-3" :property="$property" class="img-fluid"></x-image>
                                            </a>
                                        </td>
                                        <td>
                                            <div class="inner">
                                                <a href="/property/{{$property->slug}}"><h2>{{$property->truncatedTitleWidget()}}</h2></a>
                                                <figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i>
                                                    <small>{{$property->address}}</small>
                                                </figure>
                                                <div class="tag priKsh.e">Ksh. {{$property->price}}</div>
                                            </div>
                                        </td>
                                        <td>{{$property->created_at}}</td>
                                        <td>421</td>
                                        <td class="actions">
                                            <a href="/property/{{$property->id}}/edit" class="edit"><i class="fa fa-pencil" title="Edit"></i>Edit</a>
                                            <a href="#"><i class="delete fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                @endforeach
                            </table>
                        </div>
                        <div class="pagination-box">
                            {{ $properties->links('partials.pagination') }}
                        </div>
                    @else
                        <div class="my-address contact-2 widget hdn-mb-30">
                            <h3 class="heading">All Properties</h3>
                            <p class="leading">You Don't have Properties yet</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
