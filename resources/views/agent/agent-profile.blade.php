<div class="col-lg-8 col-md-7 col-sm-12">
    <div class="my-address contact-1 widget">
        <h3 class="heading">Personal Details</h3>
        @include('partials.errors')
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif
        <form action="/agent" method="post">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group email">
                        <label>Your Nationality</label>
                        <input type="text" name="nationality" class="form-control" placeholder="Your Nationality"
                               value="{{ old('nationality', $agentProfile->nationality) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group subject">
                        <label>ID/Passport Number</label>
                        <input type="text" name="identification_number" class="form-control"
                               placeholder="ID or Passport Number"
                               value="{{ old('identification_number',$agentProfile->identification_number) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group subject">
                        <label>Phone</label>
                        <input type="text" name="phone" class="form-control" placeholder="Phone"
                               value="{{ old('phone', $agentProfile->phone) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group subject">
                        <label>Company Name (Optional)</label>
                        <input type="text" name="company" class="form-control" placeholder="Company Name"
                               value="{{ old('company', $agentProfile->company) }}">
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group subject">
                        <label>Physical Location</label>
                        <input type="text" name="physical_location" class="form-control"
                               placeholder="Where can we find you"
                               value="{{ old('physical_location', $agentProfile->physical_location) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group subject">
                        <label>No. of years in Real Estate </label>
                        <input type="number" name="years" class="form-control"
                               placeholder="How long have been in real estate" value="{{ old('years', $agentProfile->years) }}"
                               required>
                    </div>
                </div>

                <div class="col-lg-12 ">
                    <div class="form-group">
                        <label>Select Employment Status</label>
                        <select class="form-control selectpicker search-fields" name="employment_status" required>
                            <option selected disabled>Select Employment Status</option>
                            @foreach (App\Utilities\Employment::all() as $employment)
                            <option value="{{ $employment }}"
                            {{ old('employment_status', $agentProfile->employment_status) == $employment ? 'selected' : ''}}
                            >{{ ucfirst($employment) }}</option>
                           @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group message">
                        <label>Brief Information about you</label>
                        <textarea class="form-control" name="bio" placeholder="Brief Description about yourself or the company you work on"
                                  required>{{old('bio',$agentProfile->bio)}}</textarea>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <p><strong>Bank Details</strong></p>
                    <div class="form-group number">
                        <label>Name of your Bank</label>
                        <input type="text" name="bank_name" class="form-control" placeholder="Enter receiving Bank Name"
                               value="{{ old('bank_name', $agentProfile->bank_name) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group number">
                        <label>Bank Account</label>
                        <input type="text" name="bank_account" class="form-control" placeholder="Enter the Bank Account Number"
                               value="{{ old('bank_account', $agentProfile->bank_account) }}" required>
                    </div>
                </div>
                <div class="col-lg-12 ">
                    <div class="form-group number">
                        <label>Account Branch</label>
                        <input type="text" name="account_branch" class="form-control" placeholder="Enter the Account Branch"
                               value="{{ old('account_branch', $agentProfile->account_branch) }}" required>
                    </div>
                </div>

                 <div class="col-lg-12 ">
                    <div class="form-group number">
                        <label>Swift Code</label>
                        <input type="text" name="swift_code" class="form-control" placeholder="Enter the Swift code"
                               value="{{ old('swift_code', $agentProfile->swift_code) }}" required>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="send-btn">
                        <button type="submit" class="btn btn-color btn-md btn-message">Save the Changes</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
