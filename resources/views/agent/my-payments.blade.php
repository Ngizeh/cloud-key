@extends('admin.main')

@section('details')

@include('partials.user_banner', ['type' => 'My Payments'])

  <div class="user-page content-area-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="user-profile-box mrb">
                  @include('agent.agent-menu')
                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12">
                <div class="my-properties">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Property Name</th>
                                <th>Amount Payed</th>
                                <th>Balance</th>
                                <th>Payment Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <a href="#"><h2>Modern Family Home</h2></a>
                                </td>
                                <td>
                                    <div class="inner">
                                        <div class="tag priKsh.e">Ksh. 27,000</div>
                                    </div>
                                </td>
                                <td>421</td>
                                <td>14.02.2018</td>

                            </tr>
                            <tr>
                                <td>
                                   <a href="#"><h2>Modern Family Home</h2></a>
                                </td>
                                <td>
                                    <div class="inner">
                                        <div class="tag price">Ksh. 315,000</div>
                                    </div>
                                </td>
                                <td>266</td>
                                <td>4.01.2018</td>

                            </tr>
                            <tr>
                                <td>
                                   <a href="#"><h2>Modern Family Home</h2></a>
                                </td>
                                <td>
                                    <div class="inner">
                                        <div class="tag price">Ksh. 62,000</div>
                                    </div>
                                </td>
                                <td>45</td>
                                <td>24.03.2018</td>

                             </tr>
                            <tr>
                                <td>
                                     <a href="#"><h2>Modern Family Home</h2></a>
                                </td>
                                <td>
                                    <div class="inner">
                                        <div class="tag price">Ksh. 27,000</div>
                                    </div>
                                </td>
                                <td>421</td>
                                <td>14.02.2018</td>

                            </tr>
                            <tr>
                                <td>
                                   <a href="#"><h2>Modern Family Home</h2></a>
                                </td>
                                <td>
                                    <div class="inner">
                                        <div class="tag priKsh.e">Ksh. 315,000</div>
                                    </div>
                                </td>
                                <td>266</td>
                                <td>4.01.2018</td>

                            </tr>
                            <tr>
                                <td class="#">
                                     <a href="#"><h2>Modern Family Home</h2></a>
                                </td>
                                <td>
                                    <div class="inner">
                                        <div class="tag price">Ksh. 62,000</div>
                                    </div>
                                </td>
                                <td>45</td>
                                <td>24.03.2018</td>

                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-box">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="#"><span aria-hidden="true">«</span></a></li>
                            <li class="page-item"><a class="page-link active" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#"><span aria-hidden="true">»</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
