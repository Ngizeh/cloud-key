@extends('layouts.app')

@section('content')

    <div class="container" style="padding-top: 100px">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="text-light">Tell us who you are</h3>
                        </div>
                        <div class="card-body m-4">
                            <h5 class="card-title">Select who you are </h5>
                            <form action="/role" method="post">
                                @csrf
                                <div class="form-group">
                                    <select name="role" class="form-control" id="role">
                                        <option selected disabled>Choose a Category</option>
                                        @foreach(App\Utilities\Role::all() as $role)
                                            @if($role !== 'admin')
                                            <option value="{{$role}}" {{old('role') == $role ? 'selected' : ''}}>
                                                {{ucwords($role)}}
                                            </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <div class="form-check checkbox-theme">
                                        <input class="form-check-input" type="checkbox" name="terms"  id="yes" value="yes">
                                        <label class="form-check-label" for="yes">
                                            <a href="" data-toggle="modal" data-target="#exampleModal">Terms and Conditions</a>
                                        </label>
                                    </div>
                                </div>
                                <div class="py-4">
                                    <button type="submit" class="btn btn-primary" >Almost there</button>
                                </div>
                            </form>
                            @include('partials.errors')
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Terms and Conditions</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto minus, quas velit, laboriosam quod debitis, recusandae illo quae, eius omnis in consequuntur voluptatem harum odit id quaerat similique et! A!</p>
                                    <p>Deserunt pariatur placeat maxime saepe modi, culpa quisquam delectus aut fugiat, eaque distinctio repellendus nesciunt aliquid dignissimos quae quo. Voluptatibus reiciendis totam expedita quos repellendus ut facilis cum modi tempore.</p>
                                    <p>Laboriosam perferendis aliquid in consequuntur nihil mollitia dolorem harum voluptatem esse, commodi quos molestiae quia necessitatibus illum saepe adipisci dolore dignissimos. Cum repellat voluptatem doloribus ratione dignissimos dolores unde perspiciatis.</p>
                                    <p>Magnam accusamus itaque velit culpa vitae nihil quidem nisi porro error recusandae voluptatibus, cupiditate delectus unde repudiandae provident commodi sequi officia mollitia, iste aliquid odit! Quisquam, corrupti nostrum labore incidunt!</p>
                                    <p>Dignissimos quia magni tempore maxime eum omnis consequatur veritatis nobis quisquam impedit, optio vero aperiam minima distinctio, dolor dolorem totam reiciendis expedita excepturi inventore recusandae atque nesciunt fuga error. Adipisci.</p>
                                    <p>Porro maiores inventore omnis soluta natus, dolorem magni placeat exercitationem ullam eos, aliquid deserunt a. Soluta veniam eligendi molestias inventore eveniet debitis architecto sunt labore quis consequuntur quasi, veritatis eos!</p>
                                    <p>Hic nobis quos iure. Odio modi distinctio aperiam repellat nam labore quasi asperiores libero ipsum harum. Maiores aliquid facere, praesentium odit minima ab nam reiciendis, quam aperiam, provident dicta sapiente!</p>
                                    <p>Nisi minima quis maiores optio consequatur neque fugiat eos ea, repudiandae, alias laboriosam sit, minus a fugit. Aliquam voluptates nam soluta dicta officiis, delectus doloribus deserunt eos cumque vitae corporis.</p>
                                    <p>Quo quia, odio minima. Reiciendis similique a, doloribus ad voluptatum, non deleniti ipsum officiis id inventore distinctio est mollitia ducimus, animi, maxime debitis asperiores? Repellat adipisci harum, quos nemo eius?</p>
                                    <p>Ratione sint sed, iure architecto aspernatur sit, blanditiis, deserunt quae vero quasi repellat! Ipsa non, sint temporibus neque sit aliquid modi vel nostrum rerum eveniet, a consequuntur nihil ex consectetur.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

@endsection

