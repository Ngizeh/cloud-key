@foreach ($property->photos as $set)
   @if($loop->first)
        <img src="/{{ $set->thumbnail_path }}" {{ $attributes }}>
  @endif
@endforeach
