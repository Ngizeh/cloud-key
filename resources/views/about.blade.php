@extends('layouts.app')

@section('content')

@include('partials.header')

<!-- Sub banner start -->
@include('partials.banner', ['title' => 'About Us', 'type' =>'About Us' ])

<!-- Sub banner end -->

<!-- About us start -->
<div class="about-us content-area-8 bg-white">
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <h3 class="leading">About Us</h3>
            <hr>
            <p>
                CloudKey is a rental portal that provides renters with a wide choice of homes that can be viewed and rented easily online. Renters are able to select their preferred budget, home type, location, distance from office, to match individual tastes, and they are then able to access a wide selection of home options quickly, view, select, pay and move into their new home.
            </p>

            <p>We offer among the widest selection of properties in the cities and towns we cover. </p>

            <p>Our Landlords are able to access tenants easily, as they can list their properties for free and only pay when a tenant takes up their property.</p>

            <h3 class="leading">Services to the Landlords</h3>
            <hr>
            <p>
                Marketing and Advertising services for your property: we employ resources both online and offline to reach as many potential tenants as possible 
            </p>
            <p>
                Vetting of tenants: this is a painstaking exercise that we ensure we not only get you a tenant but also get the right one that will pay rent on time and take good care of your property.
            </p>
            <p>
                Ensuring all utilities are paid for on time: you have stories of collusion between tenants and employees of utility companies not to pay water and electricity, at Cloudkey we ensure this is done on time 
            </p>
            <p>>Payment of rates and rents: as a landlord you don’t have time to queue and hassle county officials for rate payment, we do that for you at Cloudkey </p>

            <p>Maintenance of your property: we partner with top maintenance electrician, plumbers, masons to ensure that your property is in top condition whenever a tenant exits </p>

            <p>Remitting of taxes: the government introduced tax on rental properties, at Cloudkey w handle this for you</p>

               <p>Rent adjustments: Our experience in the property market arms us with valuable information on the trend of rents in particular area and we are able to negotiate increases with ease with continuing tenants </p>

              <h3 class="leading"> Services to tenants</h3>
              <hr>

            <p>Variety and choice on our platform you will find a wide range of properties in your area of choice and within your budget. We additionally pinpoint important amenities within such as schools, hospitals, malls etc. At Cloudkey you will save valuable time that would have been spent on physical visits of properties </p>

            <p>3D View of the property, we ensure that we bring your view of the property as close as possible to the reality with our state of the art 3D video images, this will save you time spent on physical viewing </p>

            <p>Negotiate favorable rent and terms with landlords, our experience and rapport with landlords enables us to get you the most competitive rent in a particular area. We are also to ensure that rental increments and done fairly and ample notice is given before effecting</p>

            <p>Sharing of valuable information with potential tenants: Our wide experience with various neighborhoods arms with important information that may not privy to potential tenant e.g. security, public transportation, water and electricity reliability etc. we believe these are important considerations for any home seekers and require unbiased feedback that we are able to provide </p>

            <p> Scheduled and timely maintenance cycle; we ensure that your home has fresh coat of paint, water and electrical fittings are in good working conditions before you move in. Additionally whenever there’s a breakdown as a result of wear and tear we engage technicians from our wide pool of professionals </p>
            </div>
    </div>
</div>
</div>


<div class="about-us content-area-8 bg-white">
    {{-- <div class="container"> --}}
        <div class="row mx-2">
            <div class="col-lg-6 offset-md-3 align-self-center">
                <div class="about-text more-info">
                    <h3>Why Cloud Key?</h3>
                    <div id="faq" class="faq-accordion">
                        <div class="card m-b-0">
                            <div class="card-header">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse1">
                                   Creative Marketing
                                </a>
                            </div>
                            <div id="collapse1" class="card-block collapse">
                                <p>Cloud key will sponsor all offline and online marketing activities which will include all featured properties on the website..</p>
                            </div>

                            <div class="card-header">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse2">
                                  Free Listing
                                </a>
                            </div>
                            <div id="collapse2" class="card-block collapse">
                                <p>Cloud key will not have any monthly or annual fees for property Listings.</p>
                            </div>

                            <div class="card-header">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse3">
                                    Prompt Payments
                                </a>
                            </div>
                            <div id="collapse3" class="card-block collapse">
                                <p>Commission payable within 24hrs for all complete transaction through Cloud key.</p>
                            </div>

                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    Hustle free Tenants
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <p>Cloud key agents will be not have a limit to there no of listings.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{-- </div> --}}
    </div>
</div>
<div class="about-us content-area-8 bg-white">
        {{-- <div class="container"> --}}
             <div class="row mx-2">
               <div class="col-md-6 offset-md-3 align-self-center">
                <div class="about-text more-info mb-5">
                <h3>How Cloud key works </h3>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Cloud key will receive the 1st month’s rent/deposit</li>
                    <li class="list-group-item">One month rent commission will be share between Cloud key and the agent okn a 50/50 basis</li>
                    <li class="list-group-item">Cloud key will not charge any listing fees</li>
                    <li class="list-group-item">Cloud key will be a free listing website </li>
                    <li class="list-group-item">The agents agrees to keep their contact up to date </li>
                    <li class="list-group-item">Cloud key has exclusive right to market any property listed by the appointed agents </li>
                    <li class="list-group-item">The  Agent will provided correct information necessary for the property to be listed </li>
                    <li class="list-group-item">The agent shall be responsible of any requested viewings on their listing </li>
                    <li class="list-group-item">The agent is expect to uphold diligence and in case of any compliance the agent contract will be cancelled </li>
                    <li class="list-group-item">Accurately indicate the correct property location</li>
                    <li class="list-group-item">The agent will work exclusively with and in the best interest of all potential tenants under Cloud key.</li>
                </ul>
                </div>
        </div>
        </div>
        </div>
    {{-- </div> --}}
</div>

<br>
<!-- About us end -->

<!-- agent start -->
{{-- @include('layouts.team') --}}
<!-- agent end -->

<!-- Testimonial 3 start -->
<div class="testimonial testimonial-3">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="testimonial-inner">
                    <header class="testimonia-header">
                        <h1>Testimonial</h1>
                    </header>
                    <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <p class="lead">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                </p>
                                <div class="avatar">
                                    <img src="assets/img/avatar/avatar-2.jpg" alt="avatar-2" class="img-fluid rounded">
                                </div>
                                <ul class="rating">
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-half-full"></i>
                                    </li>
                                </ul>
                                <div class="author-name">
                                    John Antony
                                </div>
                            </div>
                            <div class="carousel-item">
                                <p class="lead">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                </p>
                                <div class="avatar">
                                    <img src="assets/img/avatar/avatar.jpg" alt="avatar" class="img-fluid rounded">
                                </div>
                                <ul class="rating">
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-half-full"></i>
                                    </li>
                                </ul>
                                <div class="author-name">
                                    Martin Smith
                                </div>
                            </div>
                            <div class="carousel-item">
                                <p class="lead">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                </p>
                                <div class="avatar">
                                    <img src="assets/img/avatar/avatar-3.jpg" alt="avatar-3" class="img-fluid rounded">
                                </div>
                                <ul class="rating">
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-half-full"></i>
                                    </li>
                                </ul>
                                <div class="author-name">
                                    Karen Paran
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Testimonial 3 end -->

@include('partials.footer')

@endsection
