@extends('layouts.app')

@section('content')


    <!-- Sub banner start -->
    <div class="sub-banner overview-bgi">
        <div class="container">
            <div class="breadcrumb-area">
                <h1>Oops, Sorry !</h1>
                <ul class="breadcrumbs">
                    <li><a href="/">Home</a></li>
                    <li class="active">You seemed to lost</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Sub banner end -->

    <<div class="pages-404 content-area-7">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pages-404-inner">
                        <h1>404</h1>
                        <div class="e404">
                            <h5>page Not Found</h5>
                            <a class="btn btn-border btn-sm" href="/" role="button">Go Home</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.footer')
@endsection
