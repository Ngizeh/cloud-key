@component('mail::message')
# Hi, Admin

<p> Email enquiry from : {{ $user->name }} </p>
<p> Email is : <a href="#"> {{ $user->email }}</a> <p>
<p>Subject : {{ $user->subject }} <p>
<p>Phone Number : {{ $user->phone_number }} <p>
<p>Message : {{ $user->message}} <p>

From Website,<br>
{{ config('app.name') }}
@endcomponent
