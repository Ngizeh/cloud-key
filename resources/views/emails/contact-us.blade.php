@component('mail::message')
# Hi, {{ $user->name }}

Thanks you for contacting us.

@component('mail::button', ['url' => config('app.url')])
Back Home
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
