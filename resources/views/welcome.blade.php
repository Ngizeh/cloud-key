@extends('layouts.app')

@section('content')

    <!-- Top header start -->
    @include('partials.header')
    <!-- Top header end -->


    <!-- Banner start -->
    <div class="banner" id="banner">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner banner-max-height">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="assets/img/banner-3.jpg" alt="banner-1">
                    <div class="carousel-caption banner-slider-inner d-flex h-100 text-center">
                        <div class="carousel-content container">
                            <div class="text-center">
                                <h1 data-animation="animated fadeInDown delay-05s"> The new way of renting <br/>amazing and simple</h1>
                                <p data-animation="animated fadeInUp delay-10s">
                                    Rent a house you would to live.
                                </p>
                                <a data-animation="animated fadeInUp delay-10s" href="/register" class="btn btn-lg btn-round btn-theme">Get Started Now</a>
                                <a data-animation="animated fadeInUp delay-12s" href="/register" class="btn btn-lg btn-round btn-white-lg-outline">Sign Up</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="carousel-item">
                    <img class="d-block w-100" src="assets/img/banner-3.jpg" alt="banner-2">
                    <div class="carousel-caption banner-slider-inner d-flex h-100 text-center">
                        <div class="carousel-content container">
                            <div class="text-center">
                                <h1 data-animation="animated fadeInDown delay-05s">Rent easy <br/> Dream Properties</h1>
                                <p data-animation="animated fadeInUp delay-10s">
                                    Need a tenant for your property? List your property, today
                                </p>
                                <a data-animation="animated fadeInUp delay-10s" href="/register" class="btn btn-lg btn-round btn-theme">Get Started Now</a>
                                <a data-animation="animated fadeInUp delay-12s" href="/register" class="btn btn-lg btn-round btn-white-lg-outline">Sign Up</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="carousel-item">
                    <img class="d-block w-100" src="assets/img/banner-3.jpg" alt="banner-3">
                    <div class="carousel-caption banner-slider-inner d-flex h-100 text-center">
                        <div class="carousel-content container">
                            <div class="text-center">
                                <h1 data-animation="animated fadeInUp delay-05s">Best Place For <br/> Rental Properties</h1>
                                <p data-animation="animated fadeInUp delay-10s">
                                    Home you would to live.
                                </p>
                                <a data-animation="animated fadeInUp delay-10s" href="/register" class="btn btn-lg btn-round btn-theme">Get Started Now</a>
                                <a data-animation="animated fadeInUp delay-12s" href="/register" class="btn btn-lg btn-round btn-white-lg-outline">Sign Up</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="slider-mover-left" aria-hidden="true">
				<i class="fa fa-angle-left"></i>
			</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="slider-mover-right" aria-hidden="true">
				<i class="fa fa-angle-right"></i>
			</span>
            </a>
        </div>
    </div>
    <!-- banner end -->

    <!-- Search area start -->
    <div class="search-area" id="search-area-1">
        <div class="container">
            <div class="search-area-inner">
                <div class="search-contents">
                    <form action="" method="POST">
                        <div class="row">
                            <div class="col-6 col-lg-3 col-md-3">
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="brand">
                                        <option>Area From</option>
                                        <option>1500</option>
                                        <option>1200</option>
                                        <option>900</option>
                                        <option>600</option>
                                        <option>300</option>
                                        <option>100</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6 col-lg-3 col-md-3">
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="property-status">
                                        <option>Property Status</option>
                                        <option>For Sale</option>
                                        <option>For Rent</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6 col-lg-3 col-md-3">
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="location">
                                        <option>Location</option>
                                        <option>Nairobi</option>
                                        <option>Mombasa</option>
                                        <option>Kisumu</option>
                                        <option>Nakuru</option>
                                        <option>Thika</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6 col-lg-3 col-md-3">
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="category">
                                        <option>Property Types</option>
                                        <option>Apartment</option>
                                        <option>Houses</option>
                                        <option>Condos</option>
                                        <option>Villa</option>
                                        <option>Bungalow</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 col-lg-3 col-md-3">
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="body">
                                        <option>Bedrooms</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6 col-lg-3 col-md-3">
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="transmission">
                                        <option>Bathrooms</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6 col-lg-3 col-md-3">
                                <div class="form-group">
                                    <div class="form-group">
                                        <select class="selectpicker search-fields" name="category">
                                            <option>Price Range</option>
                                            <option>5000-10000</option>
                                            <option>5000-10000</option>
                                            <option>5000-10000</option>
                                            <option>5000-10000</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-lg-3 col-md-3">
                                <div class="form-group">
                                    <button class="search-button btn-md btn-color" type="submit">Search</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Search area start -->

    <!-- Featured properties start -->
    @if (count($properties) > 0)
        <div class="featured-properties content-area-2">
            <div class="container">
                <div class="main-title">
                    <h1>Featured Properties</h1>
                </div>
                <ul class="list-inline-listing filters filteriz-navigation">
                    <li class="active btn filtr-button filtr" data-filter="all">All</li>
                    <li data-filter="1" class="btn btn-inline filtr-button filtr">Bungalow</li>
                    <li data-filter="2" class="btn btn-inline filtr-button filtr">House</li>
                    <li data-filter="3" class="btn btn-inline filtr-button filtr">Apartment</li>
                </ul>
                <div class="row filter-portfolio">
                    @foreach($properties->slice(0,9) as $property)
                        <div class="col-lg-4 col-md-6 col-sm-12 filtr-item" data-category="3">
                            <div class="property-box">
                                <div class="property-thumbnail">
                                    <a href="/property/{{$property->slug}}" class="property-img">
                                        <div class="tag button alt featured">Vacant</div>
                                        <div class="price-ratings-box">
                                            <p class="price">
                                                Ksh. {{number_format($property->price)}}
                                            </p>
                                        </div>
                                        <x-image :property="$property"></x-image>
                                    </a>
                                </div>
                                <div class="detail">
                                    <h1 class="title">
                                        <a href="/property/{{$property->slug}}">{{$property->truncatedTitle()}}</a>
                                    </h1>
                                    <div class="location">
                                        <a href="/property/show">
                                            <i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i>{{$property->address}} , {{$property->town}},
                                        </a>
                                    </div>
                                    <ul class="facilities-list clearfix">
                                        <li>
                                            <i class="flaticon-bed"></i> {{$property->bedroom}} Bedrooms
                                        </li>
                                        <li>
                                            <i class="flaticon-bath"></i> {{$property->bathroom}} Bathrooms
                                        </li>
                                        <li>
                                            <i class="flaticon-square-layouting-with-black-square-in-east-area"></i> Sq Ft:{{$property->area}}
                                        </li>
                                        <li>
                                            <i class="flaticon-car-repair"></i> 1 Garage
                                        </li>
                                    </ul>
                                </div>
                                <div class="footer">
                                    <a href="#">
                                        <i class="fa fa-user"></i> {{$property->user->name}}
                                    </a>
                                    <span>
									{{-- <i class="fa fa-calendar-o"></i> {{ $property->formatted_created_at}} --}}
								</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    <!-- Featured properties end -->

    <!-- services start -->
    <div class="services content-area-17 {{ count($properties) < 1 ? 'mt-5' : ''}}">
        <div class="container">
            <div class="main-title">
                <h1>What Are you Looking For?</h1>
            </div>
            @include('partials.services')
        </div>
    </div>
    <!-- services end -->

    <!-- Recent Properties start -->
    @if(count($properties))
        <div class="recent-properties content-area">
            <div class="container">
                <div class="main-title">
                    <h1>Recent Properties</h1>
                </div>
                <div class="row">
                    @foreach($properties->slice(0,4) as $property)
                        <div class="col-lg-3 col-md-6 col-sm-6 wow fadeInLeft delay-04s">
                            <div class="card property-box-2">
                                <!-- property img -->
                                <div class="property-thumbnail">
                                    <a href="/property/{{$property->slug}}" class="property-img">
                                        <x-image :property="$property"></x-image>
                                    </a>
                                </div>
                                <!-- detail -->
                                <div class="detail">
                                    <h5 class="title"><a href="/property/{{$property->slug}}">{{$property->truncatedTitleRecent()}}</a></h5>
                                    <h4 class="price">
                                        Ksh. {{$property->price}}/month
                                    </h4>
                                    <p>{{ $property->truncatedDescriptionRecent() }}.</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    <!-- Recent Properties end -->
    <!-- Testimonial start -->
    <div class="testimonial overview-bgi wow fadeInUp delay-04s" style="background-image: url(assets/img/testimonial-property-2.jpg)">
        <div class="container">
            <div class="row">

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="testimonial-inner">
                        <header class="testimonia-header">
                            <h1>Testimonial</h1>
                        </header>
                        <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="avatar">
                                        <img src="assets/img/avatar/avatar.jpg" alt="avatar" class="img-fluid rounded-circle">
                                    </div>
                                    <p class="lead">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>
                                    <div class="author-name">
                                        Martin Smith
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="avatar">
                                        <img src="assets/img/avatar/avatar-2.jpg" alt="avatar-2" class="img-fluid rounded-circle">
                                    </div>
                                    <p class="lead">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>
                                    <div class="author-name">
                                        Emma Connor
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="avatar">
                                        <img src="assets/img/avatar/avatar-3.jpg" alt="avatar-3" class="img-fluid rounded-circle">
                                    </div>
                                    <p class="lead">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>
                                    <div class="author-name">
                                        John Antony
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Testimonial end -->

    <!-- Blog 2 start -->
    <div class="blog-2 content-area-2">
        <div class="container">
            <div class="main-title">
                <h1>Blog</h1>
            </div>
            <div class="row">
                @forelse($blogs->slice(0, 2) as $blog)
                    <div class="col-lg-6 col-md-6 wow fadeInLeft delay-04s">
                        <div class="row blog-list">
                            <div class="col-lg-5 col-md-12 col-pad ">
                                <div class="photo">
                                    @foreach($blog->photos->chunk(1) as $set)
                                        @foreach($set as $photo)
                                            @if ($loop->parent->first)
                                                <a href="/blogs/{{ $blog->slug }}">
                                                    <img src="/{{$photo->path}}" alt="{{ $blog->title }}" class="img-fluid fit-coverh-100">
                                                </a>
                                            @endif
                                        @endforeach
                                    @endforeach
                                    <div class="date-box">
                                        <h5>{{ $blog->formattedDay() }}</h5>
                                        <h5> {{ $blog->formattedMonth() }}</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-12 col-pad align-self-center">
                                <div class="detail">
                                    <h3>
                                        <a href="/blogs/{{ $blog->slug }}">{{ $blog->formatted_title }}</a>
                                    </h3>
                                    <div class="post-meta">
                                        <span><a href="#"><i class="fa fa-user"></i>{{ $blog->user->name }}</a></span>
                                        <span><a href="#"><i class="fa fa-clock-o"></i>7 Comment</a></span>
                                    </div>
                                    <p>
                                        {{ $blog->excerpt }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="my-address contact-2 widget hdn-mb-30 text-center">
                            <h6>No blogs yet...</h6>
                        </div>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
    <!-- Blog 2 start -->

    <!-- intro section start -->
    <div class="intro-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-2 col-sm-12 text-uppercase">
                    <h2 class="text-white">Cloud key</h2>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12">
                    <div class="intro-text">
                        <h3>For Property Management</h3>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-12">
                    <a href="/property/create" class="btn btn-md">Submit Now</a>
                </div>
            </div>
        </div>
    </div>

    <!-- intro section end -->

    @include('partials.footer')

@endsection

