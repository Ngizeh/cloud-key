@extends('layouts.app')

@section('content')

@include('partials.header')

  <!-- Sub banner start -->
@include('partials.banner', ['title' => 'Services', 'type' =>'Services' ])
<!-- Sub banner end -->

<!-- services start -->
<div class="services content-area-13">
    <div class="container">
        <div class="main-title">
            <h1>Property Services</h1>
        </div>
        @include('partials.services')
    </div>
</div>
<!-- services end -->


<!-- Our service start -->
<div class="services content-area-2">
    <div class="container">
        <div class="main-title">
            <h1>Professional Service</h1>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="card services-info-4">
                    <img class="card-img-top" src="assets/img/service-photo/service-2.jpg" alt="service-2">
                    <div class="card-body detail">
                        <h5>
                            <a href="#">Security</a>
                        </h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                        <a href="#" class="btn btn-sm btn-border">Read more</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="card services-info-4">
                    <img class="card-img-top" src="assets/img/service-photo/service.jpg" alt="service">
                    <div class="card-body detail">
                        <h5>
                            <a href="#">Support 24/7</a>
                        </h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                        <a href="#" class="btn btn-sm btn-border">Read more</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="card services-info-4">
                    <img class="card-img-top" src="assets/img/service-photo/service-3.jpg" alt="service-3">
                    <div class="card-body detail">
                        <h5>
                            <a href="#">Bank Loans</a>
                        </h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                        <a href="#" class="btn btn-sm btn-border">Read more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Our service end -->

<!-- Testimonial 3 start -->
<!-- <div class="testimonial testimonial-3">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="testimonial-inner">
                    <header class="testimonia-header">
                        <h1>What the clients say</h1>
                    </header>
                    <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <p class="lead">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                </p>
                                <div class="avatar">
                                    <img src="assets/img/avatar/avatar-2.jpg" alt="avatar-2" class="img-fluid rounded">
                                </div>
                                <ul class="rating">
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-half-full"></i>
                                    </li>
                                </ul>
                                <div class="author-name">
                                    John Antony
                                </div>
                            </div>
                            <div class="carousel-item">
                                <p class="lead">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                </p>
                                <div class="avatar">
                                    <img src="assets/img/avatar/avatar.jpg" alt="avatar" class="img-fluid rounded">
                                </div>
                                <ul class="rating">
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-half-full"></i>
                                    </li>
                                </ul>
                                <div class="author-name">
                                    Martin Smith
                                </div>
                            </div>
                            <div class="carousel-item">
                                <p class="lead">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                </p>
                                <div class="avatar">
                                    <img src="assets/img/avatar/avatar-3.jpg" alt="avatar-3" class="img-fluid rounded">
                                </div>
                                <ul class="rating">
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-half-full"></i>
                                    </li>
                                </ul>
                                <div class="author-name">
                                    Karen Paran
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- Testimonial 3 end -->

@include('partials.footer')


@endsection
