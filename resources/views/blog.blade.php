@extends('layouts.app')

@section('content')

<!-- main header start -->
<header class="main-header do-sticky" id="main-header-2">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-expand-lg navbar-light rounded">
                    <a class="navbar-brand logo" href="index.html">
                        <h2>Cloud Key </h2>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="fa fa-bars"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbar">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link"  href="/"  aria-expanded="false">
                                    Home
                                </a>

                            </li>
                            <li class="nav-item ">
                                <a class="nav-link " href="/properties"  aria-expanded="false">
                                    Properties
                                </a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="/blog" aria-haspopup="true" aria-expanded="false">
                                    Blog
                                </a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="/about" aria-haspopup="true" aria-expanded="false">
                                    About Us
                                </a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="/services" aria-haspopup="true" aria-expanded="false">
                                    Services
                                </a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="/contat-us" aria-haspopup="true" aria-expanded="false">
                                    Contact
                                </a>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <a href="#full-page-search" class=" my-2 my-sm-0">
                                <i class="fa fa-search"></i>
                            </a>
                        </form>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- main header end -->

<!-- Sub banner 2 start -->
<div class="sub-banner-2">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>Blog Classic Right Sidebar</h1>
            <ul class="breadcrumbs">
                <li><a href="index.html">Home</a></li>
                <li class="active">Blog Classic Right Sidebar</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub banner 2 end -->

<!-- Blog section start -->
<div class="blog-section content-area-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="blog-grid-box">
                    <img class="blog-theme img-fluid" src="assets/img/blog/blog.jpg" alt="blog">
                    <div class="detail">
                        <div class="date-box">
                            <h5>03</h5>
                            <h5>May</h5>
                        </div>
                        <h2>
                            <a href="/blog-details">Selling Your Home</a>
                        </h2>
                        <div class="post-meta">
                            <span><a href="#"><i class="fa fa-user"></i>John Antony</a></span>
                            <span><a><i class="fa fa-clock-o"></i>July 20</a></span>
                            <span><a href="#"><i class="fa fa-commenting-o"></i>24 Comment</a></span>
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.But also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.But also the leap into electronic typesetting,</p>
                        <a href="/blog-details" class="btn-read-more">Read more</a>
                    </div>
                </div>
                <div class="blog-grid-box">
                    <img class="blog-theme img-fluid" src="assets/img/blog/blog-2.jpg" alt="blog-2">
                    <div class="detail">
                        <div class="date-box">
                            <h5>03</h5>
                            <h5>May</h5>
                        </div>
                        <h2>
                            <a href="/blog-details">Why Live in New York</a>
                        </h2>
                        <div class="post-meta">
                            <span><a href="#"><i class="fa fa-user"></i>John Antony</a></span>
                            <span><a><i class="fa fa-clock-o"></i>July 20</a></span>
                            <span><a href="#"><i class="fa fa-commenting-o"></i>24 Comment</a></span>
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.But also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.But also the leap into electronic typesetting,</p>
                        <a href="/blog-details" class="btn-read-more">Read more</a>
                    </div>
                </div>
                <div class="blog-grid-box">
                    <img class="blog-theme img-fluid" src="assets/img/blog/blog-3.jpg" alt="blog-3">
                    <div class="detail">
                        <div class="date-box">
                            <h5>03</h5>
                            <h5>May</h5>
                        </div>
                        <h2>
                            <a href="/blog-details">Buying a Home</a>
                        </h2>
                        <div class="post-meta">
                            <span><a href="#"><i class="fa fa-user"></i>John Antony</a></span>
                            <span><a><i class="fa fa-clock-o"></i>July 20</a></span>
                            <span><a href="#"><i class="fa fa-commenting-o"></i>24 Comment</a></span>
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.But also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.But also the leap into electronic typesetting,</p>
                        <a href="/blog-details" class="btn-read-more">Read more</a>
                    </div>
                </div>
                <div class="pagination-box hidden-mb-45">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="#"><span aria-hidden="true">«</span></a></li>
                            <li class="page-item"><a class="page-link active" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#"><span aria-hidden="true">»</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="sidebar mbl">
                    <!-- Search box start -->
                    <div class="widget search-box">
                        <h5 class="sidebar-title">Search</h5>
                        <form class="form-search" method="GET">
                            <input type="text" class="form-control" placeholder="Search">
                            <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                        </form>
                    </div>

                    <!-- Categories start -->
                    <div class="widget categories">
                        <h5 class="sidebar-title">Categories</h5>
                        <ul>
                            <li><a href="#">Apartments<span>(12)</span></a></li>
                            <li><a href="#">Houses<span>(8)</span></a></li>
                            <li><a href="#">Family Houses<span>(23)</span></a></li>
                            <li><a href="#">Offices<span>(5)</span></a></li>
                            <li><a href="#">Villas<span>(63)</span></a></li>
                            <li><a href="#">Other<span>(7)</span></a></li>
                        </ul>
                    </div>

                    <!-- Recent posts start -->
                    <div class="widget recent-posts">
                        <h5 class="sidebar-title">Recent Properties</h5>
                        <div class="media mb-4">
                            <a class="pr-4" href="properties-details.html">
                                <img src="assets/img/sub-property/sub-property.jpg" alt="sub-property">
                            </a>
                            <div class="media-body align-self-center">
                                <h5>
                                    <a href="properties-details.html">Beautiful Single Home</a>
                                </h5>
                                <p>February 27, 2018</p>
                                <p> <strong>Ksh. 245,000</strong></p>
                            </div>
                        </div>
                        <div class="media mb-4">
                            <a class="pr-4" href="properties-details.html">
                                <img src="assets/img/sub-property/sub-property-2.jpg" alt="sub-property-2">
                            </a>
                            <div class="media-body align-self-center">
                                <h5>
                                    <a href="properties-details.html">Sweet Family Home</a>
                                </h5>
                                <p>February 27, 2018</p>
                                <p> <strong>Ksh. 245,000</strong></p>
                            </div>
                        </div>
                        <div class="media">
                            <a class="pr-4" href="properties-details.html">
                                <img src="assets/img/sub-property/sub-property-3.jpg" alt="sub-property-3">
                            </a>
                            <div class="media-body align-self-center">
                                <h5>
                                    <a href="properties-details.html">Real Luxury Villa</a>
                                </h5>
                                <p>February 27, 2018</p>
                                <p> <strong>Ksh. 245,000</strong></p>
                            </div>
                        </div>
                    </div>

                    <!-- Tags start -->
                    <div class="widget tags clearfix">
                        <h5 class="sidebar-title">Tags</h5>
                        <ul class="tags">
                            <li><a href="#">Business</a></li>
                            <li><a href="#">Design</a></li>
                            <li><a href="#">Real Estate</a></li>
                            <li><a href="#">Luxury</a></li>
                            <li><a href="#">Theme</a></li>
                            <li><a href="#">Events</a></li>
                            <li><a href="#">Outdoor</a></li>
                            <li><a href="#">UI-UX</a></li>
                            <li><a href="#">Buy Website</a></li>
                            <li><a href="#">Villa</a></li>
                            <li><a href="#">Sellers</a></li>
                        </ul>
                    </div>

                    <!-- Recent comments start -->
                    <div class="widget recent-comments">
                        <h5 class="sidebar-title">Recent comments</h5>
                        <div class="media mb-4">
                            <a class="pr-3" href="#">
                                <img src="assets/img/avatar/avatar.jpg" class="rounded-circle" alt="avatar">
                            </a>
                            <div class="media-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiamrisus tortor,</p>
                                <p>By <span>John Doe</span></p>
                            </div>
                        </div>
                        <div class="media">
                            <a class="pr-3" href="#">
                                <img src="assets/img/avatar/avatar-2.jpg" class="rounded-circle" alt="avatar-2">
                            </a>
                            <div class="media-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiamrisus tortor,</p>
                                <p>By <span>Karen Paran</span></p>
                            </div>
                        </div>
                    </div>

                    <!-- Latest start -->
                    <div class="widget latest-tweet">
                        <h5 class="sidebar-title">Latest Tweet</h5>
                        <P><a href="#">Lorem Ipsum is simply</a> dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text,</P>
                        <p>@Lorem ipsum dolor<a href="#">sit amet, consectetur</a> adipiscing elit. Aenean id dignissim justo. Maecenas urna lacus,</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Blog section end -->

@include('partials.footer')

@endsection
