<?php

namespace App;

use App\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class User extends Authenticatable
{
    use Notifiable, HasFactory, Uuids;

    protected $casts = ['id' => 'string'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'provider', 'provider_id', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $baseDir = 'uploaded/avatars';

    public function roles(): HasOne
    {
        return $this->hasOne(Role::class, 'user_id');
    }
    public function tenant(): HasOne
    {
        return $this->hasOne(TenantProfile::class);
    }

    public function properties(): HasMany
    {
        return $this->hasMany(Property::class,  'user_id');
    }

    public function blog(): HasMany
    {
        return $this->hasMany(Blog::class);
    }

    public static function named($avatar): User
    {
        return (new static)->saveAs($avatar);
    }

    public function saveAs($avatar): User
    {
        $this->avatar = sprintf("%s-%s", time(), $avatar);

        return $this;
    }

    public function move(UploadedFile $file): User
    {
        $file->move($this->baseDir, $this->avatar);

        return $this;
    }

    public function agentProfile(): HasOne
    {
        return  $this->hasOne(AgentProfile::class);
    }

    public function tenantProfile(): HasOne
    {
        return  $this->hasOne(TenantProfile::class);
    }

    public function landlordProfile(): HasOne
    {
        return  $this->hasOne(LandlordProfile::class);
    }
}
