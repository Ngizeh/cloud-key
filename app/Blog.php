<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Blog extends Model
{
    use Sluggable, Uuids, HasFactory;

    protected $casts = ['id' => 'string'];

    protected $fillable = [
        'description',
        'title',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function sluggable() :array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function path()
    {
        return '/blog/'.$this->slug;
    }

    public function photos()
    {
        return $this->hasMany(BlogImage::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function getFormattedTitleAttribute()
    {
        return Str::limit($this->title, 15);
    }

    public function formattedDate()
    {
        return $this->created_at->format('M d');
    }
    public function formattedDay()
    {
        return $this->created_at->format('d');
    }
    public function formattedMonth()
    {
        return $this->created_at->format('M');
    }

    public function blogDescription(): string
    {
        return Str::limit($this->description, 150, '..');
    }

    public function getExcerptAttribute(): string
    {
        return Str::limit($this->description, 75, '...');
    }


    public static function locatedAt($slug)
    {
        return static::where(compact('slug'))->firstOrFail();
    }

    public function addPhoto(BlogImage $blogImages)
    {
        return $this->photos()->save($blogImages);
    }
}
