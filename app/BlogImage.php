<?php

namespace App;

use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BlogImage extends Model
{
    use Uuids, HasFactory;

    protected $casts = ['id' => 'string'];

    protected $fillable = [
        'path', 'name', 'thumbnail'
    ];

    protected $baseDir = 'blog/photos';

    public function blog()
    {
        return $this->belongsTo(Blog::class);
    }

    public static function named($name)
    {
        return (new static)->saveAs($name);
    }

    public function saveAs($name)
    {
        $this->name = sprintf("%s-%s", time(), $name);
        $this->path = sprintf("%s/%s", $this->baseDir, $this->name);
        $this->thumbnail = sprintf("%s/tn-%s", $this->baseDir, $this->name);

        return $this;

    }

    public function move(UploadedFile $file)
    {
        $file->move($this->baseDir, $this->name);

        $this->makeThumbnail();

        return $this;
    }

    protected function makeThumbnail()
    {
        Image::make($this->path)->fit(350, 230)->save($this->thumbnail);
    }

    public function delete()
    {
        \File::delete([
            $this->name,
            $this->path,
            $this->thumbnail
        ]);

        parent::delete();
    }
}
