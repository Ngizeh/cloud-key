<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PropertyPhoto extends Model
{
    use Uuids,HasFactory;

    protected $fillable = [
        'path', 'name', 'thumbnail_path','property_id'
    ];

    protected $baseDir = 'properties/photos';

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public static function named($name)
    {
        return (new static)->saveAs($name);
    }

    public function saveAs($name)
    {
        $this->name = sprintf("%s-%s", time(), $name);
        $this->path = sprintf("%s/%s", $this->baseDir, $this->name);
        $this->thumbnail_path = sprintf("%s/tn-%s", $this->baseDir, $this->name);

        return $this;

    }

    public function move(UploadedFile $file)
    {
        $file->move($this->baseDir, $this->name);

        $this->makeThumbnail();

        $this->resizePhoto();

        return $this;
    }

    protected function makeThumbnail()
    {
        Image::make($this->path)->fit(374, 250)->save($this->thumbnail_path);
    }

    protected function resizePhoto()
    {
        Image::make($this->path)->fit(730, 485)->save($this->path);
    }

    public function delete()
    {
      \File::delete([
         $this->name,
         $this->path,
         $this->thumbnail_path
      ]);

      parent::delete();
    }
}
