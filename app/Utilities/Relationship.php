<?php

namespace App\Utilities;

class Relationship
{

    protected static $relationship = [
        "brother",
        "sister",
        "wife",
        "husband",
        "father",
        "mother",
    ];

    public static function all()
    {
        return array_values(static::$relationship);
    }
}
