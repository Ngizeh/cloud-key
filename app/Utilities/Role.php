<?php

namespace App\Utilities;

class Role {

    protected static $roles = [
        "agent",
        "tenant",
        "landlord",
        "admin"
    ];

    public static function all()
    {
        return array_values(static::$roles);
    }

}

