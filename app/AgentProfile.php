<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentProfile extends Model
{
    use Uuids;

    protected $fillable = [
        "nationality",
        "phone",
        "years",
        "physical_location",
        "company",
        "bio",
        "identification_number",
        "employment_status",
        "bank_name",
        "bank_account",
        "account_branch",
        "swift_code"

    ];
}
