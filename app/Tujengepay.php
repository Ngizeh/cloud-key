<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tujengepay extends Model
{
    protected $fillable = [
        "paymentId",
        "externalIdentifier",
        "accountNumber" ,
        "resourceIdentifier",
        "transactedAmount",
        "transactionCost",
        "actualAmount",
        "status",
        "receiptNumber",
        "transactionDate",
        "phoneNumber"
    ];
}
