<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LandlordProfile extends Model
{
    use Uuids;

    protected $fillable = [
        "nationality",
        "phone",
        "physical_location",
        "identification_number",
        "bank_name",
        "bank_account",
        "account_branch",
        "switf_code",
        "next_of_kin_name",
        "next_of_kin_email",
        "next_of_kin_phone",
        "next_of_kin_relationship",
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
