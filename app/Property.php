<?php

namespace App;

use App\Uuids;
use Carbon\Carbon;
use App\Utilities\Counties;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Property extends Model
{
    use Sluggable,Uuids,HasFactory;

    protected $fillable = [
        'user_id',
        'title',
        'type',
        'price',
        'deposit',
        'area',
        'bedroom',
        'bathroom',
        'address',
        'county',
        'town',
        'description',
        'units',
        'postal_code',
        'available_date',
        'compound_units',
        'installments',
        'features',
        'service_charge'
    ];
    
    protected $casts = [
        'id' => 'string',
        'features' => 'array',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function photos()
    {
        return $this->hasMany(PropertyPhoto::class, 'property_id');
    }

    public static function locatedAt($slug)
    {
        return static::where(compact('slug'))->firstOrFail();
    }

    public function addPhoto(PropertyPhoto $propertyPhoto)
    {
        return $this->photos()->save($propertyPhoto);
    }

    public function path()
    {
        return '/photo/' . $this->slug;
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function createdDate()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }


    public function price_format(): string
    {
        return number_format($this->price);
    }

    public function deposit_format(): string
    {
        return number_format($this->deposit);
    }

    public function total()
    {
        return $this->price + $this->deposit;
    }

    public function fullAmount()
    {
        $total = $this->price + $this->deposit;

        return number_format($total);
    }

    public function installment(): string
    {
        return number_format($this->deposit * 0.5);
    }

    public function rentalAndInstallment(): string
    {
        return number_format($this->price + $this->deposit * 0.5);
    }

    public function truncatedTitle(): string
    {
        return Str::limit($this->title, 27, '...');
    }

    public function truncatedTitleRecent(): string
    {
        return Str::limit($this->title, 15, '...');
    }

    public function truncatedDescriptionRecent(): string
    {
        return Str::limit($this->title, 60, '...');
    }

    public function truncatedTitleWidget(): string
    {
        return Str::limit($this->title, 15, '...');
    }

    public function truncatedDescription(): string
    {
       return Str::limit($this->description, 70, '...');
    }

    public function getformattedDateAttribute(): string
    {
        return  Carbon::parse($this->created_at)->format('F j, Y');
    }

    public function getformattedCreatedAtAttribute()
    {
        return  Carbon::parse($this->created_at)->diffForHumans();
    }

    public function counties(): array
    {
        return Counties::all();
    }

}
