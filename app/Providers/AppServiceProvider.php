<?php

namespace App\Providers;

use App\Property;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share('properties',
            Property::query()->select('id','title','slug', 'price', 'street', 'town', 'bedroom', 'bathroom', 'created_at','user_id')
                ->with('photos:id,thumbnail_path,property_id', 'user:id,name')
                ->paginate(9));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
