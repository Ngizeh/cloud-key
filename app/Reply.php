<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    use Uuids;

    protected $with = 'user';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    

}
