<?php

namespace App;

use App\Tenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TenantProfile extends Model
{
   use Uuids;

     protected $fillable = [
            "nationality" ,
            "phone",
            "current_residence",
            "bio",
            "identification_number" ,
            "employment_status" ,
            "next_of_kin_name" ,
            "next_of_kin_email" ,
            "next_of_kin_phone" ,
            "next_of_kin_relationship" ,
    ];


    public function user(): BelongsTo
    {
       return $this->belongsTo(User::class);
    }
}
