<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TenantProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "nationality" => "required",
            "phone" => "required",
            "current_residence" => "required|string",
            "bio" => "required",
            "identification_number" => "required",
            "employment_status" => "required",
            "next_of_kin_name" => "string|required",
            "next_of_kin_email" => "required|email",
            "next_of_kin_phone" => "required",
            "next_of_kin_relationship" => "required",
        ];
    }
}
