<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'type' => 'required|string',
            'price' => 'required|integer',
            'deposit' => 'required|integer',
            'available_date' => 'required',
            'area' => 'required|integer',
            'bedroom' => 'required|integer',
            'bathroom' => 'required|integer',
            'address' => 'required|string',
            'county' => 'required|string',
            'town' => 'required|string',
            'description' => 'required|string',
            'units' => 'required|integer',
            'postal_code' => 'string|nullable',
            'compound_units' => 'integer|nullable',
            'installments' => 'required',
            'features.*' => 'string|nullable'
        ];
    }
}
