<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgentProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nationality" => "required",
            "phone" => "required",
            "years" => "required",
            "physical_location" => "required|string",
            "company" => "string|nullable",
            "bio" => "required",
            "identification_number" => "required",
            "employment_status" => "required",
            "bank_name" => "string|required",
            "bank_account" => "string|required",
            "account_branch" => "required",
            "swift_code" => "required",
        ];
    }
}
