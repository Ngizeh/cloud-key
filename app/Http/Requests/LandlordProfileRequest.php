<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LandlordProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "nationality" => "required",
            "identification_number" => "required",
            "phone" => "required",
            "physical_location" => "required|string",
            "bank_name" => "string|required",
            "bank_account" => "string|required",
            "account_branch" => "required",
            "swift_code" => "nullable",
            "next_of_kin_name" => "required",
            "next_of_kin_email" => "required",
            "next_of_kin_phone" => "required",
            "next_of_kin_relationship" => "required",
        ];
    }
}
