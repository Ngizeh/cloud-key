<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Tenant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->roles->role == 'tenant') {
            return $next($request);
        } elseif (auth()->user()->roles->role == 'agent') {
            return redirect('/agent');
        } elseif (auth()->user()->roles->role == 'admin') {
            return redirect('/admin');
        } else {
            return redirect('/tenant');
        }
    }
}
