<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Agent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {
       if (auth()->user()->roles->role == 'agent') {
         return $next($request);
         }
//      elseif (auth()->user()->roles->roles == 'tenant') {
//         return redirect('/tenant');
//        }
//     else {
//         return redirect('/landlord');
//        }
    }
}
