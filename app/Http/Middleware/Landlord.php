<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Landlord
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->roles->role == 'landlord') {
            return $next($request);
        }
    }
}
