<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

class AllLandlordController extends Controller
{
    public function __invoke()
    {
    	$landlords = Role::query()->select('user_id','created_at')
            ->whereRole('landlord')->with('user:id,name')->paginate(15);

        return view('admin.landlords', compact('landlords'));
    }
}
