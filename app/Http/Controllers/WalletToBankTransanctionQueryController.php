<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class WalletToBankTransanctionQueryController extends Controller
{
    //Bank Enquiry
    public function transactionBankQuery()
    {
       
        //dd(request('amount'));
        $timezone = Carbon::parse(Carbon::now(),"Africa/Nairobi");

        $now = Carbon::now();
        $timestamp = $now->getTimestamp();
       //dd($timestamp);
        $uuid = (string) uniqid(rand());

        //Nonce
       $nonce = str_replace('-', '', $uuid);
      // dd($nonce);
       //$nonce = "10259587675cdace75e9d19";



       //url
        $reference = "601"; //this is the receipt number
        
        $baseurl = "https://testids.interswitch.co.ke:19081";

        $url = "https://testids.interswitch.co.ke:19081/api/v1/wallet/wallets/transactions/".$reference;
      
        $walletAccount  = "8880320000000026";
        $amount = request('amount');
        $encodeUrl =  urlencode($url);
        
        //dd($encodeUrl);
        
       //signature
        $httpMethod = "GET";
        $clientId = "IKIA65D824B667995D138E5A71F21B67C33BC8DEA348";
        $clientSecret = "IpxUHRFGulpFAUaKIpX1HLdgi7QDUTmd8HQs5bZUjqw=";
        $signatureCipher = $httpMethod. "&" .$encodeUrl. "&" .$timestamp. "&" .$nonce. "&" .$clientId. "&" .$clientSecret;
          //dd($signatureCipher);
        $messageDigest = hash('SHA512', $signatureCipher, true);
        
        //dd($messageDigest);
        $message = base64_encode($messageDigest);
        
        //dd($message);

        $encodedClientId =  base64_encode($clientId);
        $authorization = "InterswitchAuth ".$encodedClientId;
        //dd($authorization);
       
        $client = new Client();  
  
        try {
             $response = $client->get($url,
              [
                'headers' => [
                  'Content-Type' => 'application/json',
                  'Authorization' => $authorization,
                  'Nonce' => $nonce,
                  'SignatureMethod' => 'SHA512',
                  'Signature' => $message,
                  'Timestamp' => $timestamp,
             ],
          ]);
         
             
           $requests = json_decode( (string) $response->getBody(), true );
           
           print('<pre>');
           print_r(json_encode($requests));
           print('</pre>');


       } catch (GuzzleHttp\Exception\BadResponseException $e) {
             var_dump($e->getMessage());
       }
       //return view('wallet.index');     
    }
}
