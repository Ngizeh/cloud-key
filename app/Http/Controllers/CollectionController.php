<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;

class CollectionController extends Controller
{

   public function interswitchCollection()
   {
        //Post to Bank

        //dd(request('amount'));
        $timezone = Carbon::parse(Carbon::now(),"Africa/Nairobi");

        $now = Carbon::now();
        $timestamp = $now->getTimestamp();
       //dd($timestamp);
        $uuid = (string) uniqid(rand());

        //Nonce
       $nonce = str_replace('-', '', $uuid);
      // dd($nonce);
       //$nonce = "10259587675cdace75e9d19";

       //url
        $baseurl = "https://testids.interswitch.co.ke:19081";

        $url = "https://testids.interswitch.co.ke:19081/api/v1/merchant/transact/cards";

        $amount = request('amount');
        $encodeUrl =  urlencode($url);
        $merchantId = "PRIVAT0001";
        $card = 4111111111111111;
        $cvv = 123;
        $expiry_date = '02/20';

        //dd($encodeUrl);

       //signature
        $httpMethod = "POST";
        $clientId = "IKIA4939A0172AAFAFFFC86B01E777D06FA6D162B346";
        $clientSecret = "GcKk63KlGheZJKdco6XO7iFBaEsUM0Nlx5tPIikxuQU=";
        $signatureCipher = $httpMethod. "&" .$encodeUrl. "&" .$timestamp. "&" .$nonce. "&" .$clientId. "&" .$clientSecret."&".$card."&".$cvv."&".$expiry_date;
          //dd($signatureCipher);
        $messageDigest = hash('SHA512', $signatureCipher, true);

        //dd($messageDigest);
        $message = base64_encode($messageDigest);

        //dd($message);

        $encodedClientId =  base64_encode($clientId);
        $authorization = "InterswitchAuth ".$encodedClientId;
        //dd($authorization);

        $client = new Client();

        try {
             $response = $client->post($url,
              [
                'headers' => [
                  'Content-Type' => 'application/json',
                  'Authorization' => $authorization,
                  'Nonce' => $nonce,
                  'SignatureMethod' => 'SHA512',
                  'Signature' => $message,
                  'Timestamp' => $timestamp,
             ],
              'body' => json_encode([
                "amount" => request('amount'),
                "orderId" => "OID123453",
                "transactionRef" => "66809285644",
                "terminalType" => "MOBILE",
                "terminalId" => "3TLP0001",
                "paymentItem" =>"CRD",
                "provider" => "VSI",
                "authData" => $message,
                "merchantId" => $merchantId,
                "transactionType" => "9002",
                "customerInfor" =>  "kelvin|mwangi|NBI|KE|00200|wstlnds|NBI|kelvin@interswitch.com",
                "currency" => "KES",
                "country" => "KE",
                "city" => "NBI",
                "narration" =>  "Payment-Card",
                "domain" =>  "ISWKE"
                ])
          ]);

           $requests = json_decode( (string) $response->getBody(), true );

           print('<pre>');
           print_r(json_encode($requests));
           print('</pre>');


       } catch (BadResponseException $e) {
               $response = $e->getResponse();
               $jsonBody = json_decode((string) $response->getBody());
              // print('<pre>');
               var_dump(json_encode($jsonBody));
               //print('</pre>');
       }
       //return view('wallet.index');
   }

     public function card()
    {

          //dd(request('amount'));
        $timezone = Carbon::parse(Carbon::now(),"Africa/Nairobi");

        $now = Carbon::now();
        $timestamp = $now->getTimestamp();
       //dd($timestamp);
        $uuid = (string) uniqid(rand());

        //Nonce
       $nonce = str_replace('-', '', $uuid);
      // dd($nonce);
       //$nonce = "10259587675cdace75e9d19";

       //url
        $baseurl = "https://testids.interswitch.co.ke:19081";

        $url = "https://testids.interswitch.co.ke:19081/api/v1/merchant/transact/cards";

        $amount = request('amount');
        $encodeUrl =  urlencode($url);
        $merchantId = "PRIVAT0001";
        $card = 4111111111111111;
        $cvv = 123;
        $expiry_date = '02/20';

        //dd($encodeUrl);

       //signature
        $httpMethod = "POST";
        $clientId = "IKIA4939A0172AAFAFFFC86B01E777D06FA6D162B346";
        $clientSecret = "GcKk63KlGheZJKdco6XO7iFBaEsUM0Nlx5tPIikxuQU=";
        $signatureCipher = $httpMethod. "&" .$encodeUrl. "&" .$timestamp. "&" .$nonce. "&" .$clientId. "&" .$clientSecret;
          //dd($signatureCipher);
        $messageDigest = hash('SHA512', $signatureCipher, true);

        //dd($messageDigest);
        $message = base64_encode($messageDigest);


       return view('wallet.3d', compact('message', 'merchantId'));
    }
}

