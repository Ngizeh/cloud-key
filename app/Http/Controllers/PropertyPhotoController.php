<?php

namespace App\Http\Controllers;

use App\Property;
use App\PropertyPhoto;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PropertyPhotoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Request $request, Property $property, $slug)
    {
        $property = Property::locatedAt($slug)->load('photos');

       return view('properties.upload-photos', compact('property','slug'));
    }

    public function destroy($id): RedirectResponse
    {
       PropertyPhoto::findOrFail($id)->delete();

       return back();
    }

}
