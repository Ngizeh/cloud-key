<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class AdminProfileController extends Controller
{
    public function store(Request $request): RedirectResponse
    {
        $data= $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users'
        ]);

        auth()->user()->update($data);

        return back()->with('updated-profile', 'You\'ve updated your profile');
    }

}
