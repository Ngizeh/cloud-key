<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;

class AllTenantsController extends Controller
{
    public function __invoke()
    {
        // $tenants = Role::query()->select('user_id','created_at')
        //     ->whereRole('tenant')->with('user:id,name')->paginate(15);
        
    	// To change
    	$tenants = Role::query()->whereRole('tenant')->with('user')->paginate(15);

        return view('admin.tenants', compact('tenants'));
    }
}
