<?php

namespace App\Http\Controllers;

use Auth;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\RoleController;

class SocialController extends Controller
{

	public function redirect($provider)
	{
		return Socialite::driver($provider)->redirect();
	}

	public function callback($provider)
	{
		$user = Socialite::driver($provider)->user();

		$authUser = $this->findOrCreateUser($user, $provider);

		Auth::login($authUser, true);

		return auth()->user()->role ? "/{auth()->user()->roles->role}" : "/role";

	}
	public function findOrCreateUser($user, $provider)
	{
        return User::where('email', $user->email)->firstOr(fn() =>
             User::create([
                'name'     => $user->name,
                'email'    => $user->email,
                'provider' => $provider,
                'provider_id' => $user->id,
            ])
        );
	}
}
