<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;

class AdminAllPropertiesController extends Controller
{
    public function __invoke()
    {
         $user = auth()->user();

        return view('admin.properties', compact( 'user'));
    }
}
