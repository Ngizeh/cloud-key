<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UserController extends Controller
{
    public function index(User $user)
    {
        $user = Auth::user();

        return view('agent.settings',compact('user'));
    }

    public function store(Request $request): RedirectResponse
    {
        $this->validate($request, ['file' => 'required|mimes:jpg,jpeg,png,bmp']);

        $avatarName = $this->makePhoto($request->file('file'));

        $user = Auth::user();

        $user->avatar = $avatarName->avatar;

        $user->save();

        return back();

    }

    protected function makePhoto(UploadedFile $file)
    {
        return User::named($file->getClientOriginalName())->move($file);
    }
}

