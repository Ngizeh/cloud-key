<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class AllBlogsController extends Controller
{
	public function __construct()
	{
		$this->middleware(['auth', 'admin']);
	}

	public function __invoke()
	{
		$blogs = Blog::query()
		->select('id','title','description','slug','user_id','created_at')
		->with('user:id,name','photos:path,blog_id')->latest()->paginate(9);

		return view('admin.all_blogs', compact('blogs'));
	}

}
