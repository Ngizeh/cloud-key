<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    //Todo
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('user.change-password');
    }

    public function update()
    {
        request()->validate([
            'password' => 'required|min:6|confirmed'
        ]);

        auth()->user()->update(['password' => Hash::make(request('password'))]);

        return response([], 201);
    }
}
