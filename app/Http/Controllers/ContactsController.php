<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactRequest;
use App\Mail\ReceivingMail;

class ContactsController extends Controller
{
	public function index()
	{
		return view('contact-us');
	}

	public function store(ContactRequest $request)
	{
		Mail::to($request->email)->send(new ContactMail($request));
		Mail::to('edngize@gmail.com')->send(new ReceivingMail($request));

		return back();
	}


}
