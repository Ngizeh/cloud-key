<?php

namespace App\Http\Controllers;

use App\LandlordProfile;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Requests\LandlordProfileRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LandlordProfileController extends Controller
{
     public function __construct()
    {
        $this->middleware(['auth','landlord']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return View
     */
   public function index(LandlordProfile $landlordProfile, Request $request)
    {
        $landlordProfiles = $landlordProfile->where('user_id', Auth::id())->get();

        $user = Auth::user();

        foreach ($landlordProfiles as $landlordProfile) {}

        return view('landlord.index', compact('landlordProfile', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param LandlordProfileRequest $request
     * @param LandlordProfile $profile
     * @return RedirectResponse
     */
     public function store(LandlordProfileRequest $request, LandlordProfile $profile)
    {
        auth()->user()->landlordProfile()->updateOrCreate(
            ['user_id' => auth()->id() ],
            $request->except('_token')
        );

        return back()->with('success', 'You have successfully updated your profile!');
    }

}
