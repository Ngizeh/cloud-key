<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class WalletController extends Controller
{

    public function index()
    {
       return view('wallet.index');
    }
    
    public function status()
    {
            //dd(request('amount'));
        $timezone = Carbon::parse(Carbon::now(),"Africa/Nairobi");

        $now = Carbon::now();
        $timestamp = $now->getTimestamp();
       //dd($timestamp);
        $uuid = (string) uniqid(rand());

        //Nonce
       $nonce = str_replace('-', '', $uuid);
      // dd($nonce);
       //$nonce = "10259587675cdace75e9d19";

       //url
        $baseurl = "https://testids.interswitch.co.ke:19081";
        $url = "https://testids.interswitch.co.ke:19081/api/v1/wallet/status";
        $encodeUrl =  urlencode($url);
        
        //dd($encodeUrl);
        
       //signature
        $httpMethod = "GET";
        $clientId = "IKIA65D824B667995D138E5A71F21B67C33BC8DEA348";
        $clientSecret = "IpxUHRFGulpFAUaKIpX1HLdgi7QDUTmd8HQs5bZUjqw=";
        $signatureCipher = $httpMethod. "&" .$encodeUrl. "&" .$timestamp. "&" .$nonce. "&" .$clientId. "&" .$clientSecret;
          //dd($signatureCipher);
        $messageDigest = hash('SHA512', $signatureCipher, true);
        
        //dd($messageDigest);
        $message = base64_encode($messageDigest);
        
        //dd($message);

        $encodedClientId =  base64_encode($clientId);
        $authorization = "InterswitchAuth ".$encodedClientId;
        //dd($authorization);
       
        $client = new Client();  
  
        try {
             $response = $client->get($url,
              [
                'headers' => [
                  'Content-Type' => 'application/json',
                  'Authorization' => $authorization,
                  'Nonce' => $nonce,
                  'SignatureMethod' => 'SHA512',
                  'Signature' => $message,
                  'Timestamp' => $timestamp,
             ],
          ]);
         
             
           $requests = json_decode( (string) $response->getBody(), true );
           
           print('<pre>');
           print_r(json_encode($requests));
           print('</pre>');


       } catch (GuzzleHttp\Exception\BadResponseException $e) {
             var_dump($e->getMessage());
       }
    }
    
    //Wallet to Mpesa
    
    public function walletToMpesa()
    {
        //dd(request('amount'));
        $timezone = Carbon::parse(Carbon::now(),"Africa/Nairobi");

        $now = Carbon::now();
        $timestamp = $now->getTimestamp();
       //dd($timestamp);
        $uuid = (string) uniqid(rand());

        //Nonce
       $nonce = str_replace('-', '', $uuid);
      // dd($nonce);
       //$nonce = "10259587675cdace75e9d19";



       //url
        $baseurl = "https://testids.interswitch.co.ke:19081";

        $url = "https://testids.interswitch.co.ke:19081/api/v1/wallet/status";
        $url4 = "https://testids.interswitch.co.ke:19081/api/v1/wallet/wallets/transactions";
        $walletAccount  = "8880320000000026";
         $reference = "701";
        $amount = request('amount');
        $encodeUrl =  urlencode($url4);
        
        //dd($encodeUrl);
        
       //signature
        $httpMethod = "POST";
        $clientId = "IKIA65D824B667995D138E5A71F21B67C33BC8DEA348";
        $clientSecret = "IpxUHRFGulpFAUaKIpX1HLdgi7QDUTmd8HQs5bZUjqw=";
        $signatureCipher = $httpMethod. "&" .$encodeUrl. "&" .$timestamp. "&" .$nonce. "&" .$clientId. "&" .$clientSecret."&".$reference."&".$walletAccount."&".$amount;
          //dd($signatureCipher);
        $messageDigest = hash('SHA512', $signatureCipher, true);
        
        //dd($messageDigest);
        $message = base64_encode($messageDigest);
        
        //dd($message);

        $encodedClientId =  base64_encode($clientId);
        $authorization = "InterswitchAuth ".$encodedClientId;
        //dd($authorization);
       
        $client = new Client();  
  
        try {
             $response = $client->post($url4,
              [
                'headers' => [
                  'Content-Type' => 'application/json',
                  'Authorization' => $authorization,
                  'Nonce' => $nonce,
                  'SignatureMethod' => 'SHA512',
                  'Signature' => $message,
                  'Timestamp' => $timestamp,
             ],
              'body' => json_encode([
                  "walletAccount"  => $walletAccount,
                  "reference"   => $reference,
                  "processingCode"  => "502020",
                  "amount"      => request('amount'),
                  "provider"      => "MPESAWALLET",
                  "currency"      =>  404,
                  "beneficiaryAccount"=> "254723456312",
                  "senderName"    => "TEST TEST",
                  "transactionType" => "9004"
                ])
          ]);
         
             
           $requests = json_decode( (string) $response->getBody(), true );
           
            print('<pre>');
           print_r(json_encode($requests));
           print('</pre>');


       } catch (GuzzleHttp\Exception\BadResponseException $e) {
             var_dump($e->getMessage());
       }
       //return view('wallet.index');     
    }
    
     //Mpesa Enquiry 
    public function transactionsQuery()
    {
       
        //dd(request('amount'));
        $timezone = Carbon::parse(Carbon::now(),"Africa/Nairobi");

        $now = Carbon::now();
        $timestamp = $now->getTimestamp();
       //dd($timestamp);
        $uuid = (string) uniqid(rand());

        //Nonce
       $nonce = str_replace('-', '', $uuid);
      // dd($nonce);
       //$nonce = "10259587675cdace75e9d19";

       //url
        $reference = "701"; //this is the receipt number
        
        $baseurl = "https://testids.interswitch.co.ke:19081";

        $url = "https://testids.interswitch.co.ke:19081/api/v1/wallet/wallets/transactions/".$reference;
      
        $walletAccount  = "8880320000000026";
        $amount = request('amount');
        $encodeUrl =  urlencode($url);
        
        //dd($encodeUrl);
        
       //signature
        $httpMethod = "GET";
        $clientId = "IKIA65D824B667995D138E5A71F21B67C33BC8DEA348";
        $clientSecret = "IpxUHRFGulpFAUaKIpX1HLdgi7QDUTmd8HQs5bZUjqw=";
        $signatureCipher = $httpMethod. "&" .$encodeUrl. "&" .$timestamp. "&" .$nonce. "&" .$clientId. "&" .$clientSecret;
          //dd($signatureCipher);
        $messageDigest = hash('SHA512', $signatureCipher, true);
        
        //dd($messageDigest);
        $message = base64_encode($messageDigest);
        
        //dd($message);

        $encodedClientId =  base64_encode($clientId);
        $authorization = "InterswitchAuth ".$encodedClientId;
        //dd($authorization);
       
        $client = new Client();  
  
        try {
             $response = $client->get($url,
              [
                'headers' => [
                  'Content-Type' => 'application/json',
                  'Authorization' => $authorization,
                  'Nonce' => $nonce,
                  'SignatureMethod' => 'SHA512',
                  'Signature' => $message,
                  'Timestamp' => $timestamp,
             ],
          ]);
         
             
           $requests = json_decode( (string) $response->getBody(), true );
           
           print('<pre>');
           print_r(json_encode($requests));
           print('</pre>');


       } catch (GuzzleHttp\Exception\BadResponseException $e) {
             var_dump($e->getMessage());
       }
       //return view('wallet.index');     
    }
    
    
    public function balance()
    {
            //dd(request('amount'));
        $timezone = Carbon::parse(Carbon::now(),"Africa/Nairobi");

        $now = Carbon::now();
        $timestamp = $now->getTimestamp();
       //dd($timestamp);
        $uuid = (string) uniqid(rand());

        //Nonce
       $nonce = str_replace('-', '', $uuid);
      // dd($nonce);
       //$nonce = "10259587675cdace75e9d19";

       //url
        $baseurl = "https://testids.interswitch.co.ke:19081";

        $walletAccount  = "8880320000000026";

        $url = "https://testids.interswitch.co.ke:19081/api/v1/wallet/wallets/balance/".$walletAccount; 
    
      
        $encodeUrl =  urlencode($url);
        
        //dd($encodeUrl);
        
       //signature
        $httpMethod = "GET";
        $clientId = "IKIA65D824B667995D138E5A71F21B67C33BC8DEA348";
        $clientSecret = "IpxUHRFGulpFAUaKIpX1HLdgi7QDUTmd8HQs5bZUjqw=";
        $signatureCipher = $httpMethod. "&" .$encodeUrl. "&" .$timestamp. "&" .$nonce. "&" .$clientId. "&" .$clientSecret;
          //dd($signatureCipher);
        $messageDigest = hash('SHA512', $signatureCipher, true);
        
        //dd($messageDigest);
        $message = base64_encode($messageDigest);
        
        //dd($message);

        $encodedClientId =  base64_encode($clientId);
        $authorization = "InterswitchAuth ".$encodedClientId;
        //dd($authorization);
       
        $client = new Client();  
  
        try {
             $response = $client->get($url,
              [
                'headers' => [
                  'Content-Type' => 'application/json',
                  'Authorization' => $authorization,
                  'Nonce' => $nonce,
                  'SignatureMethod' => 'SHA512',
                  'Signature' => $message,
                  'Timestamp' => $timestamp,
             ],
          ]);
         
             
           $requests = json_decode( (string) $response->getBody(), true );
           
            print('<pre>');
           print_r(json_encode($requests));
           print('</pre>');


       } catch (GuzzleHttp\Exception\BadResponseException $e) {
             var_dump($e->getMessage());
       }
       //return view('wallet.index');
    }
}

 
