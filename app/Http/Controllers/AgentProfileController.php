<?php

namespace App\Http\Controllers;

use App\AgentProfile;
use Illuminate\Http\Request;
use App\Http\Requests\AgentProfileRequest;

class AgentProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'agent']);
    }


    public function index(AgentProfile $agentProfile)
    {
       $agentProfiles = $agentProfile->whereUserId(auth()->id())->get();

        $user = auth()->user();

        foreach($agentProfiles as $agentProfile){}

        return view('agent.index', compact('agentProfile','user'));
    }


   public function store(AgentProfileRequest $request, AgentProfile $agentProfile)
    {
        auth()->user()->agentProfile()->updateOrCreate(
            ['user_id' => auth()->id() ],
            $request->except('_token')
        );

        return back()->with('success', 'You have successfully updated your profile!');
    }

 }
