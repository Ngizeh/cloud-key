<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProfilePictureController extends Controller
{
	public function index(User $user)
	{
		$user = Auth::user();
		
		return view('agent.settings', compact('user'));
	}

	public function store(Request $request)
	{
		$this->validate($request, ['file' => 'required|mimes:jpg,jpeg,png,bmp']);

		$avatarName = $this->makePhoto($request->file('file'));

		$user = Auth::user();

		$user->avatar = $avatarName->avatar;

		$user->save();

		return back()->with('uploaded-picture', 'You\'ve uploaded your profile picture successfully');
	}

	public function destroy($id)
	{
		$user = User::findOrFail($id);

		\File::delete("uploaded/avatars/{$user->avatar}");

		$user->update(['avatar' => null]);

		return back()->with('uploaded-picture', 'You\'ve deleted your profile picture successfully');

	}


	protected function makePhoto(UploadedFile $file)
	{
		return User::named($file->getClientOriginalName())->move($file);
	}
}
