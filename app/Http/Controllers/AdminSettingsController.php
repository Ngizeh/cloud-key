<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminSettingsController extends Controller
{
    public function __invoke()
    {
    	$user = auth()->user();

    	return view('admin.settings', compact('user'));
    }

}
