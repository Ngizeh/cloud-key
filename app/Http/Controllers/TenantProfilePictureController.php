<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;

class TenantProfilePictureController extends Controller
{
    public function index(User $user)
    {
        $user = Auth::user();

        return view('tenant.settings', compact('user'));
    }
}
