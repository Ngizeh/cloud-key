<?php

namespace App\Http\Controllers;

use App\Role;


class AllAgentsController extends Controller
{
    public function __invoke()
    {
    	$agents = Role::query()->whereRole('agent')->with('user')->paginate(15);

        return view('admin.agents', compact('agents'));
    }
}
