<?php

namespace App\Http\Controllers;


use App\Admin;
use App\Role;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function index(Request $request)
    {
        $user = User::query()->select('id','name','email')->whereId(auth()->id())->first();

        return view('admin.index', compact('user'));

    }

    public function create()
    {
        return view('admin.create');
    }

    public function allAdmins()
    {
        $admins = Role::whereRole('admin')->paginate(15);

        return view('admin.all-admins', compact('admins'));

    }

    public function store(Request $request): RedirectResponse
    {
        $data = $request->validate([
            'email' => 'required|unique:users',
            'name' => 'required'
        ]);

        $admin  = auth()->user()->create([
            'email' => $data['email'],
            'name' => $data['name'],
            'password' => Hash::make('password')
        ]);

        $admin->roles()->create([
            'role' => 'admin',
            'terms' => 'yes'
        ]);

        return back()->with('added-admin', 'Successfully added anther admin');

    }

}
