<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class TujengepayController extends Controller
{
    public function savedata()
     {
      
       $client = new Client();
       $token = $client->post('https://api.tujengepay.com/oauth/token',[
        'form_params' => [
            "grant_type" => "client_credentials", 
            "client_id" => "7a46cc70-dde3-11e8-8c8e-5bda978f86e5",
            "client_secret" => "A0XpL8iUJEn9oayLHuq6Z0DAKhRCM2xulYcmJMC5"
          ]
      ]);
       
       $token = json_decode((string) $token->getBody(), true)['access_token'];
       
            
       $responce = $client->post('https://api.tujengepay.com/api/v1/quick/payments',[
             'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '.$token
            ],
              'body' => json_encode([
                  "organizationId" => "306d0790-01d6-11e9-936b-b97f2f159e32",
                  "externalIdentifier" => "1",
                 "phoneNumber" => request('phoneNumber'),
                 "amount" => request('amount'),
                 "callBackUrl" => "https://4013c0e8.ngrok.io/api/tujenge"
              ])
            ]);
       
        $message = $responce->getBody();
        
        $obj = json_decode($message);
        
        $success = $obj->message;
        
        return back()->with('success', $success);
        
        return redirect()->back();
    }

    public function responseData(Request $request)
    {
        $tujenge =  Tujengepay::create(
            request([
               "paymentId",
               "externalIdentifier",
               "accountNumber" ,
               "resourceIdentifier",
               "transactedAmount",
               "transactionCost",
               "actualAmount",
               "status",
               "receiptNumber",
               "transactionDate",
               "phoneNumber"
           ])
    );   
      return response()->json($tujenge->toArray(), 201);  
    }
}
