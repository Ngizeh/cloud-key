<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;

class LandLordPropertiesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'landlord']);
    }

    public function index()
    {
        $properties = Property::whereUserId(auth()->user()->roles->id)->paginate(15);

        return view('landlord.my-properties', compact('properties'));
    }

}
