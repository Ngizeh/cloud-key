<?php

namespace App\Http\Controllers;


use App\Property;
use App\PropertyPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PropertyRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class PropertyController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }


    public function index()
    {
         $user = Auth::user();

        return view('properties.index', compact( 'user'));
    }


    public function create(Property $property)
    {
       return view('properties.create', compact('property'));
    }


    public function store(Property $property, PropertyRequest $request)
    {
        $property = new Property($request->all());

        Auth::user()->properties()->save($property);

        return redirect($property->path());
    }

    public function addPhoto(Request $request, $slug)
    {

        $this->validate($request, ['file' => 'required|mimes:jpg,jpeg,png,bmp,webp']);

        $propertyPhoto = $this->makePhoto($request->file('file'));

        Property::locatedAt($slug)->addPhoto($propertyPhoto);

    }

    protected function makePhoto(UploadedFile $file)
    {
        return PropertyPhoto::named($file->getClientOriginalName())->move($file);
    }


    public function show(Property $property, $slug)
    {
         $property = Property::locatedAt($slug)->load('photos:id,thumbnail_path,path,property_id');

         return view('properties.show', compact('property'));
    }


    public function edit(Request $request, Property $property, $id)
    {
        $property = Property::findOrFail($id);

        return view('properties.edit', compact('property'));
    }


    public function update(PropertyRequest $request, $slug)
    {
        $property = Property::findOrFail($slug);

        $property->update($request->all());

        return redirect($property->path())->with('You have successfully updated the Property');
    }


    public function destroy($slug)
    {
        Property::findOrFail($slug)->delete();

        return back()->with('deleted-property', 'You have removed a property from listing');
    }

}
