<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Http\Requests\CommentRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BlogCommentsController extends Controller
{

    public function store(Blog $blog, CommentRequest $request): Model
    {
        return $blog->comments()->create($request->all());
    }

}
