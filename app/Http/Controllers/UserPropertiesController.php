<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Contracts\View\View;

class UserPropertiesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Property $property): View
    {
        $properties = $property->whereUserId(auth()->id())->with('photos')->latest()->paginate(4);

        $user = Auth::user();

        if(auth()->user()->roles->role === 'landlord')
            return  view('landlord.my-properties',compact('properties', 'user'));

        if (auth()->user()->roles->role === 'agent')
            return  view('agent.my-properties',compact('properties', 'user'));

    }
}
