<?php

namespace App\Http\Controllers;

use App\TenantProfile;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TenantProfileRequest;

class TenantProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'tenant']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param TenantProfile $tenant
     * @param Request $request
     * @return View
     */

    public function index(TenantProfile $tenant, Request $request): View
    {
        $tenantProfiles = $tenant->where('user_id', Auth::id())->get();

        $user = Auth::user();

        foreach ($tenantProfiles as $tenant) {}

        return view('tenant.index', compact('tenant','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TenantProfileRequest $request
     * @param TenantProfile $tenant
     * @return RedirectResponse
     */
    public function store(TenantProfileRequest $request, TenantProfile $tenant): RedirectResponse
    {
        auth()->user()->tenantProfile()->updateOrCreate(
            ['user_id' => auth()->id() ],
            $request->except('_token')
        );

        return back()->with('success', 'You have successfully updated your profile!');
    }

}

