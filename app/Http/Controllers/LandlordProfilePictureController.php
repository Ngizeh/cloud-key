<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;

class LandlordProfilePictureController extends Controller
{
    public function index(User $user)
    {
        $user = auth()->user();
        
        return view('landlord.settings',compact('user'));
    }
}
