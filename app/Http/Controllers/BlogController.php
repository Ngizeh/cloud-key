<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogImage;
use App\Reply;
use Illuminate\Http\Request;
use App\Http\Requests\BlogRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BlogController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth')->except(['index', 'show']);
	}


	public function index()
	{
		$blogs = Blog::query()
		->select('id','title','description','slug','user_id','created_at')
		->with('user:id,name','photos:path,blog_id')->latest()->paginate(9);

		return view('blog.index', compact('blogs'));
	}


	public function create(Blog $blog)
	{
		return view('blog.create', compact('blog'));
	}


	public function store(Blog $blog, BlogRequest $request)
	{
		$blog = auth()->user()->blog()->create($request->all());

		return redirect($blog->path().'/images');

	}

	public function addImage(Request $request, $slug)
	{

		$this->validate($request, ['file' => 'required|mimes:jpg,jpeg,png,bmp']);

		$blogImage = $this->makePhoto($request->file('file'));

		Blog::locatedAt($slug)->addPhoto($blogImage);

	}

	protected function makePhoto(UploadedFile $file)
	{
		return BlogImage::named($file->getClientOriginalName())->move($file);
	}


	public function show(Blog $blog, Reply $reply)
	{
		$comments = $blog->comments->load('replies.user');
		return view('blog.show', compact('blog', 'comments', 'reply'));
	}


	public function edit(Blog $blog)
	{
		return view('blog.edit', compact('blog'));
	}

	public function update(BlogRequest $request, Blog $blog)
	{
		$blog = Blog::findOrFail($blog->id);

        $blog->update($request->all());

		return redirect($blog->path().'/images');
	}


	public function destroy(Blog $blog)
	{
		Blog::findOrFail($blog->id)->delete();

        return back()->with('deleted-blog', 'You have removed a blog listing');
	}
}
