<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class MinistatementController extends Controller
{
    public function ministatement()
    {
        //dd(request('amount'));
        $timezone = Carbon::parse(Carbon::now(),"Africa/Nairobi");

        $now = Carbon::now();
        $timestamp = $now->getTimestamp();
       //dd($timestamp);
        $uuid = (string) uniqid(rand());

        //Nonce
       $nonce = str_replace('-', '', $uuid);
      // dd($nonce);
       //$nonce = "10259587675cdace75e9d19";



       //url
        $baseurl = "https://testids.interswitch.co.ke:19081";
        $walletAccount  = "8880320000000026";
        $reference = "624";
        $url = "https://testids.interswitch.co.ke:19081/api/v1/wallet/wallets/transactions/ministatement/".$walletAccount;
        $amount = request('amount');
        $encodeUrl =  urlencode($url);
        
        //dd($encodeUrl);
        
       //signature
        $httpMethod = "GET";
        $clientId = "IKIA65D824B667995D138E5A71F21B67C33BC8DEA348";
        $clientSecret = "IpxUHRFGulpFAUaKIpX1HLdgi7QDUTmd8HQs5bZUjqw=";
        $signatureCipher = $httpMethod. "&" .$encodeUrl. "&" .$timestamp. "&" .$nonce. "&" .$clientId. "&" .$clientSecret;
          //dd($signatureCipher);
        $messageDigest = hash('SHA512', $signatureCipher, true);
        
        //dd($messageDigest);
        $message = base64_encode($messageDigest);
        
        //dd($message);

        $encodedClientId =  base64_encode($clientId);
        $authorization = "InterswitchAuth ".$encodedClientId;
        //dd($authorization);
       
        $client = new Client();  
  
        try {
             $response = $client->get($url,
              [
                'headers' => [
                  'Content-Type' => 'application/json',
                  'Authorization' => $authorization,
                  'Nonce' => $nonce,
                  'SignatureMethod' => 'SHA512',
                  'Signature' => $message,
                  'Timestamp' => $timestamp,
             ],
          ]);
         
             
           $requests = json_decode( (string) $response->getBody(), true );
           
           print('<pre>');
           print_r(json_encode($requests));
           print('</pre>');



       } catch (GuzzleHttp\Exception\BadResponseException $e) {
             var_dump($e->getMessage());
       }
       //return view('wallet.index');     
    }
}
