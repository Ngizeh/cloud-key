<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Property;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {

        $blogs = Blog::query()
        ->select('id','title','description', 'slug','user_id', 'created_at')
        ->with('photos:id,path,blog_id', 'user:id, name')->latest()->get();

        return view('welcome', compact('blogs'));
    }
}
