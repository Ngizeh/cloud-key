<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;

class WalletToBankController extends Controller
{
    public function index()
    {
       return view('wallet.index');
    }
    
    //Post to Bank
    
    public function walletToBank()
    {
       
        //dd(request('amount'));
        $timezone = Carbon::parse(Carbon::now(),"Africa/Nairobi");

        $now = Carbon::now();
        $timestamp = $now->getTimestamp();
       //dd($timestamp);
        $uuid = (string) uniqid(rand());

        //Nonce
       $nonce = str_replace('-', '', $uuid);
      // dd($nonce);
       //$nonce = "10259587675cdace75e9d19";

       //url
        $baseurl = "https://testids.interswitch.co.ke:19081";

        $url = "https://testids.interswitch.co.ke:19081/api/v1/wallet/wallets/transactions";
      
        $walletAccount  = "8880320000000026";
        $reference = "747"; //this is the receipt number
        $amount = request('amount');
        $encodeUrl =  urlencode($url);
        
        //dd($encodeUrl);
        
       //signature
        $httpMethod = "POST";
        $clientId = "IKIA65D824B667995D138E5A71F21B67C33BC8DEA348";
        $clientSecret = "IpxUHRFGulpFAUaKIpX1HLdgi7QDUTmd8HQs5bZUjqw=";
        $signatureCipher = $httpMethod. "&" .$encodeUrl. "&" .$timestamp. "&" .$nonce. "&" .$clientId. "&" .$clientSecret."&".$reference."&".$walletAccount."&".$amount;
          //dd($signatureCipher);
        $messageDigest = hash('SHA512', $signatureCipher, true);
        
        //dd($messageDigest);
        $message = base64_encode($messageDigest);
        
        //dd($message);

        $encodedClientId =  base64_encode($clientId);
        $authorization = "InterswitchAuth ".$encodedClientId;
        //dd($authorization);
       
        $client = new Client();  
  
        try {
             $response = $client->post($url,
              [
                'headers' => [
                  'Content-Type' => 'application/json',
                  'Authorization' => $authorization,
                  'Nonce' => $nonce,
                  'SignatureMethod' => 'SHA512',
                  'Signature' => $message,
                  'Timestamp' => $timestamp,
             ],
              'body' => json_encode([
                  "walletAccount"  => $walletAccount,
                  "reference"   => $reference,
                  "processingCode"  => "502020",
                  "amount"      => $amount,
                  "provider"      => "BANK",
                  "providerID"      => "941",
                  "currency"      =>  404,
                  "narration" => "Payment",
                  "beneficiaryAccount"=> "1003199149",
                  "senderName"    => "Doris Karimi",
                  "transactionType" => "9002"
                ])
          ]);
         
             
           $requests = json_decode( (string) $response->getBody(), true );
           
           print('<pre>');
           print_r(json_encode($requests));
           print('</pre>');


       } catch (BadResponseException $e) {
               $response = $e->getResponse();
               $jsonBody = json_decode((string) $response->getBody());
               print('<pre>');
               print_r(json_encode($jsonBody));
               print('</pre>');
       }
       //return view('wallet.index');     
    }
    
    public function balance()
    {
            //dd(request('amount'));
        $timezone = Carbon::parse(Carbon::now(),"Africa/Nairobi");

        $now = Carbon::now();
        $timestamp = $now->getTimestamp();
       //dd($timestamp);
        $uuid = (string) uniqid(rand());

        //Nonce
       $nonce = str_replace('-', '', $uuid);
      // dd($nonce);
       //$nonce = "10259587675cdace75e9d19";

       //url
        $baseurl = "https://testids.interswitch.co.ke:19081";

        $walletAccount  = "8880320000000026";

        $url = "https://testids.interswitch.co.ke:19081/api/v1/wallet/wallets/balance/".$walletAccount; 
    
      
        $encodeUrl =  urlencode($url);
        
        //dd($encodeUrl);
        
       //signature
        $httpMethod = "GET";
        $clientId = "IKIA65D824B667995D138E5A71F21B67C33BC8DEA348";
        $clientSecret = "IpxUHRFGulpFAUaKIpX1HLdgi7QDUTmd8HQs5bZUjqw=";
        $signatureCipher = $httpMethod. "&" .$encodeUrl. "&" .$timestamp. "&" .$nonce. "&" .$clientId. "&" .$clientSecret;
          //dd($signatureCipher);
        $messageDigest = hash('SHA512', $signatureCipher, true);
        
        //dd($messageDigest);
        $message = base64_encode($messageDigest);
        
        //dd($message);

        $encodedClientId =  base64_encode($clientId);
        $authorization = "InterswitchAuth ".$encodedClientId;
        //dd($authorization);
       
        $client = new Client();  
  
        try {
             $response = $client->get($url,
              [
                'headers' => [
                  'Content-Type' => 'application/json',
                  'Authorization' => $authorization,
                  'Nonce' => $nonce,
                  'SignatureMethod' => 'SHA512',
                  'Signature' => $message,
                  'Timestamp' => $timestamp,
             ],
          ]);
         
             
           $requests = json_decode( (string) $response->getBody(), true );
           
            print('<pre>');
           print_r(json_encode($requests));
           print('</pre>');


       } catch (GuzzleHttp\Exception\BadResponseException $e) {
             var_dump($e->getMessage());
       }
       //return view('wallet.index');
    }
}
