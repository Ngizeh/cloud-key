<?php

namespace App\Http\Controllers;

use App\Interswitch;
use App\InterswitchCallback;
use Exception;
use Illuminate\Http\Request;

class InterswitchController extends Controller
{
    public  function callback(Request $request){

        try {
            $interswitch = InterswitchCallback::create(
                request([
                  'transactionID',
                  'status',
              ])
            );

            return response()->json([
                "responseCode"=> "0",
                "responseMessage" => "Transaction Updated Successfully"
            ],201);

        } catch (Exception $e) {
           return response()->json([
            "responseCode"=> "1",
            "responseMessage" => "Transaction could not be Updated"
        ], 403);
       }}

       public function notification(Request $request){

        try {
    //represent Rented Property details
            $interswitch = Interswitch::create(
                request([
              'transactionRef', //property id
              'status',
              'provider',  //optional
              'transactionAmount', // Property amount
              'orderId',//property receipt number
              'paymentItem',
              'initiatorAccount',
              'mpesaTransactionId', //optional
              'approvalCode', //optional
              'externalRefrence', //optional
              'statusMessage', // mandatory
              'authorization' //mandatory
          ])
            );

            return response()->json([
                "responseCode"=> "0",
                "responseMessage" => "Transaction Updated Successfully"
            ],201);

        } catch (Exception $e) {
           return response()->json([
            "responseCode"=> "1",
            "responseMessage" => "Transaction could not be Updated"
        ], 403);
       }
   }
}




  // "transactionRef" : "something" ,
  // "status" : "something",
  // "provider" : "something"  ,
  // "transactionAmount" : "something" ,
  // "orderId" : "something",
  // "paymentItem" : "something",
  // "initiatorAccount" : "something",
  // "mpesaTransactionId" : "something" ,
  // "approvalCode" : "something",
  // "externalRefrence" : "something",
  // "statusMessage" : "something" ,
  // "authorization" : "something" ,
