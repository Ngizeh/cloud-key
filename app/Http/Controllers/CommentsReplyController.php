<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class CommentsReplyController extends Controller
{
    public function index(Comment $comment)
    {
        return $comment->replies;
    }

    public function store(Comment $comment): Model
    {
        request()->validate([ 'reply' => 'required']);

        return $comment->replies()->create([
            'user_id' => auth()->id(),
            'reply' => request('reply'),
        ]);
    }

    public function update(Comment $comment): int
    {
        request()->validate([ 'reply' => 'required']);

        return $comment->replies()->update([
            'user_id' => auth()->id(),
            'reply' => request('reply'),
        ]);
    }
}
