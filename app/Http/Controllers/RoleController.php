<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    public function index(Role $role)
    {
       return view('role.index',compact('role'));     
    }
    
    public function store(RoleRequest $request, Role $role)
    {
      $role = new Role($request->all());
      
      Auth::user()->roles()->save($role);
      
      return redirect('/'.$role->role);
      
  }
}
