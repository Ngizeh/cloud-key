<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RentalController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');     
    }
    public function index(Property $property, $slug)
    {
       $user = Auth::user();
       
       $property = Property::locatedAt($slug);
       
       return view('rent.index', compact('property', 'user'));     
    }
    
    
}
