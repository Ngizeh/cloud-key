<?php

namespace App\Http\Controllers;

use App\TenantProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MyPaymentController extends Controller
{
    public function index(TenantProfile $profile)
    {
       $profiles = $profile->where('user_id', Auth::id())->get();
        
        $user = Auth::user();
    
        foreach ($profiles as $profile) {
            $profile;
        }
        
        return view('tenant.my-payments', compact('profile', 'user'));
    }
}
