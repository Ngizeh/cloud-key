<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogImage;
use Illuminate\Http\RedirectResponse;

class BlogImageController extends Controller
{
    public function create($slug, Blog $blog)
    {
        $blog = Blog::locatedAt($slug)->load('photos');

        return view('blog.blog-images', compact('blog', 'slug'));
    }

    public function destroy($id): RedirectResponse
    {
        BlogImage::findOrFail($id)->delete();

        return back();
    }
}
