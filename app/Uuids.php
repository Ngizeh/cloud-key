<?php

namespace App;

use Illuminate\Support\Str;

trait Uuids
{
  protected static function bootUuids() {
    static::creating(function ($model) {
      if (! $model->getKey()) {
        // $model->setAttribute($model->getKeyName(), Str::orderedUuid()->toString());
        $model->{$model->getKeyName()} = (string) Str::uuid();
      }
    });
  }

  public function getIncrementing()
  {
      return false;
  }

  public function getKeyType()
  {
      return 'string';
  }
}