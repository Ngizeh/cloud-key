<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Role extends Model
{
	use HasFactory;
	
	protected $casts = ['user_id' => 'string'];

	protected $fillable = [
		'role', 'terms', 'user_id'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function roleRedirect()
	{
		if ($this->role == 'landlord') {
			return redirect('/landlord');
		}
		if ($this->role == 'agent') {
			return redirect('/agent');
		}
		if ($this->role == 'agent') {
			return redirect('/tenant');
		}
	}
}
