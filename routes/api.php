<?php

use App\Interswitch;
use App\InterswitchCallback;
use App\Tujengepay;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
//cloudkey.co.ke/api/v1/interswitch
Route::post('v1/interswitch', 'InterswitchController@notification');

Route::post('v1/interswitch/callback', 'InterswitchController@callback');
/**
 * ---------------------------------------------
 */

Route::post('/tujenge', 'TujengepayController@responseData');
