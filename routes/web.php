<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;



/**
 * Public Viewing Routes
 * ----------------------------------------------------------------------------------------->
 */
Route::get('/','WelcomeController@index');
Route::view('about', 'about');
Route::view('services', 'services');
Route::view('my-favorite', 'tenant.my-favorite');
Route::view('faq', 'faq');


/**
 * Contact Us  Routes
 * ----------------------------------------------------------------------------------------->
 */
Route::get('/contact-us', 'ContactsController@index');
Route::post('/contact-us', 'ContactsController@store');


// Route::view('blog','blog');

// Route::view('blog-details','blog-full');


/**
 * Password Reset Routes
 * ----------------------------------------------------------------------------------------->
 */
Route::get('settings-password', 'SettingsController@index');
Route::patch('settings-password', 'SettingsController@update');

Route::get('my-properties', 'UserPropertiesController@index')->name('my-properties');


/**
 * Roles Routes
 * ----------------------------------------------------------------------------------------->
 */
Route::get('role', 'RoleController@index');
Route::post('role', 'RoleController@store');

/**
 * Property Routes
 * ----------------------------------------------------------------------------------------->
 */
Route::get('property', 'PropertyController@index');
Route::post('property', 'PropertyController@store');
Route::get('property/create', 'PropertyController@create');
Route::get('property/{id}/edit', 'PropertyController@edit');
Route::get('property/{slug}', 'PropertyController@show');
Route::patch('property/{slug}', 'PropertyController@update');
Route::delete('property/{id}', 'PropertyController@destroy');

/**
 * Property Photos Routes
 * ----------------------------------------------------------------------------------------->
 */
Route::post('photo/{slug}/files', 'PropertyController@addPhoto');
Route::get('photo/{slug}', 'PropertyPhotoController@show');
Route::delete('photo/{slug}', 'PropertyPhotoController@destroy');


/**
 * Agents Routes
 * ----------------------------------------------------------------------------------------->
 */
Route::resource('agent', 'AgentProfileController');
Route::get('agent-settings', 'ProfilePictureController@index');


/**
 * Admin Routes
 * ----------------------------------------------------------------------------------------->
 */
Route::get('admin', 'AdminController@index');
Route::get('all-admins', 'AdminController@allAdmins');
Route::get('admin/create', 'AdminController@create');
Route::post('admin/{admin}', 'AdminController@store');
Route::post('admin-profile', 'AdminProfileController@store');
Route::get('admin-properties', 'AdminAllPropertiesController');
Route::get('admin-settings', 'AdminSettingsController');
Route::post('admin-settings', 'AdminSettingsController@store');
Route::get('all-agents', 'AllAgentsController');
Route::get('all-tenants', 'AllTenantsController');
Route::get('all-landlords', 'AllLandlordController');
Route::get('list-blogs', 'AllBlogsController');

/**
 * LandLord Routes
 * ----------------------------------------------------------------------------------------->
 */
Route::resource('landlord', 'LandlordProfileController');
Route::get('landlord-settings', 'LandlordProfilePictureController@index');
Route::post('landlord-settings', 'LandlordProfilePictureController@store');
Route::get('/landlord-properties', 'LandLordPropertiesController@index');

/**
 * Tenants Routes
 * ----------------------------------------------------------------------------------------->
 */
Route::resource('tenant', 'TenantProfileController');
Route::get('tenant-settings', 'TenantProfilePictureController@index');


Route::get('my-tenants', 'RentedPropertyController@index');
Route::delete('/profile-picture/{id}', 'ProfilePictureController@destroy');
Route::post('/profile-picture', 'ProfilePictureController@store');

/**
 * Blog Routes
 * ----------------------------------------------------------------------------------------->
 */
Route::resource('blogs', 'BlogController');
Route::get('blog/{slug}/images', 'BlogImageController@create')->middleware('admin');
Route::post('blog-images/{slug}/files', 'BlogController@addImage')->middleware('admin');
Route::delete('blog-image/{blog}/', 'BlogImageController@destroy')->middleware('admin');
Route::post('blog/{blog}/comments', 'BlogCommentsController@store');
Route::post('comments/{comment}/reply', 'CommentsReplyController@store')->middleware('admin');
Route::patch('comments/{comment}/reply', 'CommentsReplyController@update')->middleware('admin');
Route::get('comments/{comment}', 'CommentsReplyController@index');
Route::get('blog/show', 'BlogController@show'); // This is a temporary route




/**
 * Payment Routes Groups
 * ----------------------------------------------------------------------------------------->
 */
Route::get('payments', 'MyPaymentController@index');
Route::get('rental/{slug}', 'RentalController@index');
Route::post('tujenge', 'TujengepayController@savedata');
Route::get('wallet', 'WalletController@index');
Route::get('status', 'WalletController@status');
Route::post('wallet', 'WalletController@walletToMpesa');
Route::get('balance', 'WalletToBankController@balance');
Route::get('transaction-query', 'WalletController@transactionsQuery'); //Mpesa
Route::get('ministatement', 'MinistatementController@ministatement');
Route::get('wallet-to-bank', 'WalletToBankController@index');
Route::post('wallet-to-bank', 'WalletToBankController@walletToBank');
Route::get('transaction-query-bank', 'WalletToBankTransanctionQueryController@transactionBankQuery'); //Bank
Route::post('collection', 'CollectionController@interswitchCollection'); //collection
Route::get('card', 'CollectionController@card'); //collection



//password Ngi!956ozeh

/**
 * Socialite Authentication Routes
 * ----------------------------------------------------------------------------------------->
 */
Route::get('redirect/{provider}', 'SocialController@redirect');
Route::get('callback/{provider}', 'SocialController@callback');
Route::get('logout', 'Auth\LoginController@logout');

Auth::routes();
