<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterswitchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('interswitches', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('transactionRef')->nullable(); //property->id
            $table->string('status')->nullable();
            $table->string('provider')->nullable();
            $table->string('transactionAmount')->nullable();
            $table->string('orderId')->nullable();
            $table->string('paymentItem')->nullable();
            $table->string('initiatorAccount')->nullable();
            $table->string('mpesaTransactionId')->nullable();
            $table->string('approvalCode')->nullable();
            $table->string('externalRefrence')->nullable();
            $table->string('statusMessage')->nullable();
            $table->string('authorization')->nullable();
            $table->timestamps();
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interswitches');
    }
}
