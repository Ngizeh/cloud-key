<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandlordProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landlord_profiles', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id')->index();
            $table->string('nationality');
            $table->string('physical_location');
            $table->string('bank_name');
            $table->string('swift_code')->nullable();
            $table->string('bank_account');
            $table->string('account_branch');
            $table->string('identification_number');
            $table->string('phone');
            $table->string('next_of_kin_name');
            $table->string('next_of_kin_email');
            $table->string('next_of_kin_phone');
            $table->string('next_of_kin_relationship');
            $table->timestamp('approved')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landlord_profiles');
    }
}
