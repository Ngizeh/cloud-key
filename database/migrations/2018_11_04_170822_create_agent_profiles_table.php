<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_profiles', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id')->index();
            $table->string('nationality');
            $table->string('identification_number');
            $table->string('phone');
            $table->string('company')->nullable();
            $table->unsignedInteger('years');
            $table->string('employment_status');
            $table->string('physical_location');
            $table->longText('bio');
            $table->string('bank_name');
            $table->string('bank_account');
            $table->string('account_branch');
            $table->string('swift_code');
            $table->timestamp('approved')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
