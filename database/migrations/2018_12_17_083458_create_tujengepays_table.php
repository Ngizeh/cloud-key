<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTujengepaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tujengepays', function (Blueprint $table) {
            $table->uuid('id')->primary();
             $table->string('paymentId');
             $table->string('externalIdentifier' );
             $table->string('accountNumber');
             $table->string('resourceIdentifier' );
             $table->string('transactedAmount'  );
             $table->string('transactionCost' );
             $table->string('actualAmount'  );
             $table->string('status' );
             $table->string('receiptNumber'  );
             $table->string('transactionDate'    );
             $table->string('phoneNumber');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tujengepays');
    }
}
