<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->uuid('id')->primary()->index();
            $table->uuid('user_id')->index();
            $table->string('title');
            $table->string('type');
            $table->double('price', 8, 2);
            $table->double('service_charge', 8, 2)->nullable();
            $table->double('deposit', 8, 2);
            $table->string('area');
            $table->integer('bedroom');
            $table->integer('bathroom');
            $table->string('address');
            $table->string('county');
            $table->string('town');
            $table->date('available_date');
            $table->boolean('approved')->default(0);
            $table->longtext('description');
            $table->string('postal_code')->nullable();
            $table->boolean('all_rented')->default(0);
            $table->integer('units');
            $table->string('slug');
            $table->unsignedInteger('compound_units');
            $table->boolean('installments')->default(0);
            $table->json('features')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
