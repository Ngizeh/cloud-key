<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_profiles', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id')->index();
            $table->string('nationality');
            $table->string('phone');
            $table->string('current_residence');
            $table->longText('bio');
            $table->string('identification_number');
            $table->string('employment_status');
            $table->string('next_of_kin_name');
            $table->string('next_of_kin_email');
            $table->string('next_of_kin_phone');
            $table->string('next_of_kin_relationship');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenants');
    }
}
