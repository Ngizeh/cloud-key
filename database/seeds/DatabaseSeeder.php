<?php

use App\Blog;
use App\BlogImage;
use App\Role;
use App\User;
use App\Property;
use App\PropertyPhoto;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::factory()->create([
            'name' => 'Ryan Admin',
            'email' => 'ryan@gmail.com',
            'password' => Hash::make('password'),
        ]);

        $agent = User::factory()->create([
            'name' => 'Ngizeh Mwas',
            'email' => 'ngizeh@gmail.com',
            'password' => Hash::make('password'),
        ]);

        $landlord = User::factory()->create([
            'name' => 'Maggie Githinji',
            'email' => 'maggie@gmail.com',
            'password' => Hash::make('password'),
        ]);

        $tenant = User::factory()->create([
            'name' => 'Suzie Githinji',
            'email' => 'suzie@gmail.com',
            'password' => Hash::make('password'),
        ]);

        Role::factory()->create([
            'role' => 'admin',
            'user_id' => $admin->id,
            'terms' => 'yes'
        ]);

        Role::factory()->create([
            'role' => 'landlord',
            'user_id' => $landlord->id,
            'terms' => 'yes'
        ]);

        Role::factory()->create([
            'role' => 'tenant',
            'user_id' => $tenant->id,
            'terms' => 'yes'
        ]);

        Role::factory()->create([
            'role' => 'agent',
            'user_id' => $agent->id,
            'terms' => 'yes'
        ]);

        Property::factory()->times(10)->create(['user_id' => $agent->id])->each(
            fn ($property) => PropertyPhoto::factory()->times(4)->create(['property_id' => $property->id])
        );

        Blog::factory()->times(10)->create(['user_id' => $agent->id])->each(
            fn ($blog) => BlogImage::factory()->create(['blog_id' => $blog->id])
        );
    }
}
