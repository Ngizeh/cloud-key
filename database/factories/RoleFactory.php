<?php

namespace Database\Factories;

use App\Role;
use App\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'role' => $this->faker->word,
            'user_id' => fn() => User::factory()->create()->id,
            'terms' => $this->faker->numberBetween(1, 0)
        ];
    }
}
