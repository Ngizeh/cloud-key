<?php

namespace Database\Factories;

use App\User;
use App\Property;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class PropertyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Property::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title  = $this->faker->realTextBetween(20, 40);

        return [
            'id' => Str::uuid(),
            'title' => $title,
            'type' => $this->faker->word,
            'price'  => $this->faker->numberBetween(2000, 5000),
            'deposit' => $this->faker->numberBetween(2000, 5000),
            'area' => $this->faker->numberBetween(10, 20),
            'bedroom' => $this->faker->numberBetween(10, 15),
            'bathroom' => $this->faker->numberBetween(10, 15),
            'address' => $this->faker->address,
            'county' => $this->faker->state,
            'town' => $this->faker->city,
            'available_date' => now(),
            'approved' => $this->faker->numberBetween(0, 1),
            'description' => $this->faker->realText(120),
            'postal_code' => $this->faker->postcode,
            'all_rented' => $this->faker->numberBetween(0, 1),
            'units' => $this->faker->numberBetween(5, 10),
            'slug' => Str::slug($title),
            'compound_units' => $this->faker->numberBetween(5, 10),
            'installments' => $this->faker->numberBetween(1000, 5000),
            'user_id' =>  fn() => User::factory()->create()->id
        ];
    }
}
