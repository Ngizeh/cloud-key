<?php

namespace Database\Factories;

use App\Property;
use App\PropertyPhoto;
use Illuminate\Database\Eloquent\Factories\Factory;

class PropertyPhotoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PropertyPhoto::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'property_id' => fn() => Property::factory()->create()->id,
            'name' => $this->faker->name,
            'path' => 'properties/photos/' .$this->faker->image(public_path('properties/photos'), 730, 485, null, false),
            'thumbnail_path' => 'properties/photos/'. $this->faker->image(public_path('properties/photos'), 374, 250, null, false)
        ];
    }
}