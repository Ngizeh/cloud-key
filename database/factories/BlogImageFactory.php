<?php

namespace Database\Factories;

use App\Blog;
use App\BlogImage;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlogImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BlogImage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => Str::uuid(),
            'blog_id' => fn() => Blog::factory()->create()->id,
            'name' => $this->faker->name,
            'path' => 'blog/photos/' .$this->faker->image(public_path('blog/photos'), 730, 485, null, false),
            'thumbnail' => 'blog/photos/'. $this->faker->image(public_path('blog/photos'), 374, 250, null, false)
        ];
    }
}